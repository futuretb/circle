# dope

#### 项目模块划分
- dope-user
    1. dope-user-api(controller接口模板，出/入参数)
        ##### api - controller 接口模板
        ##### input - user-service 请求入参
        ##### output - user-service 请求出参
    2. dope-user-service(具体业务实现，po、mapper-xml、dao、service/impl、controller)
        ##### controller - api 具体实现
        ##### dao - dao interface
        ##### impl - service 实现 
        ##### po - 数据库映射对象
        ##### service - service interface

#### 软件架构
SpringCould