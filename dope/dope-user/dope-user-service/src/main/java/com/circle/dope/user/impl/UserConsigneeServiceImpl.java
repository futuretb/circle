package com.circle.dope.user.impl;

import com.circle.dope.user.dao.UserConsigneeDao;
import com.circle.dope.user.service.UserConsigneeService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.user.po.UserConsignee;

/**
 * 收/发件人信息 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 16:32:48'.
 *  Email dicksoy@163.com
 */
@Service
public class UserConsigneeServiceImpl extends BaseServiceImpl<UserConsigneeDao, UserConsignee>
	implements UserConsigneeService {

}
