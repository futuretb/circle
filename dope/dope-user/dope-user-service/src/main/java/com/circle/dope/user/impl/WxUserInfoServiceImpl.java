package com.circle.dope.user.impl;

import com.circle.dope.user.dao.WxUserInfoDao;
import com.circle.dope.user.po.WxUserInfo;
import com.circle.dope.user.service.WxUserInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;

/**
 * 用户版用户信息表 - ServiceImpl
 *
 *  Created by dicksoy on '2018-12-26 10:43:16'.
 *  Email dicksoy@163.com
 */
@Service
public class WxUserInfoServiceImpl extends BaseServiceImpl<WxUserInfoDao, WxUserInfo>
	implements WxUserInfoService {

}
