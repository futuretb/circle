package com.circle.dope.user.job;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.MD5Util;
import com.circle.dope.user.client.OutsideServiceClient;
import com.circle.dope.user.po.WxUserInfo;
import com.circle.dope.user.service.WxUserInfoService;
import com.circle.dope.user.util.ImEasemobUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 生成用户二维码以及注册Im
 */
@JobHandler(value="wxUserInfoJobHandler")
@Component
public class WxUserInfoJobHandler extends IJobHandler {

    @Resource
    private WxUserInfoService wxUserInfoService;

    @Resource
    private ImEasemobUtil imEasemobUtil;

    @Resource
    private OutsideServiceClient outsideServiceClient;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        // 查询二维码或者im_id为空的用户
        Map<String, Object> params = new HashMap<>();
        ListResult<WxUserInfo> wxUserInfos = wxUserInfoService.findList(params);
        if (null != wxUserInfos && null != wxUserInfos.getList() && wxUserInfos.getList().size() > 0) {
            for (WxUserInfo wxUserInfo: wxUserInfos.getList()) {
                if (null != wxUserInfo) {
                    String qrCode = "";
                    String imId = "";
                    if (StringUtils.isEmpty(wxUserInfo.getQrCode())) {
                        String logoUrl = wxUserInfo.getAvatar();
                        String qrCodeUrl = "http://www.baidu.com/" + wxUserInfo.getWxUserCode();
                        qrCode = generateQrCode(qrCodeUrl, logoUrl);
                    }
                    if (StringUtils.isEmpty(wxUserInfo.getImId())) {
                        imId = generateImId(wxUserInfo);
                    }
                    WxUserInfo newWxUserInfo = new WxUserInfo();
                    newWxUserInfo.setId(wxUserInfo.getId());
                    newWxUserInfo.setQrCode(qrCode);
                    newWxUserInfo.setImId(imId);
                    wxUserInfoService.updateByCondition(newWxUserInfo);
                }
            }
        }
        return ReturnT.SUCCESS;
    }

    private String generateImId(WxUserInfo wxUserInfo) {
        Map<String, String> imMap = new HashMap<>();
        imMap.put("username", wxUserInfo.getWxUserCode());
        imMap.put("password", MD5Util.md5(wxUserInfo.getWxUserCode()));
        Map<String, Object> response = imEasemobUtil.register(imMap);
        return String.valueOf(response.get("uuid"));
    }

    private String generateQrCode(String qrCodeUrl, String logoUrl) {
        Map<String, Object> params = new HashMap<>();
        params.put("logoUrl", logoUrl);
        params.put("qrCodeUrl", qrCodeUrl);
        BaseResult baseResult = outsideServiceClient.generateUploadQrCode(params);
        AssertUtil.notNull(baseResult);
        return String.valueOf(baseResult.getResult());
    }

}
