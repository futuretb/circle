package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.EmployeeInfo;

/**
 * 员工 - Service
 *
 *  Created by dicksoy on '2019-01-06 16:32:02'.
 *  Email dicksoy@163.com
 */
public interface EmployeeInfoService extends BaseService<EmployeeInfo> {

}
