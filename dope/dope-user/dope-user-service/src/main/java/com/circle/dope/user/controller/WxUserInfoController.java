package com.circle.dope.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.*;
import com.circle.dope.user.enums.LOGIN_TYPE;
import com.circle.dope.user.enums.USER_INFO_STATUS;
import com.circle.dope.user.po.MiddleUserWxUser;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import com.circle.dope.user.service.MiddleUserWxUserService;
import com.circle.dope.user.service.UserInfoService;
import com.circle.dope.user.service.WxUserInfoService;
import com.circle.dope.user.util.UserInfoUtil;
import com.circle.dope.user.util.WxUtil;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 用户版用户信息表 - Controller
 *
 *  Created by dicksoy on '2018-12-26 10:43:16'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/user/wxUserInfo")
public class WxUserInfoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WxUserInfoController.class);

    @Resource
    private WxUserInfoService wxUserInfoService;

    @Resource
    private RedisManager redisManager;

    @Resource
    private WxUtil wxUtil;

    @Resource
    private UserInfoService userInfoService;

    @Resource
    private MiddleUserWxUserService middleUserWxUserService;

    @Resource
    private UserInfoUtil userInfoUtil;

    @RequestMapping(value = "/findByWxUserCode", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByWxUserCode(@RequestParam("wxUserCode") String wxUserCode) {
        WxUserInfo wxUserInfo = userInfoUtil.findWxUserInfo(wxUserCode);
        wxUserInfo.setNickName(UnicodeUtil.unicodeToCn(wxUserInfo.getNickName()));
        wxUserInfo.setSignature(UnicodeUtil.unicodeToCn(wxUserInfo.getSignature()));
        return SUCCESS.result(wxUserInfo);
    }

    /**
     * 通用登录
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult login(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        System.out.println(JSONObject.toJSONString(params));
        String loginType = String.valueOf(params.get("loginType"));
        if (!StringUtils.isEmpty(loginType)) {
            if (loginType.equals(LOGIN_TYPE.ACCOUNT.name())) {
                return accountLogin(params);
            }
            if (loginType.equals(LOGIN_TYPE.WECHAT.name())) {
                return wechatLogin(params);
            }
            if (loginType.equals(LOGIN_TYPE.MOBILE.name())) {
                return mobileLogin(params);
            }
            if (loginType.equals(LOGIN_TYPE.MINI_PROGRAM.name())) {
                return miniProgramLogin(params);
            }
        }
        return FAILD;
    }

    /**
     * 小程序成功授权
     * @param params
     * @param buyerToken
     * @return
     */
    @RequestMapping(value = "/miniProgramAuthSuccess", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult miniProgramAuthSuccess(@RequestBody Map<String, String> params,
                                             @RequestParam(UserType.BUYER_TOKEN) String buyerToken) {
        boolean result = false;
        try {
            String encryptedData = params.get("encryptedData");
            String sessionKey = params.get("sessionKey");
            String iv = params.get("iv");
            byte[] resultByte = AesUtil.decrypt(Base64.decodeBase64(encryptedData),
                    Base64.decodeBase64(sessionKey),
                    Base64.decodeBase64(iv));
            String wxUserInfoStr = new String(resultByte, "UTF-8");
            HashMap<String, String> wxUserInfoMap = JSONObject.parseObject(wxUserInfoStr, HashMap.class);
            Map<String, Object> findOneMap = new HashMap<>();
            findOneMap.put("wxUserCode", buyerToken);
            WxUserInfo wxUserInfo = wxUserInfoService.findOne(findOneMap);
            AssertUtil.notNull(wxUserInfo);
            wxUserInfo.setAvatar(wxUserInfoMap.get("avatarUrl"));
            wxUserInfo.setNickName(UnicodeUtil.cnToUnicode(wxUserInfoMap.get("nickName")));
            wxUserInfo.setUnionId(wxUserInfoMap.get("unionId"));
            result = wxUserInfoService.updateByCondition(wxUserInfo);
            userInfoUtil.refreshRedis(buyerToken, wxUserInfo);
        } catch (InvalidAlgorithmParameterException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result ? SUCCESS : FAILD;
    }

    /**
     * 绑定手机
     */
    @RequestMapping(value = "/bindPhone", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult bindPhone(@RequestBody Map<String, Object> params,
                                @RequestParam(UserType.BUYER_TOKEN) String buyerToken) {
        AssertUtil.notNull(params);
        WxUserInfo wxUserInfo = userInfoUtil.findWxUserInfo(buyerToken);
        String mobile = String.valueOf(params.get("mobile"));
        MatchUtil.matchMobile(mobile);
        String verifyCode = String.valueOf(params.get("verifyCode"));
        String key = PrefixEnum.MESSAGE_TYPE.BIND_PHONE.getPrefix() + mobile;
        String redisVerifyCode = redisManager.get(key);
        if (!verifyCode.equals(redisVerifyCode)) {
            return FAILD.msg("验证码错误");
        }
        redisManager.del(key);
        Map<String, Object> findOneMap = new HashMap<>();
        findOneMap.put("mobile", mobile);
        UserInfo userInfo = userInfoService.findOne(findOneMap);
        BeanUtil.mapToBean(params, wxUserInfo);
        MiddleUserWxUser middleUserWxUser = new MiddleUserWxUser();
        middleUserWxUser.setWxUserCode(wxUserInfo.getWxUserCode());
        if (null == userInfo) {
            userInfo = new UserInfo();
            userInfo.setUserId(UUID.randomUUID().toString());
            userInfo.setStatus(USER_INFO_STATUS.INIT.name());
            userInfo.setUserName(UnicodeUtil.cnToUnicode(wxUserInfo.getNickName()));
            BeanUtil.copeProperties(params, userInfo);
        }
        Integer count = middleUserWxUserService.findTotal(BeanUtil.beanToMap(middleUserWxUser));
        if (count > 0) {
            return FAILD.msg("当前手机号已被其他买家绑定");
        } else {
            middleUserWxUser.setUserId(userInfo.getUserId());
            return middleUserWxUserService.insertMiddle(wxUserInfo, userInfo, middleUserWxUser) > 0 ?
                    SUCCESS : FAILD;
        }
    }

    @RequestMapping(value = "/updateByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult updateByCondition(@RequestBody WxUserInfo params,
                                        @RequestParam(UserType.BUYER_TOKEN) String buyerToken) {
        if (!StringUtils.isEmpty(buyerToken)) {
            WxUserInfo oldWxUserInfo = userInfoUtil.findWxUserInfo(buyerToken);
            params.setId(oldWxUserInfo.getId());
        }
        if (!StringUtils.isEmpty(params.getSignature())) {
            params.setSignature(UnicodeUtil.cnToUnicode(params.getSignature()));
        }
        return wxUserInfoService.updateByCondition(params) ? SUCCESS : FAILD;
    }

    private BaseResult wechatLogin(Map<String, Object> params) {
        Map<String, Object> findOneMap = new HashMap<>();
        findOneMap.put("unionId", params.get("unionId"));
        WxUserInfo wxUserInfo = wxUserInfoService.findOne(findOneMap);
        if (null == wxUserInfo) {
            wxUserInfo = new WxUserInfo();
            BeanUtil.mapToBean(params, wxUserInfo);
            wxUserInfo.setType(1);
            insertWxUserInfo(wxUserInfo);
        }
        return SUCCESS.result(wxUserInfo);
    }

    private BaseResult accountLogin(Map<String, Object> params) {
        WxUserInfo wxUserInfo = findWxUserByMobile(String.valueOf(params.get("mobile")));
        if (null == wxUserInfo) {
            return FAILD.msg("当前账户不存在");
        } else {
            return wxUserInfo.getPassword().equals(params.get("password")) ? SUCCESS.result(wxUserInfo) : FAILD.msg("账号密码错误");
        }
    }

    private BaseResult mobileLogin(Map<String, Object> params) {
        String mobile = String.valueOf(params.get("mobile"));
        String verifyCode = redisManager.get(PrefixEnum.MESSAGE_TYPE.LOGIN.getPrefix() + mobile);
        if (StringUtils.isEmpty(verifyCode)) {
            return FAILD.msg("验证码已过期");
        }
        if (!verifyCode.equals(params.get("verifyCode"))) {
            return FAILD.msg("验证码错误");
        }
        WxUserInfo wxUserInfo = findWxUserByMobile(mobile);
        if (!StringUtils.isEmpty(wxUserInfo.getWxUserCode())) {
            wxUserInfo = new WxUserInfo();
            BeanUtils.copyProperties(wxUserInfo, params);
        }
        return insertWxUserInfo(wxUserInfo) > 0 ? SUCCESS.result(wxUserInfo) : FAILD;
    }

    private BaseResult miniProgramLogin(Map<String, Object> params) {
        Map<String, Object> response = wxUtil.miniProgramOauthInfo(String.valueOf(params.get("code")));
        String openId = String.valueOf(response.get("openid"));
        String sessionKey = String.valueOf(response.get("session_key"));
        String unionId = String.valueOf(response.get("unionId"));
        WxUserInfo wxUserInfo = findWxUserByOpenId(openId);
        if (null == wxUserInfo) {
            wxUserInfo = new WxUserInfo();
            wxUserInfo.setOpenId(openId);
            wxUserInfo.setType(0);
            wxUserInfo.setUnionId(StringUtils.isEmpty(unionId) ? null : unionId);
            insertWxUserInfo(wxUserInfo);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("buyerToken", wxUserInfo);
        result.put("sessionKey", sessionKey);
        return SUCCESS.result(result);
    }

    private Long insertWxUserInfo(WxUserInfo wxUserInfo) {
        try {
            if (null != wxUserInfo) {
                wxUserInfo.setWxUserCode("WX_" + UUID.randomUUID().toString());
                wxUserInfo.setStatus("NORMAL");
                wxUserInfo.setLevel(0);
                if (null != wxUserInfo.getNickName()) {
                    wxUserInfo.setNickName(UnicodeUtil.cnToUnicode(wxUserInfo.getNickName()));
                }
                if (null != wxUserInfo.getSignature()) {
                    wxUserInfo.setSignature(UnicodeUtil.cnToUnicode(wxUserInfo.getSignature()));
                }
                return wxUserInfoService.insertByCondition(wxUserInfo);
            }
        } finally {
            userInfoUtil.refreshRedis(wxUserInfo.getWxUserCode(), wxUserInfo);
        }
        return null;
    }

    private WxUserInfo findWxUserByMobile(String mobile) {
        Map<String, Object> accountMap = new HashMap<>();
        accountMap.put("mobile", mobile);
        return wxUserInfoService.findOne(accountMap);
    }

    private WxUserInfo findWxUserByOpenId(String openId) {
        Map<String, Object> openIdMap = new HashMap<>();
        openIdMap.put("openId", openId);
        return wxUserInfoService.findOne(openIdMap);
    }

}
