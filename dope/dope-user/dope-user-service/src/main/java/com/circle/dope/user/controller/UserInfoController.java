package com.circle.dope.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.*;
import com.circle.dope.user.enums.USER_INFO_STATUS;
import com.circle.dope.user.po.EmployeeInfo;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.service.EmployeeInfoService;
import com.circle.dope.user.service.UserInfoService;
import com.circle.dope.user.util.UserInfoUtil;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.Message.USER_NOT_EXIST;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 商家版用户信息表 - Controller
 *
 *  Created by dicksoy on '2018-12-24 17:37:24'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/user/userInfo")
public class UserInfoController {

    @Resource
    private RedisManager redisManager;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Resource
    private UserInfoUtil userInfoUtil;

    @Resource
    private UserInfoService userInfoService;

    @Resource
    private EmployeeInfoService employeeInfoService;

    @RequestMapping(value = "/findByUserId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByWxUserCode(@RequestBody String userId) {
        UserInfo userInfo = userInfoUtil.findUserInfo(userId);
        return SUCCESS.result(userInfo);
    }

    /**
     * 绑定邀请码 - 成为会员（商家版）
     * @param params
     * @return
     */
    @RequestMapping(value = "/bindInviteCode", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult bindInviteCode(@RequestBody Map<String, Object> params,
                                     @RequestParam(UserType.SELLER_TOKEN) String sellerToken) {
        AssertUtil.notNull(params);
        String mobile = String.valueOf(params.get("mobile"));
        MatchUtil.matchMobile(mobile);
        String inviteCode = String.valueOf(params.get("inviteCode"));
        String key = PrefixEnum.MESSAGE_TYPE.INVITE_CODE.getPrefix() + mobile + "_" + inviteCode;
        String value = redisManager.get(key);
        if (StringUtils.isEmpty(value)) {
            return FAILD.msg("验证码不正确");
        }
        Map<String, Object> inviteMap = JSONObject.parseObject(value, HashMap.class);
        AssertUtil.notNull(inviteMap);
        // TODO 根据类型创建指定数据
        System.out.println(inviteCode);
        EmployeeInfo employeeInfo = new EmployeeInfo();
        if (inviteMap.get("from").equals("SYSTEM")) {
            // 系统发送 用户为店长
            UserInfo userInfo = findByMobile(mobile);
            Assert.notNull(userInfo, USER_NOT_EXIST);
            employeeInfo.setUserId(userInfo.getUserId());
            employeeInfo.setPositionType(1);
            employeeInfoService.insert(employeeInfo);
        }
        redisManager.del(key);
        return SUCCESS.result(employeeInfo);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult login(@RequestBody UserInfo userInfo) {
        AssertUtil.notNull(userInfo);
        UserInfo dbUserInfo = findByMobile(userInfo.getMobile());
        if (null == dbUserInfo) {
            return FAILD.msg(USER_NOT_EXIST);
        } else {
            if (!dbUserInfo.getPassword().equals(userInfo.getPassword())) {
                return FAILD.msg("密码错误，请重新输入");
            } else {
                if (!dbUserInfo.getStatus().equals(USER_INFO_STATUS.NORMAL.name())) {
                    return FAILD.msg("用户状态不正确").result(dbUserInfo.getStatus());
                } else {
                    userInfoUtil.refreshRedis(userInfo.getUserId(), userInfo);
                    return SUCCESS.result(dbUserInfo);
                }
            }
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult register(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        UserInfo userInfo = new UserInfo();
        BeanUtil.mapToBean(params, userInfo);
        String key = PrefixEnum.MESSAGE_TYPE.REGISTER.getPrefix() + userInfo.getMobile();
        String verifyCode = redisManager.get(key);
        if (StringUtils.isEmpty(verifyCode)) {
            return FAILD.msg("验证码已过期");
        }
        if (!verifyCode.equals(params.get("verifyCode"))) {
            return FAILD.msg("验证码错误");
        }
        UserInfo dbUserInfo = findByMobile(userInfo.getMobile());
        if (null != dbUserInfo) {
            return FAILD.msg("当前手机号已被注册卖家版账号");
        }
        if (null != userInfo.getNickName()) {
            userInfo.setNickName(UnicodeUtil.cnToUnicode(userInfo.getNickName()));
        }
        String userId = UUID.randomUUID().toString();
        userInfo.setUserId(userId);
        userInfo.setStatus(USER_INFO_STATUS.INIT.name());
        Long id = userInfoService.insertByCondition(userInfo);
        redisManager.del(key);
        if (id > 0) {
            return SUCCESS.result(userId);
        }
        return FAILD;
    }

    @KafkaListener(topics = "dope-user")
    public void getMessage(String content) {
        System.out.println(content);
    }

    private UserInfo findByMobile(String mobile) {
        Map<String, Object> params = new HashMap<>();
        params.put("mobile", mobile);
        return userInfoService.findOne(params);
    }
}
