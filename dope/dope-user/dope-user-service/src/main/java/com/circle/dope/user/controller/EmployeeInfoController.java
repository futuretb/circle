package com.circle.dope.user.controller;

import com.circle.dope.user.api.EmployeeInfoApi;
import com.circle.dope.user.po.EmployeeInfo;
import com.circle.dope.user.service.EmployeeInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 员工 - Controller
 *
 *  Created by dicksoy on '2019-01-06 16:32:02'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/user/employeeInfo")
public class EmployeeInfoController {


}
