package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.MiddleUserWxUser;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;

/**
 * 商家版 - 用户版 用户绑定关系表 - Service
 *
 *  Created by dicksoy on '2019-01-06 10:25:32'.
 *  Email dicksoy@163.com
 */
public interface MiddleUserWxUserService extends BaseService<MiddleUserWxUser> {

    Long insertMiddle(WxUserInfo wxUserInfo, UserInfo userInfo, MiddleUserWxUser middleUserWxUser);
}
