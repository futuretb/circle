package com.circle.dope.user.impl;

import com.circle.dope.user.dao.MiddleUserFriendDao;
import com.circle.dope.user.service.MiddleUserFriendService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.user.po.MiddleUserFriend;

/**
 * 用户版 - 关注粉丝表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 16:32:27'.
 *  Email dicksoy@163.com
 */
@Service
public class MiddleUserFriendServiceImpl extends BaseServiceImpl<MiddleUserFriendDao, MiddleUserFriend>
	implements MiddleUserFriendService {

}
