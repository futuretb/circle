package com.circle.dope.user.exception;

public class WxException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public WxException(String message) {
        super(message);
    }
}
