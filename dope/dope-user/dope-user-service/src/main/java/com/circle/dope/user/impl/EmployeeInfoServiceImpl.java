package com.circle.dope.user.impl;

import com.circle.dope.user.dao.EmployeeInfoDao;
import com.circle.dope.user.service.EmployeeInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.user.po.EmployeeInfo;

/**
 * 员工 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 16:32:02'.
 *  Email dicksoy@163.com
 */
@Service
public class EmployeeInfoServiceImpl extends BaseServiceImpl<EmployeeInfoDao, EmployeeInfo>
	implements EmployeeInfoService {

}
