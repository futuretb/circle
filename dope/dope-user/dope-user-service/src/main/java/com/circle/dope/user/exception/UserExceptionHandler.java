package com.circle.dope.user.exception;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.exception.GlobalDopeExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.circle.dope.framework.base.BaseResult.Code.ERROR;
import static com.circle.dope.framework.base.BaseResult.FAILD;

@ControllerAdvice
public class UserExceptionHandler extends GlobalDopeExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public BaseResult processException(RuntimeException exception) {
        LOGGER.info("RuntimeException : {}", exception);
        return FAILD.result(exception.getMessage());
    }

    @ExceptionHandler(WxException.class)
    @ResponseBody
    public BaseResult processException(IllegalArgumentException exception) {
        LOGGER.info("WxException : {}", exception);
        return BaseResult.builder().code(ERROR).msg(exception.getMessage());
    }
}
