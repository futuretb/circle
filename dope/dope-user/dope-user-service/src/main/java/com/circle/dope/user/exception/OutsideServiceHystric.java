package com.circle.dope.user.exception;

import com.circle.dope.framework.base.BaseHystric;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.user.client.OutsideServiceClient;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OutsideServiceHystric implements OutsideServiceClient {

    private static final BaseHystric HYSTRIC = BaseHystric.newInstance(OutsideServiceHystric.class);

    @Override
    public BaseResult generateUploadQrCode(Map<String, Object> params) {
        return HYSTRIC.defaultMessage();
    }
}
