package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.UserConsignee;

/**
 * 收/发件人信息 - Service
 *
 *  Created by dicksoy on '2019-01-06 16:32:48'.
 *  Email dicksoy@163.com
 */
public interface UserConsigneeService extends BaseService<UserConsignee> {

}
