package com.circle.dope.user.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.user.po.UserConsignee;
import com.circle.dope.user.service.UserConsigneeService;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 收/发件人信息 - Controller
 *
 *  Created by dicksoy on '2019-01-06 16:32:48'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/user/userConsignee")
public class UserConsigneeController {

    @Resource
    private UserConsigneeService userConsigneeService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insert(@RequestBody UserConsignee userConsignee,
                             @RequestParam(value = UserType.BUYER_TOKEN) String buyerToken,
                             @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        AssertUtil.notNull(userConsignee);
        setUser(buyerToken, sellerToken, userConsignee);
        return userConsigneeService.insert(userConsignee) > 0 ? SUCCESS : FAILD;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody UserConsignee userConsignee,
                             @RequestParam(value = UserType.BUYER_TOKEN) String buyerToken,
                             @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        AssertUtil.notNull(userConsignee);
        setUser(buyerToken, sellerToken, userConsignee);
        return userConsigneeService.update(userConsignee) ? SUCCESS : FAILD;
    }

    /**
     * 设置所属用户
     * @param buyerToken
     * @param sellerToken
     * @param userConsignee
     */
    private void setUser(String buyerToken, String sellerToken, UserConsignee userConsignee) {
        if (StringUtils.isEmpty(sellerToken)) {
            userConsignee.setUserId(sellerToken);
        }
        if (StringUtils.isEmpty(buyerToken)) {
            userConsignee.setWxUserCode(buyerToken);
        }
    }

    @RequestMapping(value = "/findByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByCondition(@RequestBody Map<String, Object> params,
                                      @RequestParam(value = UserType.BUYER_TOKEN) String buyerToken,
                                      @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        if (StringUtils.isEmpty(sellerToken)) {
            params.put("userId", sellerToken);
        }
        if (StringUtils.isEmpty(buyerToken)) {
            params.put("wxUserCode", buyerToken);
        }
        return SUCCESS.result(userConsigneeService.findListByPage(params));
    }
}
