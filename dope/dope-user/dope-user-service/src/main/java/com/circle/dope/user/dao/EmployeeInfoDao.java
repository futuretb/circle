package com.circle.dope.user.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.user.po.EmployeeInfo;

/**
 * 员工 - Dao
 *
 *  Created by dicksoy on '2019-01-06 16:32:02'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface EmployeeInfoDao extends BaseDao<EmployeeInfo> {

}
