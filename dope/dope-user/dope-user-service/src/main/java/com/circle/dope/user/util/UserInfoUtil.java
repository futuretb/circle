package com.circle.dope.user.util;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import com.circle.dope.user.service.UserInfoService;
import com.circle.dope.user.service.WxUserInfoService;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.Message.USER_NOT_EXIST;

@Configuration
public class UserInfoUtil {

    @Resource
    private RedisManager redisManager;

    @Resource
    private WxUserInfoService wxUserInfoService;

    @Resource
    private UserInfoService userInfoService;

    public UserInfo findUserInfo(String userId) {
        return (UserInfo) baseFind(userId);
    }

    public WxUserInfo findWxUserInfo(String wxUserCode) {
        return (WxUserInfo) baseFind(wxUserCode);
    }

    public void refreshRedis(String token, Object object) {
        AssertUtil.notNull(token);
        AssertUtil.notNull(object);
        String value = JSONObject.toJSONString(object);
        if (token.startsWith("WX_")) {
            redisManager.set(PrefixEnum.USER_TYPE.WX_USER.getPrefix() + token, value);
        } else {
            redisManager.set(PrefixEnum.USER_TYPE.USER.getPrefix() + token, value);
        }
    }

    private Object baseFind(String userIdOrCode) {
        AssertUtil.notNull(userIdOrCode);
        String key = null;
        boolean isWxUser = false;
        Object infoObj = null;
        if (userIdOrCode.startsWith("WX_")) {
            key = PrefixEnum.USER_TYPE.WX_USER.getPrefix() + userIdOrCode;
            isWxUser = true;
        } else {
            key = PrefixEnum.USER_TYPE.USER.getPrefix() + userIdOrCode;
        }
        String userStr = redisManager.get(key);
        if (!StringUtils.isEmpty(userStr)) {
            return isWxUser ? JSONObject.parseObject(userStr, WxUserInfo.class) : JSONObject.parseObject(userStr, UserInfo.class);
        } else {
            Map<String, Object> findOneMap = new HashMap<>();
            if (isWxUser) {
                findOneMap.put("wxUserCode", userIdOrCode);
                WxUserInfo wxUserInfo = wxUserInfoService.findOne(findOneMap);
                if (null == wxUserInfo) {
                    Assert.notNull(wxUserInfo, USER_NOT_EXIST);
                }
                infoObj = wxUserInfo;
            } else {
                findOneMap.put("userId", userIdOrCode);
                UserInfo userInfo = userInfoService.findOne(findOneMap);
                if (null == userInfo) {
                    Assert.notNull(userInfo, USER_NOT_EXIST);
                }
                infoObj = userInfo;
            }
            return infoObj;
        }
    }
}
