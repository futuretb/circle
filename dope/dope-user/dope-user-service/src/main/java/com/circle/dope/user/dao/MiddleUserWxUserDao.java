package com.circle.dope.user.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.user.po.MiddleUserWxUser;

/**
 * 商家版 - 用户版 用户绑定关系表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 16:32:33'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface MiddleUserWxUserDao extends BaseDao<MiddleUserWxUser> {

}
