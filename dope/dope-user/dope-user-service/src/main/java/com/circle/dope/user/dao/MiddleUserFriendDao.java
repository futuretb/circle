package com.circle.dope.user.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.user.po.MiddleUserFriend;

/**
 * 用户版 - 关注粉丝表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 16:32:27'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface MiddleUserFriendDao extends BaseDao<MiddleUserFriend> {

}
