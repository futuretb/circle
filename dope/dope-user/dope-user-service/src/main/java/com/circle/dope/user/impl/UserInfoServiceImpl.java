package com.circle.dope.user.impl;

import com.circle.dope.user.dao.UserInfoDao;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.service.UserInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;

/**
 * 商家版用户信息表 - ServiceImpl
 *
 *  Created by dicksoy on '2018-12-26 10:43:23'.
 *  Email dicksoy@163.com
 */
@Service
public class UserInfoServiceImpl extends BaseServiceImpl<UserInfoDao, UserInfo>
	implements UserInfoService {

}
