package com.circle.dope.user.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.user.api.MiddleUserFriendApi;
import com.circle.dope.user.po.MiddleUserFriend;
import com.circle.dope.user.po.MiddleUserWxUser;
import com.circle.dope.user.po.UserConsignee;
import com.circle.dope.user.service.MiddleUserFriendService;
import com.circle.dope.user.service.MiddleUserWxUserService;
import com.circle.dope.user.service.UserConsigneeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 商家版用户信息表 - Controller
 *
 *  Created by dicksoy on '2018-12-24 17:37:24'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/user/middleUserFriend")
public class MiddleUserFriendController {

    @Resource
    private MiddleUserWxUserService middleUserWxUserService;

    @Resource
    private MiddleUserFriendService middleUserFriendService;

    @RequestMapping(value = "/findWxUserFriendOne", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findWxUserFriendOne(String userCode, String storeUserId) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", storeUserId);
        MiddleUserWxUser middleUserWxUser = middleUserWxUserService.findOne(params);
        params.clear();
        params.put("wxUserCode", userCode);
        params.put("friendWxUserCode", middleUserWxUser.getWxUserCode());
        MiddleUserFriend middleUserFriend = middleUserFriendService.findOne(params);
        AssertUtil.notNull(middleUserFriend);
        return SUCCESS.result(middleUserFriend);
    }

}
