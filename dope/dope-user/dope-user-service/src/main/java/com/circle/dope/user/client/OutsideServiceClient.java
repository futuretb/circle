package com.circle.dope.user.client;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.user.exception.OutsideServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "outside-service", path = "/outside", fallback = OutsideServiceHystric.class)
public interface OutsideServiceClient {

    @RequestMapping(value = "/image/generateUploadQrCode", method = RequestMethod.POST)
    BaseResult generateUploadQrCode(@RequestBody Map<String, Object> params);
}
