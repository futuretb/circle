package com.circle.dope.user.util;

import com.circle.dope.framework.constant.WxApi;
import com.circle.dope.framework.util.RequestUtil;
import com.circle.dope.user.exception.WxException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
@RefreshScope
public class WxUtil {

    @Value("${wxconfig.appid}")
    private String appid;
    @Value("${wxconfig.secret}")
    private String secret;

    public Map<String, Object> miniProgramOauthInfo(String code) {
        Map<String, Object> response = null;
        try {
            response = RequestUtil.doGet(WxApi.CODE_TO_SESSION(appid, secret, code));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        if (0 != Integer.valueOf(String.valueOf(response.get("errcode")))) {
//            throw new WxException((String) response.get("errmsg"));
//        }
        return response;
    }


}
