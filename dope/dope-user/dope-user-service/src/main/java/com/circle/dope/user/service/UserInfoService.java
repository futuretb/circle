package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.UserInfo;

/**
 * 商家版用户信息表 - Service
 *
 *  Created by dicksoy on '2018-12-26 10:43:23'.
 *  Email dicksoy@163.com
 */
public interface UserInfoService extends BaseService<UserInfo> {

}
