package com.circle.dope.user.impl;

import com.circle.dope.user.dao.MiddleUserWxUserDao;
import com.circle.dope.user.dao.UserInfoDao;
import com.circle.dope.user.dao.WxUserInfoDao;
import com.circle.dope.user.po.MiddleUserWxUser;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import com.circle.dope.user.service.MiddleUserWxUserService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;

import javax.annotation.Resource;

/**
 * 商家版 - 用户版 用户绑定关系表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 10:25:32'.
 *  Email dicksoy@163.com
 */
@Service
public class MiddleUserWxUserServiceImpl extends BaseServiceImpl<MiddleUserWxUserDao, MiddleUserWxUser>
		implements MiddleUserWxUserService {

	@Resource
	private WxUserInfoDao wxUserInfoDao;

	@Resource
	private UserInfoDao userInfoDao;

	@Override
	public Long insertMiddle(WxUserInfo wxUserInfo, UserInfo userInfo, MiddleUserWxUser middleUserWxUser) {
		if (null != wxUserInfo) {
			wxUserInfoDao.update(wxUserInfo);
		}
		if (null != userInfo) {
			userInfoDao.insert(userInfo);
		}
		if (null != middleUserWxUser) {
			mapper.insert(middleUserWxUser);
		}
		return wxUserInfo.getId();
	}
}
