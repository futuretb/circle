package com.circle.dope.user.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@RefreshScope
public class ImEasemobUtil {

    private final static Logger LOGGER = LoggerFactory.getLogger(ImEasemobUtil.class);

    @Value("${im.easemob.client.id}")
    private String id;
    @Value("${im.easemob.client.secret}")
    private String secret;
    @Value("${im.easemob.orgName}")
    private String orgName;
    @Value("${im.easemob.appName}")
    private String appName;

    public Map<String, Object> register(Map<String, String> im) throws RuntimeException {
        try {
            Map<String, Object> response = doPost(usersPath(), JSONObject.toJSONString(im));
            if (null == response || response.isEmpty() || !response.containsKey("entities")) {
                throw new RuntimeException(response + ": im用户创建失败, 返回结果异常");
            }
            List<HashMap> entities = JSONObject.parseArray(JSONObject.toJSONString(response.get("entities")), HashMap.class);
            if (1 != entities.size()) {
                throw new RuntimeException(response + ": im用户创建失败, 数据数量太多");
            }
            return entities.get(0);
        } catch (IOException e) {
            LOGGER.info("IOException: {}", e);
            throw new RuntimeException(e);
        }
    }

    private String usersPath() {
        return "https://a1-vip5.easemob.com/" + orgName + "/" + appName + "/users";
    }

    public static Map<String, Object> doPost(String url, String data) throws ClientProtocolException, IOException {
        CloseableHttpClient hc = HttpClients.createDefault();
        CloseableHttpResponse hr = null;
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(data));
        Header header = new BasicHeader("Content-Type", "application/json");
        Header accept = new BasicHeader("Accept", "application/json");
        httpPost.addHeader(header);
        httpPost.addHeader(accept);
        try {
            hr = hc.execute(httpPost);
            HttpEntity resEntity = hr.getEntity();
            String entity = EntityUtils.toString(resEntity, "utf-8");
            System.out.println(entity);
            int statusCode = hr.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                return JSONObject.parseObject(entity, HashMap.class);
            }
        } catch (Exception e0) {
            e0.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(hr);
            HttpClientUtils.closeQuietly(hc);
        }
        return null;
    }
}
