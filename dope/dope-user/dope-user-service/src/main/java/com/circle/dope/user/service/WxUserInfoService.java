package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.WxUserInfo;

/**
 * 用户版用户信息表 - Service
 *
 *  Created by dicksoy on '2018-12-26 10:43:16'.
 *  Email dicksoy@163.com
 */
public interface WxUserInfoService extends BaseService<WxUserInfo> {

}
