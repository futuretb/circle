package com.circle.dope.user.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.user.po.MiddleUserFriend;

/**
 * 用户版 - 关注粉丝表 - Service
 *
 *  Created by dicksoy on '2019-01-06 16:32:27'.
 *  Email dicksoy@163.com
 */
public interface MiddleUserFriendService extends BaseService<MiddleUserFriend> {

}
