package com.circle.dope.user.enums;

public enum LOGIN_TYPE {

    WECHAT("微信"),
    ACCOUNT("账号密码"),
    MOBILE("手机号验证码"),
    MINI_PROGRAM("小程序");

    private String cnName;

    LOGIN_TYPE(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }
}
