package com.circle.dope.user.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  用户版用户信息表 - Po
 *
 *  Created by dicksoy on '2019-01-22 14:58:35'.
 *  Email dicksoy@163.com
 */
public class WxUserInfo extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 环信id
     */
    private String imId;

    /**
     * 等级 1 会员 0 普通用户
     */
    private Integer level;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 手机类型 IOS andriod
     */
    private String mobileType;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户二维码信息
     */
    private String qrCode;

    /**
     * 个性签名
     */
    private String signature;

    /**
     * 0 小程序  1 微信相册(微信开发平台登陆) 2 微信服务号授权
     */
    private Integer type;

    /**
     * 微信夸平台唯一标识符
     */
    private String unionId;

    /**
     * 用户code
     */
    private String wxUserCode;

    /**
     * 微信账号
     */
    private String wxAccount;

    /**
     * 邀请码
     */
    private String invitedCode;

    /**
     * NORMAL 正常 DELETE 删除
     */
    private String status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getImId() {
        return imId;
    }

    public void setImId(String imId) {
        this.imId = imId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobileType() {
        return mobileType;
    }

    public void setMobileType(String mobileType) {
        this.mobileType = mobileType;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getWxUserCode() {
        return wxUserCode;
    }

    public void setWxUserCode(String wxUserCode) {
        this.wxUserCode = wxUserCode;
    }

    public String getWxAccount() {
        return wxAccount;
    }

    public void setWxAccount(String wxAccount) {
        this.wxAccount = wxAccount;
    }

    public String getInvitedCode() {
        return invitedCode;
    }

    public void setInvitedCode(String invitedCode) {
        this.invitedCode = invitedCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}