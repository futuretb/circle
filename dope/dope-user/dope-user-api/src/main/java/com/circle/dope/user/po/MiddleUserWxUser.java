package com.circle.dope.user.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  商家版 - 用户版 用户绑定关系表 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:19:00'.
 *  Email dicksoy@163.com
 */
public class MiddleUserWxUser extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 商家版用户id
     */
    private String userId;

    /**
     * 用户版用户code
     */
    private String wxUserCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWxUserCode() {
        return wxUserCode;
    }

    public void setWxUserCode(String wxUserCode) {
        this.wxUserCode = wxUserCode;
    }

}