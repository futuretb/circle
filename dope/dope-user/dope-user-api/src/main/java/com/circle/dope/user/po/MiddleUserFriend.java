package com.circle.dope.user.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  用户版 - 关注粉丝表 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:19:19'.
 *  Email dicksoy@163.com
 */
public class MiddleUserFriend extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 关注人
     */
    private String wxUserCode;

    /**
     * 被关注人
     */
    private String friendWxUserCode;

    /**
     * 是否设置为代理
     */
    private Integer isAgent;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWxUserCode() {
        return wxUserCode;
    }

    public void setWxUserCode(String wxUserCode) {
        this.wxUserCode = wxUserCode;
    }

    public String getFriendWxUserCode() {
        return friendWxUserCode;
    }

    public void setFriendWxUserCode(String friendWxUserCode) {
        this.friendWxUserCode = friendWxUserCode;
    }

    public Integer getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Integer isAgent) {
        this.isAgent = isAgent;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

}