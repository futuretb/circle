package com.circle.dope.user.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  收/发件人信息 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:18:54'.
 *  Email dicksoy@163.com
 */
public class UserConsignee extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 区
     */
    private String area;

    /**
     * 城市
     */
    private String city;

    /**
     * 内容
     */
    private String content;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 联系人姓名
     */
    private String name;

    /**
     * 联系人省份
     */
    private String province;

    /**
     * 街道
     */
    private String street;

    /**
     * SENDER 发件人 RECEIVER 收件人
     */
    private String type;

    /**
     * 买家usercode
     */
    private String wxUserCode;

    /**
     * 卖家userid
     */
    private String userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWxUserCode() {
        return wxUserCode;
    }

    public void setWxUserCode(String wxUserCode) {
        this.wxUserCode = wxUserCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}