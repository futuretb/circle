package com.circle.dope.user.enums;

public enum USER_INFO_STATUS {

    INIT("初始"),
    NORMAL("正常"),
    SET_UP("开店"),
    LOCK("锁定"),
    LOGOUT("注销"),
    STOP("停用"),
    DELETE("删除");

    private String cnName;

    USER_INFO_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }
}
