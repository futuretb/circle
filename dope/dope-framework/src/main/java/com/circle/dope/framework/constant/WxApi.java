package com.circle.dope.framework.constant;

public class WxApi {

    public static final String APP_ACCESS_TOKEN(String appid, String secret, String code) {
        return "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid
                + "&secret=" + secret + "&code=" + code + "&grant_type=authorization_code";
    }

    public static final String CODE_TO_SESSION(String appid, String secret, String jscode) {
        return "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid
                + "&secret=" + secret + "&js_code=" + jscode
                + "&grant_type=authorization_code";
    }
}
