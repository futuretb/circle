package com.circle.dope.framework.base;

import static com.circle.dope.framework.base.BaseResult.Code.*;
import static com.circle.dope.framework.base.BaseResult.Message.*;

public class BaseResult {

    private Integer code;
    private String msg;
    private Object result;

    public static BaseResult SUCCESS = BaseResult.builder().code(OK).msg(DEFAULT_SUCCESS_MGS);
    public static BaseResult FAILD = BaseResult.builder().code(ERROR).msg(DEFAULT_SERVER_ERROR_MSG);
    public static BaseResult FAILD_UN_AUTH = BaseResult.builder().code(UN_AUTH).msg(UN_AUTH_CODE_ERROR_MSG);

    public interface Message {
        String USER_NOT_EXIST = "用户不存在";
        String DEFAULT_SERVER_ERROR_MSG = "对不起服务出现异常";
        String INNER_REQUEST_ERROR = "内部请求错误";
        String DEFAULT_SUCCESS_MGS = "请求操作API成功";
        String DEFAULT_VALI_MSG = "传入数据不合法";
        String UN_AUTH_CODE_ERROR_MSG = "未登陆";
    }

    public interface Code {
        int OK = 200;
        int ERROR = 500;
        int UN_AUTH = 501;
    }

    public static BaseResult builder() {
        return new BaseResult();
    }

    public BaseResult code(Integer code) {
        this.code = code;
        return this;
    }

    public BaseResult msg(String msg) {
        this.msg = msg;
        return this;
    }

    public BaseResult result(Object result) {
        this.result = result;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }


}
