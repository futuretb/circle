package com.circle.dope.framework.util;

import com.alibaba.fastjson.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BeanUtil {

    public static Map<String, Object> beanToMap(Object object) {
        AssertUtil.notNull(object);
        return JSONObject.parseObject(JSONObject.toJSONString(object), Map.class);
    }

    public static void copeProperties(Object source, Object target) {
        AssertUtil.notNull(source);
        AssertUtil.notNull(target);
        Map<String, Object> map = JSONObject.parseObject(JSONObject.toJSONString(source), Map.class);
        mapToBean(map, target);
    }

    public static void mapToBean(Map<String, Object> map, Object object) {
        AssertUtil.notNull(map);
        AssertUtil.notNull(object);
        List<String> methodNameList = new ArrayList<String>();
        for (String key: map.keySet()) {
            methodNameList.add("set" + firstUpperCase(key));
        }
        for (Method method: object.getClass().getMethods()) {
            String methodName = method.getName();
            if (methodNameList.contains(methodName)) {
                try {
                    method.invoke(object, map.get(firstLowerCase(methodName.substring(3, methodName.length()))));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static String firstLowerCase(String name) {
        return name.substring(0, 1).toLowerCase() + name.substring(1, name.length());
    }

    private static String firstUpperCase(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
    }
}
