package com.circle.dope.framework.base;

import java.util.List;
import java.util.Map;

/**
 * Dao继承
 * @author Dicksoy
 * @email dicksoy@163.com
 * @version
 */
public interface BaseDao<T> {

	/**
     * 新增
     */
    void insertByCondition(T t);

    /**
     * 新增 - 全部字段更新
     */
    void insert(T t);

	/**
     * 更新
     */
    int updateByCondition(T t);

    /**
     * 更新 - 全部字段更新
     */
    int update(T t);

    /**
     * 删除
     */
    int deleteById(Long id);
    
    /**
     * 根据id查询
     */
    T findById(Long id);

    /**
     * 条件查询List
     */
	List<T> findList(Map<String, Object> params);

    /**
     * 条件查询数量
     */
    Integer findTotal(Map<String, Object> params);
    
	/**
	 * 分页查询
	 */
	List<T> findListByPage(Map<String, Object> params);
}
