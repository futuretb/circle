//package com.circle.dope.framework.pay.demo;
//
//import com.alipay.api.AlipayClient;
//import com.alipay.api.DefaultAlipayClient;
//
//public class AlipayClientConf{
//    static String serverUrl = "https://openapi.alipay.com/gateway.do";
//    static String appId = "2016082901820424";
//    static String appPrivateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQClV0CsBPdIiQspKx9EbEc8G0J4txh7V/q80OV7TnXOo0Apzly6cpcOLtX9hspe0KJ2hs9unY0nLLGYTy4sxeB/KNoYGNt3H7s5vlMeFYbIZtMV++s7bMA44UlUmfxf6wAE1bVeUyRIgJRu0niOX5E8OEq0ctzcAAU+ti339WCOnUXZCl6WP4XdPWQa9JIOmKfHUA9xY2tjg6GmnowrtdAd4keLzBAgCNCDh0OIedBCWMWiXrbIJp83iSULHQtzMhhNJvXFKoo3DoE7eo19Raw3Rt55a/Ej+0KxRJDbNydJ5aYs3lJd5xdP8uvhCHBwvcNhE3W2zxkOE8TWRobwby7VAgMBAAECggEADPKEMfRBmQCCSzsBO85u1Zoc3m8YD7CYBeklzz4Zo5DNGXj7ce4/PfEU+MFxX3NfBYERBUx5HTQ2xAKPTxo/tjVwvqpTpURq2N66i4SMBW1n4jTT+tdNRib+Iw3b4Xb5jWdeyIAiOzs2ZGc+4YdCTEDsOK019lRaFMsXVm90nDY8Y8ffSmQMmhyQvBPQsb6nK/207g9S+MMUB8/oA/KMqKrXbKRqruKY+fTFzwNAtWW5apeeMtVoBOe4shDAnkVqE6jZ0gHSyeWIayI6BOlfCOZWEx8srnNgBoCVYQ4xjfflwUAqnGoGLDWNtMX5RY9wfVNxA2MnT3hGY1TcC9GCgQKBgQDRr0kCHcFmjrgLWjOi4nUe8Dmiu4H8+xRbktdPWgVu0RC4Sv8PfQ7+c57+/5qmJXfKOeDooFihl+hpq5pcQZ11K6C08ku4Fh6Cj44YWOfumyATU4RIAR7b90pc4WFkTd4DfGkPkXkC4dlDQhmzUiAUqF5333JyAaVaPemKOVzpDQKBgQDJ3IXyZ/bjvQDqLhHn6VP5pML5ZHVuzX7MN+WTOVFfWARvjaDwgmNhTHpWTcHY3ucG9xr/UVO8D8cpbQGnD8Ic/dyTr8U9sMjFX17ugC50RLAfej6qpr++oGgmuZteNmudcbWEam2NPDsVpiG3jlGO9Ssyk4M6LC15x32Kv5La6QKBgFue16vRDJyfZ9OuCjn18W8fUE/G1rmPjAmnU+UcVJtjRocAXgqKfm64XpTVYuGOJZV7TMbYqWGSWr8xeDUvhnmYIpRt+p/pKlOqHlCNKGqd5ZoMOBSuSKmcmCWVZVTKoWOsjMt8qP+VZ7V3wHsWO2ZLW1vNitg9195ihxUak3pRAoGAR8Kvk4dGC5vrMVUDZJBcHAlojsQZwzQmeW79nCLqPJzLpuSQbC/OKI7JhtjJqtFDDATYgh9lRxvMz3tC9SLy/N96hL4dvlqUZSYMV3kWCptIiUgp8vyoeZoIrIubIhjylNwRjWtQWr8ouf9/SQO9FrctjiNGqW1/FSXICYUlLdECgYAMohh7hK0C8CMJ+HpU0dttM9rcPgH0Q4WejKha1mrZjnI/X/h8Sv+aLXCiGyf5+oWF8gLGzWc7IXpCgnXDTzwH4k/lFocNKhouVEmpbByWIdJGIQQGZvgc9UFEzIs8q2kSv+Wpe6vB8nON4NG0s/7OFtQ75tCrQKp6wN2BGOCddg==";
//    static String format = "json";
//    static String charset = "UTF-8";
//    static String alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjnVEUGRRty/nmDD1nCc2SOLRL5z/ouctWbfYUiGml+yJRxhzh926jsw2W6/btiNzIXGEniWurX9NBI6uWjBgLBC+0eArYlT3Wr/r8bWDMpZWxXEXvZNB4fX/OiTMC7tIMJ6FMAYr0EGduQEjJSxBrn41Gmsno/LS4rndYsh+HwEE4qVzz+zmDGZB0JANsPcneWVtB8TdCio8BYdubGByf2rMNBMS50AHhd1/DFANPmB4E6IgpUngEVNnnalff/XbH1NO4bEQdUsouDEyZvc4UMI3UccZeoJEy9kIb4gDfGN3hlR98ASbn7h5T7UHlOxIqXDlzkAYicMYF2cu6RV1IQIDAQAB";
//    static String signType = "RSA2";
//
//    private static final AlipayClient alipayClient = new DefaultAlipayClient(serverUrl, appId, appPrivateKey, format, charset, alipayPublicKey, signType);
//
//    private AlipayClientConf(){
//
//    }
//    public static AlipayClient getAlipayclient() {
//        return alipayClient;
//    }
//
//
//}
