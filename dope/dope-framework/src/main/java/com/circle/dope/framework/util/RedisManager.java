package com.circle.dope.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * Redis操作类  --- 需要可以扩展
 * @author Dicksoy
 * @date 2018年12月12日 下午3:35:48
 * @email dicksoy@163.com
 * @version
 */
public class RedisManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedisManager.class); 
	
	private JedisPool jedisPool;

	private RedisLock redisLock;
	
	public RedisManager(JedisPool jedisPool){
		init(jedisPool);
	}

	/**
	 * 初始化方法
	 */
	public void init(JedisPool jedisPool){
		if(jedisPool == null){
			LOGGER.info("JedisPool == null");
			return;
		}
		this.jedisPool = jedisPool;
		if (null == redisLock) {
			this.redisLock = new RedisLock(jedisPool);
		}
	}
	
	private void closeJedis(Jedis jedis) {
		if (null != jedis) {
			jedis.close();
		}
	}

	public String get(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.get(key);
		}finally{
			closeJedis(jedis);
		}
	}

	public String set(String key, String value) {
	    return set(key, value, 0);
    }

	public String set(String key, String value, int seconds) {
		Jedis jedis = jedisPool.getResource();
		try{
			jedis.set(key, value);
			if(seconds != 0){
				jedis.expire(key, seconds);
			}
			return value;
		}finally{
			closeJedis(jedis);
		}
	}

	public void del(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			jedis.del(key);
		}finally{
			closeJedis(jedis);
		}
	}

	/**
	 * flush
	 */
	public void flushDB(){
		Jedis jedis = jedisPool.getResource();
		try{
			jedis.flushDB();
		}finally{
			closeJedis(jedis);
		}
	}

	/**
	 * size
	 */
	public Long dbSize(){
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.dbSize();
		}finally{
			closeJedis(jedis);
		}
	}

	/**
	 * keys
	 * @return
	 */
	public Set<String> keys(String pattern){
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.keys(pattern);
		}finally{
			closeJedis(jedis);
		}
	}

	public String lpop(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.lpop(key);
		}finally{
			closeJedis(jedis);
		}
	}


	public String rpop(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.rpop(key);
		}finally{
			closeJedis(jedis);
		}
	}

	public void rpush(String key, String... values) {
		Jedis jedis = jedisPool.getResource();
		try{
			jedis.rpush(key, values);
		}finally{
			closeJedis(jedis);
		}
	}

	public boolean exists(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.exists(key);
		}finally{
			closeJedis(jedis);
		}
	}

	public Long incrBy(String key, long i) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.incrBy(key, i);
		}finally{
			closeJedis(jedis);
		}
	}

	public Long setnx(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.setnx(key, value);
		}finally{
			closeJedis(jedis);
		}
	}

	public Long incr(String key) {
		Jedis jedis = jedisPool.getResource();
		try{
			return jedis.incr(key);
		}finally{
			closeJedis(jedis);
		}
	}

	public boolean lockFree(String key, long millisecond) {
		return redisLock.lockFree(key, millisecond);
	}

	public boolean unlockFree(String key) {
		return redisLock.unlockFree(key);
	}

	public JedisPool getJedisPool() {
		return jedisPool;
	}

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

}
