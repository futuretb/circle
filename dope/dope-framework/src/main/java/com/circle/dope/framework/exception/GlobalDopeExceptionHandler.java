package com.circle.dope.framework.exception;

import com.circle.dope.framework.base.BaseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.circle.dope.framework.base.BaseResult.FAILD;

public class GlobalDopeExceptionHandler extends DefaultErrorController {

    protected final Logger LOGGER = LoggerFactory.getLogger(GlobalDopeExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BaseResult processException(Exception exception) {
        LOGGER.info("Exception : {}", exception);
        return FAILD.result(exception.getMessage());
    }

}

