package com.circle.dope.framework.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchUtil {

    private static final String MOBILE_REGEX = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[0135678])|(18[0-9]))\\d{8}$";

    public static void matchMobile(String mobile) {
        if (mobile.length() != 11) {
            throw new IllegalArgumentException("手机号应为11位数");
        } else {
            Pattern pattern = Pattern.compile(MOBILE_REGEX);
            Matcher matcher = pattern.matcher(mobile);
            boolean isMatch = matcher.matches();
            if (!isMatch) {
                throw new IllegalArgumentException("手机号格式不正确");
            }
        }
    }
}
