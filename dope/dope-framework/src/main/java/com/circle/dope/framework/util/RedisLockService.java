package com.circle.dope.framework.util;

public interface RedisLockService {

    public void lock(String redisKey) throws Exception ;

    public boolean unlock(final String name) throws Exception ;

    public boolean unlockAll() throws Exception ;

    public boolean acquireLock(final String name) throws Exception ;

    public void releaseLock(final String name) throws Exception ;

    public long locks(String redisKey) throws Exception ;

    /**
     * 自由锁
     * @param key
     * @param millisecond
     * @return
     */
    public boolean lockFree(String key, long millisecond);

    /**
     * 自由锁
     * 默认3秒自动释放锁
     * @param key
     * @return
     */
    public boolean lockFree(String key);

    /**
     * 解自由锁
     * @param key
     * @return
     */
    public boolean unlockFree(String key);
}
