package com.circle.dope.framework.base;

import java.util.Map;

/**
 * Service继承
 * @author Dicksoy
 * @email dicksoy@163.com
 * @version
 */
public interface BaseService<T> {

	/**
     * 新增 - 全部字段更新
     */
    Long insert(T t);

    /**
     * 新增
     */
    Long insertByCondition(T t);

	/**
     * 更新 - 全部字段更新
     */
    boolean update(T t);

    /**
     * 更新
     */
    boolean updateByCondition(T t);

    /**
     * 删除
     */
    boolean deleteById(Long id);

    /**
     * 根据id查询
     */
    T findById(Long id);

    /**
     * 条件查询List
     */
    ListResult<T> findList(Map<String, Object> params);

    /**
     * 条件查询数量
     */
    Integer findTotal(Map<String, Object> params);
    
	/**
	 * 分页查询
	 */
	PageResult<T> findListByPage(Map<String, Object> params);

    /**
     * 指定条件查询唯一
     */
    T findOne(Map<String, Object> params);
}
