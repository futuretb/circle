package com.circle.dope.framework.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Hashtable;

public class QRCodeUtil {

    private static int ONCOLOR = 0xFF000000; // 前景色
    private static int OFFCOLOR = 0xFFFFFFFF;

    public static void generateQRImage(String txt, int size, OutputStream out, ErrorCorrectionLevel level) {
        generateQRImage(txt, size, null, null, out, level);
    }

    public static void generateQRImage(String txt, int size, OutputStream out) {
        generateQRImage(txt, size, out, ErrorCorrectionLevel.M);
    }

    public static void generateQRImageWithColor(String txt, int size, int color, OutputStream out) {
        generateQRImage(txt, size, color, null, out, ErrorCorrectionLevel.M);
    }

    public static void generateQRImageWithColorLogo(String txt, int size, int color, InputStream logoIn,
                                                    OutputStream out) {
        generateQRImage(txt, size, color, logoIn, out, ErrorCorrectionLevel.M);
    }

    public static void generateQRImageWithLogo(String txt, int size, InputStream logoIn, OutputStream out) {
        generateQRImage(txt, size, null, logoIn, out, ErrorCorrectionLevel.M);
    }

    public static void generateQRImageWithLogo(String txt, int size, byte[] logoData, OutputStream out) {
        InputStream logoIn = new ByteArrayInputStream(logoData);
        try {
            generateQRImage(txt, size, null, logoIn, out, ErrorCorrectionLevel.M);
        } finally {
            IOUtils.closeQuietly(logoIn);
        }
    }

    public static void generateQRImage(String txt, int size, Integer frontColor, InputStream logoIn, OutputStream out,
                                       ErrorCorrectionLevel level) {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, level);
        // 指定编码格式
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        // 设置白边
        hints.put(EncodeHintType.MARGIN, 1);
        try {
            int fColor = 0;
            if (frontColor == null) {
                fColor = ONCOLOR;
            } else {
                fColor = frontColor;
            }
            MatrixToImageConfig config = new MatrixToImageConfig(fColor, OFFCOLOR);
            BitMatrix bitMatrix = new MultiFormatWriter().encode(txt, BarcodeFormat.QR_CODE, size, size, hints);
            if (logoIn == null) {
                MatrixToImageWriter.writeToStream(bitMatrix, "png", out, config);
            } else {
                writeLogo(bitMatrix, logoIn, out);
            }
        } catch (Exception e) {
        }
    }

    public static BufferedImage generateQRImage(String txt, int size, ErrorCorrectionLevel level)
            throws WriterException {
        Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, level);
        // 指定编码格式
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        // 设置白边
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(txt, BarcodeFormat.QR_CODE, size, size, hints);
        return toBufferedImage(bitMatrix);
    }

    private static BitMatrix updateBit(final BitMatrix matrix, final int margin) {
        int tempM = margin * 2;
        //获取二维码图案的属性
        int[] rec = matrix.getEnclosingRectangle();
        int resWidth = rec[2] + tempM;
        int resHeight = rec[3] + tempM;
        // 按照自定义边框生成新的BitMatrix
        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
        resMatrix.clear();
        //循环，将二维码图案绘制到新的bitMatrix中
        for (int i = margin; i < resWidth - margin; i++) {
            for (int j = margin; j < resHeight - margin; j++) {
                if (matrix.get(i - margin + rec[0], j - margin + rec[1])) {
                    resMatrix.set(i, j);
                }
            }
        }
        return resMatrix;
    }

    public static void writeLogo(BitMatrix matrix, InputStream logoin, OutputStream out) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        Graphics2D gs = image.createGraphics();
        int ratioWidth = image.getWidth() * 2 / 10;
        int ratioHeight = image.getHeight() * 2 / 10;
        // 载入logo
        Image img = ImageIO.read(logoin);
        int logoWidth = img.getWidth(null) > ratioWidth ? ratioWidth : img.getWidth(null);
        int logoHeight = img.getHeight(null) > ratioHeight ? ratioHeight : img.getHeight(null);

        int x = (image.getWidth() - logoWidth) / 2;
        int y = (image.getHeight() - logoHeight) / 2;

        gs.drawImage(img, x, y, logoWidth, logoHeight, null);
        gs.setColor(Color.black);
        gs.setBackground(Color.WHITE);
        gs.dispose();
        img.flush();
        ImageIO.write(image, "png", out);
    }

    public static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? ONCOLOR : OFFCOLOR);
            }
        }
        return image;
    }

    public static void main(String[] args) throws Exception {

        FileOutputStream fos = new FileOutputStream("E:/test.png");
        FileInputStream in = new FileInputStream(new File("E:\\b.png"));
        generateQRImageWithLogo("http://www.51zan.com", 200,  in, fos);
        fos.flush();
        fos.close();


    }
}
