package com.circle.dope.framework.pay.demo;

import com.circle.dope.framework.pay.wxpay.IWXPayDomain;
import com.circle.dope.framework.pay.wxpay.WXPayConfig;
import com.circle.dope.framework.pay.wxpay.WXPayDomainSimpleImpl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class WXPayClientConf extends WXPayConfig {

    private byte[] certData;
    private static WXPayClientConf INSTANCE;

    private WXPayClientConf() throws Exception{
        String certPath = "D://wechartpay/busi/apiclient_cert.p12";
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    public static WXPayClientConf getInstance() throws Exception{
        if (INSTANCE == null) {
            synchronized (WXPayClientConf.class) {
                if (INSTANCE == null) {
                    INSTANCE = new WXPayClientConf();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public String getAppID(String payModel) {
        if (payModel == null) {
            return "wx0c72ec145cf44240";
        }else if (payModel.equals("APP")){
            return "wx0c72ec145cf44240";
        }else if (payModel.equals("JSAPI")) {
            return "wxab3681a81de6cc81";
        }else if (payModel.equals("货圈circle-动态购物")){
            return "wx0c72ec145cf44240";
        } else if (payModel.equals("小程序货圈circle-动态购物")) {
            return "wxab3681a81de6cc81";
        }
        return "wx0c72ec145cf44240";
    }


    @Override
    public String getAppID() {
        return "wxab3681a81de6cc81";
    }

    public String getMchID() {
        return "1489151522";
    }

    public String getKey() {
        return "YL25lxBNxYM1fkoe6G5MhOImg45gdL8W";
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    public IWXPayDomain getWXPayDomain() {
        return WXPayDomainSimpleImpl.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }
}
