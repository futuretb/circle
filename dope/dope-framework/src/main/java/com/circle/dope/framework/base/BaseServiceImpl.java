package com.circle.dope.framework.base;

import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * 使用ServiceImpl实现该类<Dao需要继承BaseMapper>
 * @author Dicksoy
 * @email dicksoy@163.com
 * @version
 */
public class BaseServiceImpl<M extends BaseDao<T>, T> {

	@Autowired
	protected M mapper;

	public Long insert(T t) {
		mapper.insert(t);
		return getId(t);
	}

	public Long insertByCondition(T t) {
		mapper.insertByCondition(t);
		return getId(t);
	}

	public boolean update(T t) {
		return mapper.update(t) > 0;
	}

	public boolean updateByCondition(T t) {
		return mapper.updateByCondition(t) > 0;
	}

	public boolean deleteById(Long id) {
		return mapper.deleteById(id) > 0;
	}

	public T findOne(Map<String, Object> params) {
		ListResult<T> list = findList(params);
		if (list != null && null != list.getList() && !list.getList().isEmpty()) {
			return list.getList().get(0);
		}
		return null;
	}
	
	public T findById(Long id) {
		return mapper.findById(id);
	}

	public ListResult<T> findList(Map<String, Object> params) {
		List<T> list = mapper.findList(params);
		if (null != list && !list.isEmpty()) {
			return new ListResult<T>(list);
		}
		return null;
	}

	public PageResult findListByPage(Map<String, Object> params) {
		PageResult pageResult = new PageResult(params);
		pageResult.setList(mapper.findListByPage(params));
		pageResult.setTotalCount(mapper.findTotal(params));
		return pageResult;
	}

	public Integer findTotal(Map<String, Object> params) {
		return mapper.findTotal(params);
	}

	private Long getId(T t) {
		try {
			return (Long) t.getClass().getMethod("getId", null).invoke(t, null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
}
