package com.circle.dope.framework.base;

import java.io.Serializable;

public class BasePo implements Serializable {

    public static final long serialVersionUID = 13L;

    private Object otherData;

    public Object getOtherData() {
        return otherData;
    }

    public void setOtherData(Object otherData) {
        this.otherData = otherData;
    }
}
