package com.circle.dope.framework.base;

import com.alibaba.fastjson.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD;

/**
 * 基础熔断
 * 使用类似Slf4j
 */
public class BaseHystric {

    private final static String FEIGN_CLASS_NAME = "org.springframework.cloud.openfeign.FeignClient";

    private Class<?> clazz;

    private BaseHystric(Class<?> clazz) {
        this.clazz = clazz;
    }

    public static BaseHystric newInstance(Class<?> clazzParams) {
        return new BaseHystric(clazzParams);
    }

    public BaseResult defaultMessage() {
        Map<String, Object> result = new HashMap<>();
        for (Class inter: this.clazz.getInterfaces()) {
            for (Annotation anno: inter.getAnnotations()) {
                if (FEIGN_CLASS_NAME.equals(anno.annotationType().getName())) {
                    result.put("Class", inter.getName());
                    for (Method method: anno.annotationType().getMethods()) {
                        if ("name".equals(method.getName())) {
                            try {
                                result.put("Service", String.valueOf(method.invoke(anno, null)));
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        result.put("MethodName", new Exception().getStackTrace()[1].getMethodName());
        return FAILD.msg(JSONObject.toJSONString(result));
    }
}
