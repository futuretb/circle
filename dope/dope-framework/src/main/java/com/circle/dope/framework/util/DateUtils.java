package com.circle.dope.framework.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 时间转化方法集
 * @author hanxiao
 * @date 2014-04-19
 */
public class DateUtils {

	private static String str1 = "yyyy-MM-dd HH:mm:ss";
	private static String str2 = "yyyy-MM-dd";


	/**
	 * @Title: getNewDateStr1
	 * @Description: 获取当前时间
	 * @return String    返回类型
	 */
	public static String getNewDateStr1(){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(str1);
		String str = sdf.format(date);
		return str;
	}

	/**
	 * @Title: getNewDate
	 * @Description: 获取当前时间
	 * @return Date    返回类型
	 */
	public static Date getNewDate(){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(str1);
		String str = sdf.format(date);
		return str1DateCommon(str);
	}
	
	/**
	 * @Title: getWeek 
	 * @Description: 获取当月周数
	 * @return int    返回类型
	 */
	 public static int getWeek(String str) throws Exception{
	     SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");  
	     Date date =sdf.parse(str);  
	     Calendar calendar = Calendar.getInstance();  
	     calendar.setTime(date);  
	     //第几周  
	     int week = calendar.get(Calendar.WEEK_OF_MONTH);  
	     //第几天，从周日开始  
	     int day = calendar.get(Calendar.DAY_OF_WEEK);  
	     return week;  
	 } 
	
	
	/**
	 * 将字符串转换成指定日期格式
	 * @param time 时间
	 * @param pattern 格式
	 * @return
	 */
	public static Date DateCommon(String time, String pattern){
		Date date = null;
		try {
			date = new Timestamp(new SimpleDateFormat(pattern).parse(time).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}
	
	/**
	 * 将字符串转换成指定日期格式(yyyy-MM-dd HH:mm:ss)
	 * @return Date
	 */
	public static Date str1DateCommon(String str){
		Date date = null;
		try {
			date = new Timestamp(new SimpleDateFormat(str1).parse(str).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}
	
	/**
	 * 将字符串转换成 (yyyy-MM-dd) 日期
	 * @param 
	 * @return date
	 */
	public static Date strFormatDate2(String str){
		Date date = null;
		try {
			date = new Timestamp(new SimpleDateFormat(str2).parse(str).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	
	/**
	 * 将当前传进来的日期转换成  (yyyy-MM-dd HH:mm:ss)  类型字符串
	 * @param date
	 * @return String
	 */
	public static String dateFormatStr1(Date date){
		return  new SimpleDateFormat(str1).format(date);
	}
	
	

	/**
	 * 获取当天开始时间 (yyyy-MM-dd 00:00:00)
	 * @param str
	 * @return
	 */
	public static Date startDate(String str){
		Date date = null;
		try {
			str = str + " 00:00:00";
			date = new Timestamp(new SimpleDateFormat(str1).parse(str).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}
	
	/**
	 * 获取当天结束时间 (yyyy-MM-dd 23:59:59)
	 * @param str
	 * @return
	 */
	public static Date endDate(String str){
		Date date = null;
		try {
			str = str + " 23:59:59";
			date = new Timestamp(new SimpleDateFormat(str1).parse(str).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		return date;
	}

	/**
	 * 计算date和当前日期相差的天数
	 * @param date
	 * @return
	 */
	public static long computeBetweenDay(Date date){
		return new Date().getTime()/86400000 - date.getTime()/86400000;
	}
	
	/**
	 * 计算date1和date2日期相差的分钟数
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long computeBetweenMin(Date date1, Date date2){
		return date2.getTime()/60000 - date1.getTime()/60000;
	}
	
	/**
	 * 计算date1和date2日期相差的秒数
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long computeBetweenMiu(Date date1, Date date2){
		return date2.getTime()/1000 - date1.getTime()/1000;
	}
	
	/**
	 * 获得指定日期的后一天
	 * 
	 * @param specifiedDay
	 * @return
	 */
	public static String getSpecifiedDayAfter(String specifiedDay) {
		Calendar c = Calendar.getInstance();
		Date date = null;
		try {
			date = new SimpleDateFormat(str1).parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + 1);

		String dayAfter = new SimpleDateFormat(str1)
				.format(c.getTime());
		return dayAfter;
	}


	/**
	 * 获取明天时间
	 * @return
	 */
	public static String getTomorrow() {

		return getSpecifiedDayAfter(getNewDateStr1());
	}

	/**
	 * 获取一个月后的日期
	 * @param
	 * @return
	 */
	public static long getSpecifiedMonthAfter(){ 
		Calendar c = Calendar.getInstance(); 
		c.add(Calendar.MONTH, 1);
		return c.getTime().getTime();
	}
	
	/**
	 * 获取当前月的第一天
	 * @return
	 */
	public static String getFirstDate(){
		 Calendar calendar = Calendar.getInstance();
		 calendar.set(Calendar.DATE,1);
		return new SimpleDateFormat(str1).format(calendar.getTime());
	}
	/**
	 * 获取当前月的最后一天
	 * @return
	 */
	public static String getLastDate(){
		Calendar calendar = Calendar.getInstance();
        int max = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DATE,max);
		return new SimpleDateFormat(str1).format(calendar.getTime());
	}

	 /**
     * 某一个月第一天和最后一天
     * @param date
     * @return
     */
	public static Map<String, String> getFirstdayLastdayMonth(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Date theDate = calendar.getTime();
        
        //某个月的第一天
        GregorianCalendar gcLast = (GregorianCalendar) Calendar.getInstance();
        gcLast.setTime(theDate);
        gcLast.set(Calendar.DAY_OF_MONTH, 1);
        String day_first = df.format(gcLast.getTime());
        StringBuffer str = new StringBuffer().append(day_first).append(" 00:00:00");
        day_first = str.toString();

        //某个月的最后一天
        calendar.add(Calendar.MONTH, 1);    //加一个月
        calendar.set(Calendar.DATE, 1);        //设置为该月第一天
        calendar.add(Calendar.DATE, -1);    //再减一天即为上个月最后一天
        String day_last = df.format(calendar.getTime());
        StringBuffer endStr = new StringBuffer().append(day_last).append(" 23:59:59");
        day_last = endStr.toString();

        Map<String, String> map = new HashMap<String, String>();
        map.put("firstDate", day_first);
        map.put("lastDate", day_last);
        return map;
    }

	/**
	 * 获取本周第一天数据
	 * @return
	 */
	public static String getFirstWeek() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); 
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		String day_last = df.format(cal.getTime());
		return day_last;
   }

}
