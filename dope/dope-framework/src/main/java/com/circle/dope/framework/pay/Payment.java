package com.circle.dope.framework.pay;

public interface Payment {

    /**
     * 支付
     * @param request
     * @return
     */
    PayResponse pay(PayRequest request);
}
