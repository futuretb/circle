package com.circle.dope.framework.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class PageResult<T> implements Serializable {

    public final static String PAGE_NUM = "offset";
    public final static String PAGE_SIZE = "limit";

    private Integer pageNum;
    private Integer pageSize;
    private List<T> list;
    private Integer totalCount;

    public PageResult() {
        super();
    }

    public PageResult(Map<String, Object> params) {
        if (null != params && !params.isEmpty() && null != params.get("offset")
                && null != params.get("limit")) {
            Integer limit = (Integer) params.get("limit");
            this.pageSize = limit;
            Integer offset = (Integer) params.get("offset");
            this.pageNum = limit == 0 ? 0 : offset / limit;
            params.put("pagesize", limit);
        }
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
