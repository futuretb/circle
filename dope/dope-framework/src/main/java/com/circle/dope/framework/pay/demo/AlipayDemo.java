//package com.circle.dope.framework.pay.demo;
//
//import com.alipay.api.AlipayApiException;
//import com.alipay.api.AlipayClient;
//import com.alipay.api.domain.AlipayTradeAppPayModel;
//import com.alipay.api.request.AlipayTradeAppPayRequest;
//import com.alipay.api.response.AlipayTradeAppPayResponse;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.UnsupportedEncodingException;
//
///**
// * @Classname AlipayDemo
// * @Description TODO
// * @Date 2019/1/22 17:31
// * @Created by win7
// */
//public class AlipayDemo {
//
//    private final static Logger LOGGER = LoggerFactory.getLogger(AlipayDemo.class);
//
//    /**
//     * alipay 预支付接口调用
//     */
//    private void getOrderStr(){
//        String orderStr = null;
//        AlipayClient alipayClient = AlipayClientConf.getAlipayclient();
//        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
//        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
//        model.setBody("order name");
//        model.setSubject("货圈CIRCLE");
//        model.setOutTradeNo("order code");
//        model.setTimeoutExpress("90m");
//        model.setTotalAmount("1");
//        model.setStoreId("storeId");
//        model.setProductCode("QUICK_MSECURITY_PAY");
//        //商品主类型：0—虚拟类商品，1—实物类商品
//        model.setGoodsType("1");
//        String passBackParams = null;
//        try {
//            passBackParams = java.net.URLEncoder.encode("中国",   "utf-8");
//        } catch (UnsupportedEncodingException e2) {
//            e2.printStackTrace();
//        }
//        model.setPassbackParams(passBackParams);
//        request.setBizModel(model);
//        request.setNotifyUrl("notifyUrl");
//        try {
//            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
//            orderStr = response.getBody();
//        } catch (AlipayApiException e) {
//            e.printStackTrace();
//        }
//        LOGGER.info(orderStr);
//    }
//}
