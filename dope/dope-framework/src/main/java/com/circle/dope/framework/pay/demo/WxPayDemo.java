package com.circle.dope.framework.pay.demo;

import com.circle.dope.framework.pay.wxpay.WXPay;
import com.circle.dope.framework.pay.wxpay.WXPayConstants;
import com.circle.dope.framework.pay.wxpay.WXPayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @Classname WxPayDemo
 * @Description TODO
 * @Date 2019/1/22 17:32
 * @Created by win7
 */
public class WxPayDemo {

    private final static Logger LOGGER = LoggerFactory.getLogger(WxPayDemo.class);


    private WXPay wxpay;
    private WXPayClientConf config;

    public WxPayDemo() throws Exception {
        config = WXPayClientConf.getInstance();
        wxpay = new WXPay(config);
    }


    private void getOrderStr(){
        String ip = null;
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("body", "货圈circle-动态购物");
        data.put("out_trade_no", "order code");
        data.put("fee_type", "CNY");
        data.put("total_fee", "payMoney");
        data.put("spbill_create_ip", ip);
        data.put("notify_url", "notifyUrl");
        data.put("trade_type", "APP");
        try {
            Map<String, String> map = wxpay.unifiedOrder(data);
            System.out.println("微信支付统一下单返回数据：" + map);
            // 再次签名 返回的result成功结果取出prepay_id：
            String return_code = (String)map.get("return_code");
            String prepay_id = null;
            if(return_code.contains("SUCCESS")){
                prepay_id=(String)map.get("prepay_id");//获取到prepay_id
            }
            String seconds = String.valueOf(System.currentTimeMillis()/1000L).substring(0, 10);//生成时间戳（转换成秒截取前10位）
            HashMap<String, String> signParam = new HashMap<String, String>();
            signParam.put("appid", config.getAppID("货圈circle-动态购物"));//app_id
            signParam.put("partnerid", config.getMchID());//微信商户账号
            signParam.put("prepayid", prepay_id);//预付订单id
            signParam.put("package", "Sign=WXPay");//默认sign=WXPay
            signParam.put("noncestr", map.get("nonce_str"));//自定义不重复的长度不长于32位
            signParam.put("timestamp", seconds);//北京时间时间戳
            String signAgain = WXPayUtil.generateSignature(signParam, config.getKey(), WXPayConstants.SignType.HMACSHA256);//再次生成签名
            signParam.put("sign", signAgain);
            signParam.put("packageValue", "Sign=WXPay");
            LOGGER.info(signParam + "返回参数");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
