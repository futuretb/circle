package com.circle.dope.framework.util;

import org.springframework.util.StringUtils;

public class UnicodeUtil {

    public static void main(String[] args) {
        String cn = "你AAA";
        System.out.println(cnToUnicode(cn));
        // 字符串 : \u5f00\u59cb\u4efb\u52a1 ，由于 \ 在java里是转义字符，要写出下面这种形式
        String unicode = "\\u4f60\\u0041\\u0041\\u0041";
        System.out.println(unicodeToCn(unicode));
    }

    /**
     * 字符串转换unicode
     */
    public static String cnToUnicode(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        char[] chars = str.toCharArray();
        String returnStr = "";
        for (int i = 0; i < chars.length; i++) {
            String code = Integer.toString(chars[i], 16);
            code = addZeroForNum(code, 4);
            returnStr += "\\u" + code;
        }
        return returnStr;
    }



    public static String unicodeToCn(String unicode) {
        if (StringUtils.isEmpty(unicode)) {
            return null;
        }
        /** 以 \ u 分割，因为java注释也能识别unicode，因此中间加了一个空格*/
        String[] strs = unicode.split("\\\\u");
        String returnStr = "";
        // 由于unicode字符串以 \ u 开头，因此分割出的第一个字符是""。
        for (int i = 1; i < strs.length; i++) {
            returnStr += (char) Integer.valueOf(strs[i], 16).intValue();
        }
        return returnStr;
    }

    public static String addZeroForNum(String str, int strLength) {
        int strLen = str.length();
        StringBuffer sb = null;
        while (strLen < strLength) {
            sb = new StringBuffer();
            // 左补0
            sb.append("0").append(str);
            str = sb.toString();
            strLen = str.length();
        }
        return str;
    }
}
