package com.circle.dope.framework.constant;

public class UserType {

    /**
     * 卖家Token
     */
    public static final String SELLER_TOKEN = "seller_token";
    /**
     * 买家Token
     */
    public static final String BUYER_TOKEN = "buyer_token";
}
