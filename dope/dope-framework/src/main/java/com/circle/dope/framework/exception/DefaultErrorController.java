package com.circle.dope.framework.exception;

import com.circle.dope.framework.base.BaseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DefaultErrorController implements ErrorController {

    private final String ERROR_MSG = "The path is not filled in correctly";
    private final String ERROR_PATH = "/error";

    private final Logger LOGGER = LoggerFactory.getLogger(DefaultErrorController.class);

    @RequestMapping(ERROR_PATH)
    @ResponseBody
    public BaseResult handleError(HttpServletRequest request){
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String requestUrl = (String) request.getAttribute("javax.servlet.error.request_uri");
        return BaseResult.builder().code(statusCode).msg(ERROR_MSG).result(requestUrl);
    }

    public String getErrorPath() {
        return ERROR_PATH;
    }
}
