package com.circle.dope.framework.util;

import org.apache.commons.lang.math.RandomUtils;

public class RandomUtil {

    public static String randomCode(int count) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count; i++) {
            sb.append(RandomUtils.nextInt(6));
        }
        return sb.toString();
    }

}
