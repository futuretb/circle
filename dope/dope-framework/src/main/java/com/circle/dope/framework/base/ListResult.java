package com.circle.dope.framework.base;

import java.io.Serializable;
import java.util.List;

public class ListResult<T> implements Serializable {

    private List<T> list;

    public ListResult(List<T> list) {
        this.list = list;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
