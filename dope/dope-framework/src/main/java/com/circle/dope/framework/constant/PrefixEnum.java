package com.circle.dope.framework.constant;

public class PrefixEnum {

    public enum MESSAGE_TYPE {

        LOGIN("LOGIN_", "登录"),
        BIND_PHONE("BIND_PHONE_", "绑定手机"),
        UPDATE_PASSWORD("UPDATE_PASSWORD_", "修改密码"),
        INVITE_CODE("INVITE_CODE_", "邀请码"),
        REGISTER("REGISTER_", "商家版注册");

        private String prefix;
        private String cnName;

        public static String findPrefixByType(String type) {
            for (MESSAGE_TYPE messageType: MESSAGE_TYPE.values()) {
                if (messageType.name().equals(type)) {
                    return messageType.getPrefix();
                }
            }
            return null;
        }

        MESSAGE_TYPE(String prefix, String cnName) {
            this.prefix = prefix;
            this.cnName = cnName;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getCnName() {
            return cnName;
        }

        public void setCnName(String cnName) {
            this.cnName = cnName;
        }
    }

    public enum GOODS_TYPE {

        CATEGORY_ID("CATEGORY_INFO_", "类目编码"),
        BRAND_ID("BRAND_INFO_", "品牌编码"),
        STORE_INFO_ID("STORE_INFO_", "店铺编码"),
        INVENTORY("INVENTORY_", "商品库存"),
        SALE_IN("SALE_IN_", "商品在售库存"),
        USER_ID("USER_ID_", "用户信息"),
        WX_USER_ID("WX_USER_", "微信用户信息");

        private String prefix;
        private String cnName;

        public static String findPrefixByType(String name) {
            for (GOODS_TYPE goodsType: GOODS_TYPE.values()) {
                if (goodsType.name().equals(name)) {
                    return goodsType.getPrefix();
                }
            }
            return null;
        }

        GOODS_TYPE(String prefix, String cnName) {
            this.prefix = prefix;
            this.cnName = cnName;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getCnName() {
            return cnName;
        }

        public void setCnName(String cnName) {
            this.cnName = cnName;
        }
    }

    public enum LOCK_TYPE {

        ORDER_LOCK("ORDER_LOCK_", "订单锁");

        private String prefix;
        private String cnName;

        public static String findPrefixByType(String name) {
            for (LOCK_TYPE lockType: LOCK_TYPE.values()) {
                if (lockType.name().equals(name)) {
                    return lockType.getPrefix();
                }
            }
            return null;
        }

        LOCK_TYPE(String prefix, String cnName) {
            this.prefix = prefix;
            this.cnName = cnName;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getCnName() {
            return cnName;
        }

        public void setCnName(String cnName) {
            this.cnName = cnName;
        }
    }


    public enum USER_TYPE {

        WX_USER("WX_USER_", "微信用户"),
        USER("USER_", "商家版用户");

        private String prefix;
        private String cnName;

        public static String findPrefixByType(String name) {
            for (USER_TYPE userType: USER_TYPE.values()) {
                if (userType.name().equals(name)) {
                    return userType.getPrefix();
                }
            }
            return null;
        }

        USER_TYPE(String prefix, String cnName) {
            this.prefix = prefix;
            this.cnName = cnName;
        }

        public String getPrefix() {
            return prefix;
        }

        public void setPrefix(String prefix) {
            this.prefix = prefix;
        }

        public String getCnName() {
            return cnName;
        }

        public void setCnName(String cnName) {
            this.cnName = cnName;
        }
    }
}
