package com.circle.dope.framework.util;

import com.circle.dope.framework.base.BaseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import static com.circle.dope.framework.base.BaseResult.Message.DEFAULT_VALI_MSG;
import static com.circle.dope.framework.base.BaseResult.Message.INNER_REQUEST_ERROR;

public class AssertUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssertUtil.class);

    public static void notNull(Object obj) {
        if (null == obj) {
            Assert.notNull(obj, DEFAULT_VALI_MSG);
        }
        if (obj instanceof String) {
            Assert.isTrue(!StringUtils.isEmpty(obj), DEFAULT_VALI_MSG);
        }
        if (obj instanceof BaseResult) {
            BaseResult baseResult = (BaseResult) obj;
            Assert.isTrue(baseResult.getCode() == 200, INNER_REQUEST_ERROR);
        }
    }
}
