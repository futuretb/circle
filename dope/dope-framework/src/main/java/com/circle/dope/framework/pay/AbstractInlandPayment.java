package com.circle.dope.framework.pay;

public abstract class AbstractInlandPayment implements Payment{

    public void payment() {
        System.out.println("国内支付模板");
//        1、准备参数
        prePay();
//        2、pay
        pay(new PayRequest());
//        3、记录
        postPay();
    }

    protected  abstract void prePay();

    protected abstract void postPay();

}
