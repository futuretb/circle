package com.circle.dope.framework.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class RequestUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestUtil.class);

    public static Map<String, Object> doPost(String url, Map<String, String> params) throws IOException {
        CloseableHttpClient hc = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        for (Entry<String, String> entry : params.entrySet()) {
            if (null != entry.getKey() && null != entry.getValue()) {
                nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
        return execute(hc, httpPost);
    }

    public static Map<String, Object> doGet(String url) throws IOException {
        CloseableHttpClient hc = HttpClients.createDefault();
        CloseableHttpResponse hr = null;
        HttpGet httpGet = new HttpGet(url);
        return execute(hc, httpGet);
    }

    private static Map<String, Object> execute(CloseableHttpClient hc, HttpUriRequest request) throws IOException {
        CloseableHttpResponse hr = null;
        try {
            hr = hc.execute(request);
            int statusCode = hr.getStatusLine().getStatusCode();
            String log = "Url: %s, Content-Type: %s, StatusCode: %s";
            LOGGER.info(String.format(log, request.getURI(), request.getHeaders("Content-Type"), statusCode));
            if (statusCode == HttpStatus.SC_OK) {
                HttpEntity resEntity = hr.getEntity();
                String entity = EntityUtils.toString(resEntity, "utf-8");
                return JSONObject.parseObject(entity, HashMap.class);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HttpClientUtils.closeQuietly(hr);
            HttpClientUtils.closeQuietly(hc);
        }
        return null;
    }
}
