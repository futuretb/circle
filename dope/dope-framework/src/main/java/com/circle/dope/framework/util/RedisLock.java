package com.circle.dope.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class RedisLock implements RedisLockService {

    private final static long ACCQUIRE_LOCK_TIMEOUT_IN_MS = 10 * 1000;

    private final static int EXPIRE_IN_SECOND = 4;// 锁失效时间

    private final static long WAIT_INTERVAL_IN_MS = 100;

    private JedisPool jedisPool;

    private final long acquireLocktimeoutInMS;
    private final int expireInSecond;
    private final long waitIntervalInMS;
    private final ConcurrentMap<String, String> settedKeys;

    static Logger LOG = LoggerFactory.getLogger(RedisLock.class);

    public RedisLock(
            final long acquireLocktimeout, final int expireInSecond,
            final long waitIntervalInMS, JedisPool jedisPool) {
        this.acquireLocktimeoutInMS = acquireLocktimeout;
        this.expireInSecond = expireInSecond;
        this.waitIntervalInMS = waitIntervalInMS;
        this.settedKeys = new ConcurrentHashMap<String, String>();
        this.jedisPool = jedisPool;
    }

    public RedisLock(JedisPool jedisPool) {
        this(ACCQUIRE_LOCK_TIMEOUT_IN_MS, EXPIRE_IN_SECOND,
                WAIT_INTERVAL_IN_MS, jedisPool);
        LOG.info("RedisLockServiceImpl init");
    }


    @Override
    public void lock(String redisKey) throws Exception {
        validateRedisKeyName(redisKey);
        Jedis resource = null;
        try {
            resource = jedisPool.getResource();
            long timeoutAt = currentTimeMillisFromRedis()
                    + acquireLocktimeoutInMS;
            boolean flag = false;
            while (true) {
                String expireAt = String.valueOf(currentTimeMillisFromRedis()
                        + expireInSecond * 1000);
                long ret = resource.setnx(redisKey, expireAt);
                if (ret == 1) {
                    settedKeys.put(redisKey, expireAt);
                    flag = true;
                    break;
                } else {
                    String oldExpireAt = resource.get(redisKey);
                    if (oldExpireAt != null
                            && Long.parseLong(oldExpireAt) < currentTimeMillisFromRedis()) {
                        oldExpireAt = resource.getSet(redisKey, expireAt);

                        if (Long.parseLong(oldExpireAt) < currentTimeMillisFromRedis()) {
                            settedKeys.put(redisKey, expireAt);

                            flag = true;
                            break;
                        } else {
                            // loop ...
                        }
                    } else {
                        // loop ...
                    }
                }

                if (acquireLocktimeoutInMS <= 0
                        || timeoutAt < currentTimeMillisFromRedis()) {
                    break;
                }

                try {
                    TimeUnit.MILLISECONDS.sleep(waitIntervalInMS);
                } catch (Exception ignore) {
                }
            }
            if (!flag) {
                throw new RuntimeException("canot acquire lock now ...");
            }
        } catch (JedisException je) {
            je.printStackTrace();
            if (resource != null) {
                resource.close();
            }
            throw je;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (resource != null) {
                resource.close();
            }
        }
    }

    @Override
    public boolean unlock(String name) throws Exception {
        validateRedisKeyName(name);
        Jedis resource = null;
        try {
            resource = jedisPool.getResource();
            resource.del(name);
            settedKeys.remove(name);
            return true;
        } catch (JedisException je) {
            je.printStackTrace();
            if (resource != null) {
                resource.close();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (resource != null) {
                resource.close();
            }
        }
    }

    @Override
    public boolean unlockAll() throws Exception {
        Jedis resource = null;
        try {
            resource = jedisPool.getResource();
            Iterator<String> iter = settedKeys.keySet().iterator();
            while (iter.hasNext()) {
                String key = iter.next();
                resource.del(key);
                settedKeys.remove(key);
            }
            return true;
        } catch (JedisException je) {
            je.printStackTrace();
            if (resource != null) {
                resource.close();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (resource != null) {
                resource.close();
            }
        }
    }



    private void validateRedisKeyName(String name) {
        if (name == null || "".equals(name.trim())) {
            throw new IllegalArgumentException("validateKey fail.");
        }
    }

    private Long currentTimeMillisFromRedis() throws Exception {
        Jedis resource = null;
        try {
            resource = jedisPool.getResource();
            return Long.parseLong(resource.time().get(0)) * 1000;
        } catch (JedisException je) {
            je.printStackTrace();
            if (resource != null) {
                resource.close();
            }
            throw je;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (resource != null) {
                resource.close();
            }
        }
    }
    private static final long expired = 2000;//1秒超时


    @Override
    public boolean acquireLock(String lock) throws Exception {
        Jedis jedis = jedisPool.getResource();
        // 1. 通过SETNX试图获取一个lock
        boolean success = false;
        long value = System.currentTimeMillis() + expired + 1;
        long acquired = jedis.setnx(lock, String.valueOf(value));
        jedis.expire(lock, 2);//设置1秒超时
        //SETNX成功，则成功获取一个锁
        if (acquired == 1)  success = true;
            //SETNX失败，说明锁被其他客户端保持，检查其是否已经超时
        else {
            long oldValue = Long.valueOf(jedis.get(lock));
            if (oldValue < System.currentTimeMillis()) {//超时
                //获取上一个锁到期时间，并设置现在的锁到期时间，
                //只有一个线程才能获取上一个线上的设置时间，因为jedis.getSet是同步的
                String getValue = jedis.getSet(lock, String.valueOf(value));
                if (getValue !=null) {
                    if (Long.valueOf(getValue) == oldValue)
                        success = true;
                    else success = false;// 已被其他进程捷足先登了
                }
            }else //未超时，则直接返回失败
                success = false;
        }
        return success;
    }

    @Override
    public void releaseLock(String lock) throws Exception {
        Jedis jedis = jedisPool.getResource();
        jedis.del(lock);
    }

    @Override
    public long locks(String redisKey) throws Exception {
        validateRedisKeyName(redisKey);
        long ret = 0;
        Jedis resource = null;
        try {
            resource = jedisPool.getResource();
            long timeoutAt = currentTimeMillisFromRedis()
                    + acquireLocktimeoutInMS;
            boolean flag = false;
            String expireAt = String.valueOf(currentTimeMillisFromRedis()
                    + expireInSecond * 1000);
            ret = resource.setnx(redisKey, expireAt);
            return ret;
        } catch (JedisException je) {
            je.printStackTrace();
            if (resource != null) {
                resource.close();
            }
            throw je;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (resource != null) {
                resource.close();
            }
        }
    }

    //====================自由锁 BY:COLIN====================
    /**
     * 获取锁成功
     */
    private static final String LOCK_OK = "OK";
    /**
     * 解锁成功
     */
    private static final Long UNLOCK_OK = 1L;
    /**
     * SET IF NOT EXIST
     * 如果不存在则插入
     */
    private static final String NX = "NX";
    private static final String PX = "PX";
    private static final long TIME = 3000L;

    /**
     * 自由锁
     * 自定义锁失效时间(*毫秒)
     */
    @Override
    public boolean lockFree(String key, long millisecond){
        Jedis jedis = null;
        try {
            if (null != key || !"".equals(key) || 0 < millisecond) {
                jedis = jedisPool.getResource();
                //为保持设置的原子性，使用set执行命令行，可保证无论什么情况下都不会出现死锁的情况
                if (LOCK_OK.equals(jedis.set(key, key, NX, PX, millisecond))) {
                    return true;
                }
            }
        } catch (Exception e) {
            LOG.info("the lockFree exception, key--[" + key + "]", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        return false;
    }

    /**
     * 自由锁
     * 默认锁失效时间(3000毫秒)
     */
    @Override
    public boolean lockFree(String key) {
        return lockFree(key, TIME);
    }

    /**
     * 解锁自由锁
     */
    @Override
    public boolean unlockFree(String key) {
        Jedis jedis = null;
        try {
            if (null != key || !"".equals(key)) {
                jedis = jedisPool.getResource();
                //使用LUA脚本执行，能做到安全删除锁
                String scrpit = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                List<String> keys = new ArrayList<String>();
                keys.add(key);
                if (UNLOCK_OK.equals(jedis.eval(scrpit, keys, keys))) {
                    return true;
                }
            }
        } catch (Exception e) {
            LOG.info("the unlockFree exception, key--[" + key + "]", e);
        } finally {
            if (null != jedis) {
                jedis.close();
            }
        }
        return false;
    }



}