package com.circle.api.gateway.filter;

import com.alibaba.fastjson.JSONObject;
import com.circle.api.gateway.config.FilterLoginConfig;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.ClientUtil;
import com.circle.dope.framework.util.HttpServletRequestUtil;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD_UN_AUTH;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_DECORATION_FILTER_ORDER;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.PRE_TYPE;

@Component
public class LoginFilter extends ZuulFilter {

    @Resource
    private FilterLoginConfig filterLoginConfig;

    @Override
    public String filterType() {
        return PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return PRE_DECORATION_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        printSource(request);
        if (filterLoginConfig.getIgnoreUrl().contains(request.getRequestURI())) {
            return null;
        }
        // 当前从url参数里获取，也可以从cookie，header里获取
        String buyerToken = request.getParameter(UserType.BUYER_TOKEN);
        String sellerToken = request.getParameter(UserType.SELLER_TOKEN);
        if (StringUtils.isBlank(buyerToken) && StringUtils.isBlank(sellerToken)) {
            requestContext.setSendZuulResponse(false);
            requestContext.setResponseStatusCode(HttpStatus.SC_UNAUTHORIZED);
            requestContext.getResponse().setContentType("application/json;charset=UTF-8");
            requestContext.setResponseBody(JSONObject.toJSONString(FAILD_UN_AUTH));
        }
        return null;
    }

    private void printSource(HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        result.put("ip", ClientUtil.getClientIp(request));
        result.put("url", request.getRequestURI());
        result.put("token", request.getParameterNames());
        result.put("body", HttpServletRequestUtil.ReadAsChars(request));
        System.out.println(JSONObject.toJSONString(result));
    }
}
