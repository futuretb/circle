package com.circle.api.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@ConfigurationProperties(prefix="filter.login")
public class FilterLoginConfig {

    private Set<String> ignoreUrl = new HashSet<>();

    public Set<String> getIgnoreUrl() {
        return ignoreUrl;
    }

    public void setIgnoreUrl(Set<String> ignoreUrl) {
        this.ignoreUrl = ignoreUrl;
    }
}
