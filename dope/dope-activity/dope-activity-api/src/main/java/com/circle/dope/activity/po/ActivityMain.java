package com.circle.dope.activity.po;

import com.circle.dope.framework.base.BasePo;

import java.math.BigDecimal;
import java.util.Date;


/**
 *  活动配置信息 - Po
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
public class ActivityMain extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 用户卖家版（创建人）
     */
    private String userId;

    /**
     * 商品唯一标识
     */
    private Long goodsId;

    /**
     * 最大优惠数量
     */
    private Integer maxDiscountsCount;

    /**
     * 最大优惠金额
     */
    private BigDecimal maxDiscountsMoney;

    /**
     * 规则描述
     */
    private String ruleDesc;

    /**
     * 类型 BARGAIN_ACTIVITY 砍价活动
     */
    private String type;

    /**
     * 状态 开始 STARTED 进行中 RUNNING 停止 STOPPED 过期 EXPIRED
     */
    private String status;

    /**
     * 下单限制
     */
    private BigDecimal orderRestriction;

    /**
     * 有效天数
     */
    private Integer validDay;

    /**
     * 初始价格
     */
    private BigDecimal initialPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getMaxDiscountsCount() {
        return maxDiscountsCount;
    }

    public void setMaxDiscountsCount(Integer maxDiscountsCount) {
        this.maxDiscountsCount = maxDiscountsCount;
    }

    public BigDecimal getMaxDiscountsMoney() {
        return maxDiscountsMoney;
    }

    public void setMaxDiscountsMoney(BigDecimal maxDiscountsMoney) {
        this.maxDiscountsMoney = maxDiscountsMoney;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getOrderRestriction() {
        return orderRestriction;
    }

    public void setOrderRestriction(BigDecimal orderRestriction) {
        this.orderRestriction = orderRestriction;
    }

    public Integer getValidDay() {
        return validDay;
    }

    public void setValidDay(Integer validDay) {
        this.validDay = validDay;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }
}