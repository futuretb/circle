package com.circle.dope.activity.enums;

public enum  ACTIVITY_PARTICIPATION_STATUS {

    NORMAL("正常"),
    EXPIRE("过期");

    private String cnName;

    ACTIVITY_PARTICIPATION_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }
}
