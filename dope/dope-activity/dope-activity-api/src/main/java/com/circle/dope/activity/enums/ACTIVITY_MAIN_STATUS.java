package com.circle.dope.activity.enums;

public enum ACTIVITY_MAIN_STATUS {

    STARTED("开始"),
    RUNNING("进行中"),
    STOPPED("停止"),
    EXPIRED("过期");

    private String cnName;

    ACTIVITY_MAIN_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }
}
