package com.circle.dope.activity.po;

import com.circle.dope.framework.base.BasePo;
import java.util.Date;


/**
 *  参加活动记录 - Po
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
public class ActivityParticipation extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 活动id
     */
    private Long activityMainId;

    /**
     * 买家code
     */
    private String userCode;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 状态
     */
    private String status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityMainId() {
        return activityMainId;
    }

    public void setActivityMainId(Long activityMainId) {
        this.activityMainId = activityMainId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}