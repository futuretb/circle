package com.circle.dope.activity.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 参加活动记录 - Api
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
public interface ActivityParticipationApi<R, P> extends BaseApi<R, P> {

    @RequestMapping(value = "/startBargain", method = RequestMethod.POST)
    public R startBargain(P p);

}
