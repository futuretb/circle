package com.circle.dope.activity.api;

/**
 * 活动配置信息 - Api
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
public interface ActivityMainApi<R, P> extends BaseApi<R, P> {

}
