package com.circle.dope.activity.po;

import com.circle.dope.framework.base.BasePo;

import java.math.BigDecimal;
import java.util.Date;


/**
 *  邀请互助记录 - Po
 *
 *  Created by dicksoy on '2018-12-26 15:22:20'.
 *  Email dicksoy@163.com
 */
public class ActivityRecord extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 微信夸平台唯一标识符
     */
    private String unionId;

    /**
     * 买家user_code
     */
    private String userCode;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 砍价金额
     */
    private BigDecimal bargainMoney;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 活动id
     */
    private Long activityMainId;

    /**
     * 参与id
     */
    private Long activityParticipationId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public BigDecimal getBargainMoney() {
        return bargainMoney;
    }

    public void setBargainMoney(BigDecimal bargainMoney) {
        this.bargainMoney = bargainMoney;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getActivityMainId() {
        return activityMainId;
    }

    public void setActivityMainId(Long activityMainId) {
        this.activityMainId = activityMainId;
    }

    public Long getActivityParticipationId() {
        return activityParticipationId;
    }

    public void setActivityParticipationId(Long activityParticipationId) {
        this.activityParticipationId = activityParticipationId;
    }

}