package com.circle.dope.activity.impl;

import com.circle.dope.activity.dao.ActivityMainDao;
import com.circle.dope.activity.service.ActivityMainService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.activity.po.ActivityMain;

/**
 * 活动配置信息 - ServiceImpl
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
@Service
public class ActivityMainServiceImpl extends BaseServiceImpl<ActivityMainDao, ActivityMain>
	implements ActivityMainService {

}
