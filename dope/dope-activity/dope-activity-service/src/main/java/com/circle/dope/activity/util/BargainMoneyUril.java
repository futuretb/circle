package com.circle.dope.activity.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BargainMoneyUril {

    private static double getRandomMoney(BargainMoney bargainMoney) {
        // remainSize 剩余的红包数量
        // remainMoney 剩余的钱
        if (bargainMoney.remainSize == 1) {
            bargainMoney.remainSize--;
            return (double) Math.round(bargainMoney.remainMoney * 100) / 100;
        }
        Random r     = new Random();
        double min   = 0.01; //
        double max   = bargainMoney.remainMoney / bargainMoney.remainSize * 2;
        double money = r.nextDouble() * max;
        money = money <= min ? 0.01: money;
        money = Math.floor(money * 100) / 100;
        bargainMoney.remainSize--;
        bargainMoney.remainMoney -= money;
        return money;
    }

    public static List<Double> getResult(int remainSize, double remainMoney) {
        BargainMoney bargainMoney = new BargainMoney(remainSize, remainMoney);
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < remainSize; i++) {
            result.add(i, BargainMoneyUril.getRandomMoney(bargainMoney));
        }
        return result;
    }

    public static class BargainMoney {

        private int remainSize;
        private double remainMoney;

        public BargainMoney(int remainSize, double remainMoney) {
            this.remainSize = remainSize;
            this.remainMoney = remainMoney;
        }
    }
}
