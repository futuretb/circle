package com.circle.dope.activity.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.activity.po.ActivityParticipation;

/**
 * 参加活动记录 - Dao
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ActivityParticipationDao extends BaseDao<ActivityParticipation> {

}
