package com.circle.dope.activity.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.activity.po.ActivityMain;

/**
 * 活动配置信息 - Service
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
public interface ActivityMainService extends BaseService<ActivityMain> {

}
