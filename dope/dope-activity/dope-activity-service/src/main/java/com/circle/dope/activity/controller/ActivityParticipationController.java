package com.circle.dope.activity.controller;

import com.circle.dope.activity.enums.ACTIVITY_PARTICIPATION_STATUS;
import com.circle.dope.activity.po.ActivityMain;
import com.circle.dope.activity.po.ActivityRecord;
import com.circle.dope.activity.service.ActivityMainService;
import com.circle.dope.activity.service.ActivityRecordService;
import com.circle.dope.activity.util.BargainMoneyUril;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.activity.api.ActivityParticipationApi;
import com.circle.dope.activity.po.ActivityParticipation;
import com.circle.dope.activity.service.ActivityParticipationService;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.base.PageResult;
import com.circle.dope.framework.util.AssertUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;

import static com.circle.dope.framework.constant.RedisPrefix.BARGAINMONEY_REDIS_PREFIX;

/**
 * 参加活动记录 - Controller
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/activity/activityParticipation", method = RequestMethod.POST)
public class ActivityParticipationController extends BaseController<ActivityParticipationService, ActivityParticipation>
        implements ActivityParticipationApi<BaseResult, ActivityParticipation> {

    private ExecutorService executorService = Executors.newCachedThreadPool();

    @Resource
    private ActivityMainService activityMainService;

    @Resource
    private ActivityRecordService activityRecordService;

    @Resource
    private RedisTemplate<String, Double> redisTemplate;

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    @ResponseBody
    public Object test() {
        ActivityParticipation activityParticipation = new ActivityParticipation();
        activityParticipation.setActivityMainId(1L);
        activityParticipation.setStatus("RUNNING");
        activityParticipation.setUserCode("123123");
        activityParticipation.setData(new Date());
        Future<Long> future = executorService.submit(() -> service.insert(activityParticipation));
        try {
            System.out.println(future.get());
            return future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/findRecordList", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findRecord(@RequestBody Map<String, Object> params) {
        PageResult<ActivityParticipation> pageResult = service.findListByPage(params);
        List<ActivityParticipation> list = pageResult.getList();
        if (null != list && !list.isEmpty()) {
            for (ActivityParticipation participation: list) {
                if (null != participation) {
                    BigDecimal total = activityRecordService.findTotalBargainMoneyByParticipationId(participation.getId());
                    Map<String, Object> data = new HashMap<>();
                    data.put("totalBargainMoney", total);
                    participation.setData(data);
                }
            }
        }
        return BaseResult.SUCCESS.result(pageResult);
    }

    @RequestMapping(value = "/findRecordById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findRecordById(@RequestBody Map<String, Object> params) {
        ActivityParticipation activityParticipation = service.findById(Long.valueOf(String.valueOf(params.get("id"))));
        AssertUtil.notNull(activityParticipation);
        ActivityMain activityMain = activityMainService.findById(activityParticipation.getActivityMainId());
        AssertUtil.notNull(activityMain);
        Map<String, Object> result = new HashMap<>();
        result.put("activityParticipation", activityParticipation);
        result.put("activityMain", activityMain);
        Map<String, Object> activityRecordMap = new HashMap<>();
        activityRecordMap.put("userCode", params.get("userCode"));
        activityRecordMap.put("activityParticipationId", activityParticipation.getId());
        activityRecordMap.put("activityMainId", activityParticipation.getActivityMainId());
        ListResult<ActivityRecord> recordList = activityRecordService.findList(activityRecordMap);
        result.put("activityRecordList", recordList);
        return BaseResult.SUCCESS.result(result);
    }


    @Override
    @ResponseBody
    public BaseResult startBargain(@RequestBody ActivityParticipation activityParticipation) {
        AssertUtil.notNull(activityParticipation);
        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("activityMainId", activityParticipation.getActivityMainId());
        totalMap.put("status", ACTIVITY_PARTICIPATION_STATUS.NORMAL.name());
        Integer count = service.findTotal(totalMap);
        if (count > 0) {
            return BaseResult.builder().code(505).msg("当前活动已经参与过了");
        }
        ActivityMain activityMain = activityMainService.findById(activityParticipation.getActivityMainId());
        activityParticipation.setStatus(ACTIVITY_PARTICIPATION_STATUS.NORMAL.name());
        Future<Long> future = executorService.submit(() -> service.insert(activityParticipation));
        List<Double> list = BargainMoneyUril.getResult(activityMain.getMaxDiscountsCount(), activityMain.getMaxDiscountsMoney().doubleValue());
        try {
            if (!future.isDone()) {
                Thread.sleep(1000);
            } else {
                redisTemplate.opsForList().leftPushAll(BARGAINMONEY_REDIS_PREFIX + future.get(), list);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public static void main(String[] args) {
//        System.out.println(Arrays.toString(BargainMoneyUril.getResult(30, 100)));
//
//        String[] arr = "0.84, 2.51, 3.1, 3.49, 6.24, 1.94, 6.79, 4.55, 4.73, 3.42, 4.48, 0.57, 2.82, 2.52, 3.01, 1.49, 2.81, 5.82, 2.51, 3.95, 0.01, 5.91, 5.96, 1.73, 2.26, 6.49, 3.57, 2.1, 2.0, 2.38".split(", ");
//        BigDecimal total = new BigDecimal("0");
//        for (int i = 0; i < arr.length; i++) {
//            total = total.add(new BigDecimal(arr[i]));
//        }
//        System.out.println(arr.length);
//        System.out.println(total);

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                BargainMoneyUril.BargainMoney bargainMoney = BargainMoneyUril.init(10, 20);
//                for (int i = 0; i < 10; i++) {
//                    System.out.println(Thread.currentThread().getName() + " - " + BargainMoneyUril.getRandomMoney(bargainMoney));;
//                }
//            }
//        }, "th1").start();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                BargainMoneyUril.BargainMoney bargainMoney = BargainMoneyUril.init(10, 20);
//                for (int i = 0; i < 12; i++) {
//                    System.out.println(Thread.currentThread().getName() + " - " + BargainMoneyUril.getRandomMoney(bargainMoney));;
//                }
//            }
//        }, "th2").start();
    }
}
