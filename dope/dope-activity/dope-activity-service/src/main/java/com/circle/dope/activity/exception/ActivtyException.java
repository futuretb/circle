package com.circle.dope.activity.exception;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.exception.DopeException;
import com.circle.dope.framework.exception.GlobalDopeExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.circle.dope.framework.base.BaseResult.Code.ERROR;
import static com.circle.dope.framework.base.BaseResult.FAILD;

@ControllerAdvice
public class ActivtyException extends GlobalDopeExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public BaseResult processException(RuntimeException exception) {
        LOGGER.info("RuntimeException : {}", exception);
        return FAILD.result(exception.getMessage());
    }
}
