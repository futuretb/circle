package com.circle.dope.activity.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.activity.po.ActivityParticipation;

/**
 * 参加活动记录 - Service
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
public interface ActivityParticipationService extends BaseService<ActivityParticipation> {

}
