package com.circle.dope.activity.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.activity.po.ActivityMain;

/**
 * 活动配置信息 - Dao
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ActivityMainDao extends BaseDao<ActivityMain> {

}
