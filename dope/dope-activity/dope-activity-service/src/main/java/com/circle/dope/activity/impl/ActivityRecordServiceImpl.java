package com.circle.dope.activity.impl;

import com.circle.dope.activity.dao.ActivityRecordDao;
import com.circle.dope.activity.service.ActivityRecordService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.activity.po.ActivityRecord;

import java.math.BigDecimal;

/**
 * 邀请互助记录 - ServiceImpl
 *
 *  Created by dicksoy on '2018-12-26 15:22:20'.
 *  Email dicksoy@163.com
 */
@Service
public class ActivityRecordServiceImpl extends BaseServiceImpl<ActivityRecordDao, ActivityRecord>
	implements ActivityRecordService {

	@Override
	public BigDecimal findTotalBargainMoneyByParticipationId(Long ParticipationId) {
		String totalMoney = mapper.findTotalBargainMoneyByParticipationId(ParticipationId);
		return new BigDecimal(totalMoney);
	}
}
