package com.circle.dope.activity.controller;

import com.circle.dope.activity.enums.ACTIVITY_PARTICIPATION_STATUS;
import com.circle.dope.activity.service.ActivityParticipationService;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.activity.api.ActivityMainApi;
import com.circle.dope.activity.po.ActivityMain;
import com.circle.dope.activity.service.ActivityMainService;
import com.circle.dope.framework.util.AssertUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 活动配置信息 - Controller
 *
 *  Created by dicksoy on '2018-12-26 15:22:32'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/activity/activityMain")
public class ActivityMainController {

    @Resource
    private  ActivityMainService activityMainService;

    @Resource
    private ActivityParticipationService activityParticipationService;

    @RequestMapping(value = "/insertByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insertByCondition(@RequestBody ActivityMain params) {
        AssertUtil.notNull(params);
        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("goodsId", params.getGoodsId());
        if (activityMainService.findTotal(totalMap) > 0) {
            return FAILD.msg("当前商品已被创建了活动");
        }
        return activityMainService.insertByCondition(params) > 0 ? SUCCESS : FAILD;
    }

    @RequestMapping(value = "/updateByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult updateByCondition(@RequestBody ActivityMain params) {
        AssertUtil.notNull(params);
        Map<String, Object> totalMap = new HashMap<>();
        totalMap.put("activityMainId", params.getId());
        totalMap.put("status", ACTIVITY_PARTICIPATION_STATUS.NORMAL.name());
        if (activityParticipationService.findTotal(totalMap) > 0) {
            return FAILD.msg("当前活动还有未结束的砍单");
        }
        return activityMainService.update(params) ? SUCCESS : FAILD;
    }
}
