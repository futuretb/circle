package com.circle.dope.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
public class DopeActivityServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DopeActivityServiceApplication.class, args);
    }

}

