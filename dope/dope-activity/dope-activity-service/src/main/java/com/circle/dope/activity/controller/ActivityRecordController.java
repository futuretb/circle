package com.circle.dope.activity.controller;

import com.circle.dope.activity.enums.ACTIVITY_MAIN_STATUS;
import com.circle.dope.activity.enums.ACTIVITY_PARTICIPATION_STATUS;
import com.circle.dope.activity.po.ActivityMain;
import com.circle.dope.activity.po.ActivityParticipation;
import com.circle.dope.activity.service.ActivityMainService;
import com.circle.dope.activity.service.ActivityParticipationService;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.activity.api.ActivityRecordApi;
import com.circle.dope.activity.po.ActivityRecord;
import com.circle.dope.activity.service.ActivityRecordService;
import com.circle.dope.framework.util.AssertUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import java.math.BigDecimal;

import static com.circle.dope.framework.constant.RedisPrefix.BARGAINMONEY_REDIS_PREFIX;

/**
 * 邀请互助记录 - Controller
 *
 *  Created by dicksoy on '2018-12-26 15:22:20'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/activity/activityRecord")
public class ActivityRecordController extends BaseController<ActivityRecordService, ActivityRecord>
        implements ActivityRecordApi<BaseResult, ActivityRecord> {

    @Resource
    private ActivityMainService activityMainService;

    @Resource
    private ActivityParticipationService activityParticipationService;

    @Resource
    private RedisTemplate<String, Double> redisTemplate;

    @RequestMapping(value = "/helpBargain", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult helpBargain(@RequestBody ActivityRecord activityRecord) {
        AssertUtil.notNull(activityRecord);
        ActivityMain activityMain = activityMainService.findById(activityRecord.getActivityMainId());
        AssertUtil.notNull(activityMain);
        if (!activityMain.getStatus().equals(ACTIVITY_MAIN_STATUS.RUNNING.name())) {
            return FAILD.msg("当前活动已经结束");
        }
        ActivityParticipation activityParticipation = activityParticipationService.findById(activityRecord.getActivityParticipationId());
        if (null == activityParticipation || activityParticipation.getStatus().equals(ACTIVITY_PARTICIPATION_STATUS.EXPIRE.name())) {
            return FAILD.msg("本次砍价以失效");
        }
        Double bargainmoney = redisTemplate.opsForList().leftPop(BARGAINMONEY_REDIS_PREFIX + activityParticipation.getId());
        activityRecord.setBargainMoney(new BigDecimal(String.valueOf(bargainmoney)));
        service.insert(activityRecord);
        return SUCCESS.result(bargainmoney);
    }



}
