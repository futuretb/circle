package com.circle.dope.activity.impl;

import com.circle.dope.activity.dao.ActivityParticipationDao;
import com.circle.dope.activity.service.ActivityParticipationService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.activity.po.ActivityParticipation;

/**
 * 参加活动记录 - ServiceImpl
 *
 *  Created by dicksoy on '2018-12-26 15:22:05'.
 *  Email dicksoy@163.com
 */
@Service
public class ActivityParticipationServiceImpl extends BaseServiceImpl<ActivityParticipationDao, ActivityParticipation>
	implements ActivityParticipationService {

}
