package com.circle.dope.activity.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.activity.po.ActivityRecord;
import org.apache.ibatis.annotations.Param;

/**
 * 邀请互助记录 - Dao
 *
 *  Created by dicksoy on '2018-12-26 15:22:20'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ActivityRecordDao extends BaseDao<ActivityRecord> {

    String findTotalBargainMoneyByParticipationId(@Param("participationId") Long participationId);
}
