package com.circle.dope.activity.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.activity.po.ActivityRecord;

import java.math.BigDecimal;

/**
 * 邀请互助记录 - Service
 *
 *  Created by dicksoy on '2018-12-26 15:22:20'.
 *  Email dicksoy@163.com
 */
public interface ActivityRecordService extends BaseService<ActivityRecord> {

    BigDecimal findTotalBargainMoneyByParticipationId(Long id);
}