package com.circle.dope.message.exception;

import com.circle.dope.framework.exception.GlobalDopeExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class MessageException extends GlobalDopeExceptionHandler {

}
