package com.circle.dope.message.enums;

/**
 * 短信模板
 */
public enum MessageTemplateId {

    VERIFICATION_CODE("151275", "验证码"),
    INVITATION_CODE("165006", "邀请码");

    private String code;
    private String cnName;

    MessageTemplateId(String code, String cnName) {
        this.code = code;
        this.cnName = cnName;
    }

    public String getCode() {
        return code;
    }

    public String getCnName() {
        return cnName;
    }
}
