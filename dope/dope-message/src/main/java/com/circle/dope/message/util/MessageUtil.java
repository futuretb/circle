package com.circle.dope.message.util;

import com.cloopen.rest.sdk.CCPRestSmsSDK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@RefreshScope
@ConfigurationProperties(prefix = "sms")
public class MessageUtil {

    private final static Logger LOGGER = LoggerFactory.getLogger(MessageUtil.class);

    private static int corePoolSize = 20;
    private static int maximumPoolSize = 50;
    private static long keepAliveTime = 5;
    private static TimeUnit unit = TimeUnit.SECONDS;
    private static ArrayBlockingQueue workQueue = new ArrayBlockingQueue(10);

    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime,
            unit, workQueue);

    public void send(String phone, String templateId, String... content) {
        executor.execute(() -> {
            try {
                execute(phone, templateId, content);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private String serverIp;
    private String port;
    private String accountSid;
    private String accountToken;
    private String appid;

    private void execute(String phone, String templateId, String... datas) throws Exception {
        System.out.println(serverIp);
        HashMap<String, Object> result = null;
        //初始化SDK
        CCPRestSmsSDK restAPI = new CCPRestSmsSDK();
        //*初始化服务器地址和端口*
        restAPI.init(serverIp, port);
        //*初始化主帐号和主帐号令牌,对应官网开发者主账号下的ACCOUNT SID和AUTH TOKEN*
        restAPI.setAccount(accountSid, accountToken);
        //*初始化应用ID*
        restAPI.setAppId(appid);
        String[] message = null;
        result = restAPI.sendTemplateSMS(phone, templateId, datas);
        if("000000".equals(result.get("statusCode"))){
            //正常返回输出data包体信息（map）
            HashMap<String,Object> data = (HashMap<String, Object>) result.get("data");
            Set<String> keySet = data.keySet();
            for(String key:keySet){
                Object object = data.get(key);
                System.out.println(key +" = "+object);
            }
        }else{
            //异常返回输出错误码和错误信息
            System.out.println();
            LOGGER.info("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
        }
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAccountSid() {
        return accountSid;
    }

    public void setAccountSid(String accountSid) {
        this.accountSid = accountSid;
    }

    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }
}
