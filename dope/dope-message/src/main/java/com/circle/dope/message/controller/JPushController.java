package com.circle.dope.message.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.message.util.JPushUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

@Controller
@RequestMapping(value = "/message/jpush")
public class JPushController {


    @Resource
    private JPushUtil jPushUtil;

    @RequestMapping(value = "/push", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult push() {
        return SUCCESS;
    }

}
