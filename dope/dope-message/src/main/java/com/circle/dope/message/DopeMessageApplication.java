package com.circle.dope.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class DopeMessageApplication {

	public static void main(String[] args) {
		SpringApplication.run(DopeMessageApplication.class, args);
	}

}

