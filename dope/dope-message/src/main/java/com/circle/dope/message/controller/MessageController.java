package com.circle.dope.message.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.MatchUtil;
import com.circle.dope.framework.util.RandomUtil;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.message.enums.MessageTemplateId;
import com.circle.dope.message.util.MessageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

@Controller
@RequestMapping(value = "/message/sms")
public class MessageController {

    @Resource
    private RedisManager redisManager;

    @Resource
    private MessageUtil messageUtil;

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult sendMessage(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        String mobile = String.valueOf(params.get("mobile"));
        MatchUtil.matchMobile(mobile);
        String type = String.valueOf(params.get("type"));
        String key = PrefixEnum.MESSAGE_TYPE.findPrefixByType(type) + mobile;
        String value = RandomUtil.randomCode(4);
        String[] content = new String[2];
        content[0] = value;
        content[1] = "5分钟";
        int expireTime = 5 * 60;
        String templateId = MessageTemplateId.VERIFICATION_CODE.getCode();
        if (PrefixEnum.MESSAGE_TYPE.INVITE_CODE.name().equals(type)) {
            key = key + "_" + value;
            value = JSONObject.toJSONString(params.get("inviteUser"));
            expireTime = 24 * 60 * 60;
            content[1] = "24小时";
            templateId = MessageTemplateId.INVITATION_CODE.getCode();
        }
        redisManager.set(key, value, expireTime);
//        messageUtil.send(mobile, templateId, content);
        return SUCCESS.result(key + "---" + value);
    }
}
