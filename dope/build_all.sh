cd eureka-server/
./build.sh
cd /opt/code/dope/
cd api-gateway/
./build.sh
cd /opt/code/dope/
cd dope-user/dope-user-api/
mvn install
cd /opt/code/dope/
cd dope-user/dope-user-service/
./build.sh
cd /opt/code/dope/
cd dope-goods/dope-goods-api/
mvn install
cd /opt/code/dope/
cd dope-goods/dope-goods-service/
./build.sh
cd /opt/code/dope/
cd dope-order/dope-order-api/
mvn install
cd /opt/code/dope/
cd dope-order/dope-order-service/
./build.sh
cd /opt/code/dope/
cd dope-message/
./build.sh
cd /opt/code/dope/
cd config-server/
./build.sh
cd /opt/code/dope/
cd zipkin/
./build.sh
cd /opt/code/dope/
cd outside-service/
./build.sh
cd /opt/code/dope/
