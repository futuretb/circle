package com.circle.dope.order.dto;

import com.circle.dope.order.po.ConsigneeSnap;
import com.circle.dope.order.po.OrderInfo;
import com.circle.dope.order.po.OrderItem;

import java.io.Serializable;
import java.util.List;

public class CreateOrderDto implements Serializable {

    private OrderInfo orderInfo;
    private List<OrderItem> orderItems;
    private ConsigneeSnap consigneeSnap;

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public ConsigneeSnap getConsigneeSnap() {
        return consigneeSnap;
    }

    public void setConsigneeSnap(ConsigneeSnap consigneeSnap) {
        this.consigneeSnap = consigneeSnap;
    }
}
