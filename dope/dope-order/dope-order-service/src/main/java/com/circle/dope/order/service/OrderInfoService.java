package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.dto.CreateOrderDto;
import com.circle.dope.order.po.OrderInfo;

import java.util.List;

/**
 * 订单主表 - Service
 *
 *  Created by dicksoy on '2019-01-10 16:46:59'.
 *  Email dicksoy@163.com
 */
public interface OrderInfoService extends BaseService<OrderInfo> {

    boolean createOrder(List<CreateOrderDto> createOrderDtos);
}
