package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.po.OrderItem;

/**
 * 订单条目表 - Service
 *
 *  Created by dicksoy on '2019-01-22 11:25:14'.
 *  Email dicksoy@163.com
 */
public interface OrderItemService extends BaseService<OrderItem> {

}
