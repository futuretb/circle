package com.circle.dope.order.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.order.po.OrderInfo;

/**
 * 订单主表 - Dao
 * 
 *  Created by dicksoy on '2019-01-22 11:26:57'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface OrderInfoDao extends BaseDao<OrderInfo> {

}
