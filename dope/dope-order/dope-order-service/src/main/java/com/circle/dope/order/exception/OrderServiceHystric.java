package com.circle.dope.order.exception;

import com.circle.dope.framework.base.BaseHystric;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.order.client.GoodsServiceClient;
import com.circle.dope.order.client.UserServiceClient;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OrderServiceHystric implements GoodsServiceClient, UserServiceClient {

    private static final BaseHystric HYSTRIC = BaseHystric.newInstance(OrderServiceHystric.class);

    @Override
    public BaseResult findGoodsDetailById(Long id) {
        return HYSTRIC.defaultMessage();
    }

    @Override
    public BaseResult priceCalculation(Map<String, Object> params) {
        return HYSTRIC.defaultMessage();
    }

    @Override
    public BaseResult findByWxUserCode(String wxUserCode) {
        return HYSTRIC.defaultMessage();
    }
}
