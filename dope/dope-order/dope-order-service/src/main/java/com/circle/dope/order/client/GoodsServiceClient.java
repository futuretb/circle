package com.circle.dope.order.client;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.order.exception.OrderServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 只继承通用
 */
@FeignClient(name = "dope-goods-service", path = "/goods", fallback = OrderServiceHystric.class)
public interface GoodsServiceClient {

    @RequestMapping(value = "/goodsDetail/findById", method = RequestMethod.POST)
    BaseResult findGoodsDetailById(@RequestParam("id") Long id);

    @RequestMapping(value = "/shoppingCar/priceCalculation", method = RequestMethod.POST)
    BaseResult priceCalculation(@RequestBody Map<String, Object> params);
}
