package com.circle.dope.order.impl;

import com.circle.dope.order.dao.OrderItemDao;
import com.circle.dope.order.service.OrderItemService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.po.OrderItem;

/**
 * 订单条目表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-22 11:25:14'.
 *  Email dicksoy@163.com
 */
@Service
public class OrderItemServiceImpl extends BaseServiceImpl<OrderItemDao, OrderItem>
	implements OrderItemService {

}
