package com.circle.dope.order.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.order.po.RefundApply;

/**
 * 退款申请表 - Dao
 * 
 *  Created by dicksoy on '2019-01-22 11:25:02'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface RefundApplyDao extends BaseDao<RefundApply> {

}
