package com.circle.dope.order.controller;

import com.circle.dope.order.api.ConsigneeSnapApi;
import com.circle.dope.order.po.ConsigneeSnap;
import com.circle.dope.order.service.ConsigneeSnapService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 收货人快照 - Controller
 *
 *  Created by dicksoy on '2019-01-22 11:26:48'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/consigneeSnap")
public class ConsigneeSnapController {

}
