package com.circle.dope.order.controller;

import com.circle.dope.order.api.OrderItemApi;
import com.circle.dope.order.po.OrderItem;
import com.circle.dope.order.service.OrderItemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 订单条目表 - Controller
 *
 *  Created by dicksoy on '2019-01-22 11:25:14'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/orderItem")
public class OrderItemController {

}
