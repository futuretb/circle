package com.circle.dope.order.exception;

import com.circle.dope.framework.exception.GlobalDopeExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class GoodsExceptionHandler extends GlobalDopeExceptionHandler {
}
