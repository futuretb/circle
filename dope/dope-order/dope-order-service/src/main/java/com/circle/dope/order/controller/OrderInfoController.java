package com.circle.dope.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.base.PageResult;
import com.circle.dope.framework.constant.DopeTopics;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.order.client.GoodsServiceClient;
import com.circle.dope.order.client.UserServiceClient;
import com.circle.dope.order.dto.CreateOrderDto;
import com.circle.dope.order.po.OrderInfo;
import com.circle.dope.order.po.OrderItem;
import com.circle.dope.order.service.OrderInfoService;
import com.circle.dope.order.service.OrderItemService;
import com.circle.dope.order.util.InventoryUtil;
import com.circle.dope.order.util.SnowFlakeId;
import com.circle.dope.user.po.WxUserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 订单主表 - Controller
 *
 *  Created by dicksoy on '2019-01-10 16:46:59'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/orderInfo")
public class OrderInfoController {

    private final static Logger LOGGER = LoggerFactory.getLogger(OrderInfoController.class);

    @Resource
    private OrderInfoService orderInfoService;

    @Resource
    private RedisManager redisManager;

    @Resource
    private InventoryUtil inventoryUtil;

    @Resource
    private GoodsServiceClient goodsServiceClient;

    @Resource
    private UserServiceClient userServiceClient;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Resource
    private OrderItemService orderItemService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    @ResponseBody
    public BaseResult test(String type) {
        redisManager.set("zhang", 123123+"");
        if (null == type) {
            return goodsServiceClient.findGoodsDetailById(1L);
        }
        return SUCCESS;
    }

    @RequestMapping(value = "/createOrder", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult createOrder(@RequestBody List<CreateOrderDto> createOrderDtos,
                                  @RequestParam(UserType.BUYER_TOKEN) String buyerToken) {
        AssertUtil.notNull(createOrderDtos);
        BaseResult baseResult = userServiceClient.findByWxUserCode(buyerToken);
        AssertUtil.notNull(baseResult);
        WxUserInfo wxUserInfo = JSONObject.parseObject(JSONObject.toJSONString(baseResult.getResult()), WxUserInfo.class);
        final String lockKey = PrefixEnum.LOCK_TYPE.ORDER_LOCK.getPrefix() + buyerToken;
        boolean lockResult = redisManager.lockFree(lockKey, 5 * 1000);
        boolean flag = false;
        if (!lockResult) {
            return FAILD.msg("您的手速太快了，服务器还没来及响应，请稍后再试！");
        }
        List<GoodsDetail> remnantInventoryList = new ArrayList<>();
        try {
            for (CreateOrderDto createOrderDto: createOrderDtos) {
                List<OrderItem> orderItems = createOrderDto.getOrderItems();
                OrderInfo orderInfo = createOrderDto.getOrderInfo();
                BigDecimal finalPrice = new BigDecimal("0");
                for (OrderItem item : orderItems) {
                    BaseResult result = goodsServiceClient.findGoodsDetailById(item.getGoodsDetailId());
                    AssertUtil.notNull(result);
                    GoodsDetail goodsDetail = JSONObject.parseObject(JSONObject.toJSONString(result.getResult()), GoodsDetail.class);
                    item.setOriginalPrice(goodsDetail.getStorePrice());
                    Map<String, Object> calculationPriceMap = new HashMap<>();
                    calculationPriceMap.put("wxUserInfo", wxUserInfo);
                    calculationPriceMap.put("goodsDetailId", item.getGoodsDetailId());
                    calculationPriceMap.put("storeInfoId", orderInfo.getStoreId());
                    BaseResult priceResult = goodsServiceClient.priceCalculation(calculationPriceMap);
                    AssertUtil.notNull(priceResult);
                    BigDecimal itemPrice = new BigDecimal(String.valueOf(priceResult.getResult()));
                    item.setFinalPrice(itemPrice);
                    finalPrice = finalPrice.add(itemPrice);
                    remnantInventoryList.add(inventoryUtil.updateInventory(goodsDetail, item.getGoodsCount()));
                }
                orderInfo.setTotalFinalPrice(finalPrice);
                orderInfo.setOrderNum(String.valueOf(new SnowFlakeId().nextId()));
            }
            flag = orderInfoService.createOrder(createOrderDtos);
            kafkaTemplate.send(DopeTopics.UPDATE_INVENTORY, JSONObject.toJSONString(remnantInventoryList));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                redisManager.unlockFree(lockKey);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return flag ? SUCCESS : FAILD;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody Map<String, Object> params,
                             @RequestParam(value = UserType.BUYER_TOKEN) String buyerToken,
                             @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        AssertUtil.notNull(params);
        OrderInfo orderInfo = JSONObject.parseObject(JSONObject.toJSONString(params.get("orderInfo")), OrderInfo.class);
        if (null != orderInfo && null != orderInfo.getId()) {
            OrderInfo dbOrderInfo = orderInfoService.findById(orderInfo.getId());
            AssertUtil.notNull(dbOrderInfo);
            // 买家操作
            if (!StringUtils.isEmpty(buyerToken)) {
                orderInfoService.updateByCondition(null);
            }
            // 卖家操作
            if (!StringUtils.isEmpty(sellerToken)) {
                orderInfoService.updateByCondition(null);
            }
            return SUCCESS;
        }
        return FAILD;
    }

    @RequestMapping(value = "/findByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByCondition(@RequestBody Map<String, Object> params,
                                      @RequestParam(value = UserType.BUYER_TOKEN) String buyerToken,
                                      @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        AssertUtil.notNull(params);
        List<OrderInfo> list = null;
        PageResult<OrderInfo> pageResult = null;
        ListResult<OrderInfo> listResult = null;
        if (null != params.get("offset") && null != params.get("limit")) {
            pageResult = orderInfoService.findListByPage(params);
            AssertUtil.notNull(pageResult);
            list = pageResult.getList();
        } else {
            listResult = orderInfoService.findList(params);
            AssertUtil.notNull(listResult);
            list = listResult.getList();
        }
        for (OrderInfo orderInfo: list) {
            if (null != orderInfo) {
                Map<String, Object> orderItemMap = new HashMap<>();
                orderItemMap.put("orderId", orderInfo.getId());
                ListResult<OrderItem> orderItemListResult = orderItemService.findList(orderItemMap);
                orderInfo.setOtherData(orderItemListResult);
            }
        }
        if (null == pageResult) {
            listResult.setList(list);
            return SUCCESS.result(listResult);
        } else {
            pageResult.setList(list);
            return SUCCESS.result(pageResult);
        }
    }
}
