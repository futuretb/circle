package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.po.OrderItemExtend;

/**
 * 订单条目拓展表 - Service
 *
 *  Created by dicksoy on '2019-01-22 11:25:07'.
 *  Email dicksoy@163.com
 */
public interface OrderItemExtendService extends BaseService<OrderItemExtend> {

}
