package com.circle.dope.order.client;

import com.circle.dope.order.exception.OrderServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "dope-message", path = "/message", fallback = OrderServiceHystric.class)
public interface MessageServiceClient {
}
