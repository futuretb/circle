package com.circle.dope.order.controller;

import com.circle.dope.order.api.OrderItemExtendApi;
import com.circle.dope.order.po.OrderItemExtend;
import com.circle.dope.order.service.OrderItemExtendService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 订单条目拓展表 - Controller
 *
 *  Created by dicksoy on '2019-01-22 11:25:07'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/orderItemExtend")
public class OrderItemExtendController {

}
