package com.circle.dope.order.impl;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.order.dao.OrderImageDao;
import com.circle.dope.order.dao.RefundApplyDao;
import com.circle.dope.order.dto.RefundApplyDto;
import com.circle.dope.order.po.OrderImage;
import com.circle.dope.order.service.RefundApplyService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.po.RefundApply;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 退款申请表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-17 13:01:57'.
 *  Email dicksoy@163.com
 */
@Service
public class RefundApplyServiceImpl extends BaseServiceImpl<RefundApplyDao, RefundApply>
		implements RefundApplyService {

	@Resource
	private OrderImageDao orderImageDao;

	@Override
	public boolean insertRefundByList(List<RefundApplyDto> refundApplyDtos) {
		for (RefundApplyDto refundApplyDto: refundApplyDtos) {
			RefundApply refundApply = refundApplyDto.getRefundApply();
			mapper.insert(refundApply);
			if (null != refundApplyDto.getOrderImage() && !refundApplyDto.getOrderImage().isEmpty()) {
				for (OrderImage orderImage: refundApplyDto.getOrderImage()) {
					orderImage.setOrderId(refundApply.getOrderId());
					orderImage.setTypeId(refundApply.getId());
					orderImage.setType("REFUND");
					orderImageDao.insert(orderImage);
				}
			}

		}
		return true;
	}
}
