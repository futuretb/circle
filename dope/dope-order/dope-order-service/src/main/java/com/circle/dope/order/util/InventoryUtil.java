package com.circle.dope.order.util;

import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.goods.po.GoodsDetail;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class InventoryUtil {

    @Resource
    private RedisManager redisManager;

    /**
     * 默认为减库存
     * @param goodsDetail
     * @param goodsCount
     * @return goodsDetail.Id, 剩余库存
     */
    public GoodsDetail updateInventory(GoodsDetail goodsDetail, Long goodsCount) {
        Integer goodsCountInt = Integer.valueOf(String.valueOf(goodsCount));
        if (goodsDetail.getSaleIn() == 0) {
            throw new RuntimeException("商品 : " + goodsDetail.getId() + "已下架");
        }
        String inventoryKey = PrefixEnum.GOODS_TYPE.INVENTORY.getPrefix() + goodsDetail.getId();
        String saleInKey = PrefixEnum.GOODS_TYPE.SALE_IN.getPrefix() + goodsDetail.getId();
        if (!redisManager.exists(inventoryKey)) {
            // 初始化库存
            redisManager.setnx(inventoryKey, String.valueOf(goodsDetail.getInventory()));
        }
        if (!redisManager.exists(saleInKey)) {
            // 初始化在售数量
            redisManager.setnx(saleInKey, String.valueOf(goodsDetail.getSaleIn()));
        }
        String currInventory = redisManager.get(inventoryKey);
        String currSaleIn = redisManager.get(saleInKey);
        if (Integer.valueOf(currInventory) - goodsCountInt < 0 ||
                Integer.valueOf(currSaleIn) - goodsCountInt < 0) {
            throw new RuntimeException("商品 : " + goodsDetail.getId() + "库存不足");
        }
        // 剩余库存
        Long remnantInventory = redisManager.incrBy(currInventory, -goodsCountInt);
        Long remnantSaleIn = redisManager.incrBy(currSaleIn, -goodsCountInt);
        GoodsDetail updateGoodsInventory = new GoodsDetail();
        updateGoodsInventory.setId(goodsDetail.getId());
        updateGoodsInventory.setSaleIn(remnantSaleIn);
        updateGoodsInventory.setInventory(remnantInventory);
        return updateGoodsInventory;
    }
}
