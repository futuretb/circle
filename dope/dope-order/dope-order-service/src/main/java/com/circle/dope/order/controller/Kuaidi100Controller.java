package com.circle.dope.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.RequestUtil;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.Code.OK;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

@Controller
@RequestMapping(value = "/kuaidi100")
public class Kuaidi100Controller {

    @Value("kuaidi100.callbackurl")
    private String callbackUrl;

    @Value("kuaidi100.key")
    private String key;

    @Value("kuaidi100.customer")
    private String customer;

    private static final String QUERY = "http://poll.kuaidi100.com/poll/query.do";
    private static final String POLL = "http://www.kuaidi100.com/poll";

    @RequestMapping("/informCallback")
    public void informCallback(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> responseMap = new HashMap<>();
        try {
            String responseStr = request.getParameter("param");
            Map<String, Object> result = JSONObject.parseObject(responseStr, HashMap.class);
            if (true) {
                responseMap.put("result", true);
                responseMap.put("returnCode", 200);
            } else {
                responseMap.put("message", "保存失败");
            }
            //这里必须返回，否则认为失败，过30分钟又会重复推送。
        } finally {
            try {
                response.getWriter().print(JSONObject.toJSONString(responseMap));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置推送
     * @param param company 物流公司 & number 物流单号
     * @param buyerToken
     * @param sellerToken
     */
    @RequestMapping(value = "subscribeInform", method = RequestMethod.POST)
    @ResponseBody
    public void subscribeInform(@RequestBody Map<String, Object> param,
                                @RequestParam(required = false, value = UserType.BUYER_TOKEN) String buyerToken,
                                @RequestParam(required = false, value = UserType.SELLER_TOKEN) String sellerToken) {
        param.put("key", key);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("callbackurl", callbackUrl);
        param.put("parameters", parameters);
        HashMap<String, String> schema = new HashMap<>();
        schema.put("schema", "json");
        schema.put("param", JSONObject.toJSONString(param));
        try {
            Map<String, Object> response = RequestUtil.doPost(POLL, schema);
            System.out.println(response);
            Integer returnCode = Integer.valueOf(String.valueOf(response.get("returnCode")));
            if (OK == returnCode) {
                System.out.println("成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取物流详情
     * @param param
     * @param buyerToken
     * @param sellerToken
     * @return
     */
    @RequestMapping(value = "/getLogisticsInfo", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult getLogisticsInfo(@RequestBody Map<String, Object> param,
                                 @RequestParam(required = false, value = UserType.BUYER_TOKEN) String buyerToken,
                                 @RequestParam(required = false, value = UserType.SELLER_TOKEN) String sellerToken) {
        String paramStr = JSONObject.toJSONString(param);
        Map<String, String> params = new HashMap<>();
        params.put("param", paramStr);
        params.put("sign", getSign(JSONObject.toJSONString(param) + key + customer));
        params.put("customer",customer);
        Map<String, Object> response = new HashMap<>();
        try {
            response = RequestUtil.doPost(QUERY, params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SUCCESS.result(response);
    }

    private String getSign(String content) {
        return MD5Encoder.encode(content.getBytes()).toUpperCase();
    }
}