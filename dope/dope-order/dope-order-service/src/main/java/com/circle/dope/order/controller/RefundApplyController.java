package com.circle.dope.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.DopeTopics;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.order.api.RefundApplyApi;
import com.circle.dope.order.client.GoodsServiceClient;
import com.circle.dope.order.dto.RefundApplyDto;
import com.circle.dope.order.enums.ORDER_ITEM_STATUS;
import com.circle.dope.order.enums.REFUND_STATUS;
import com.circle.dope.order.po.OrderItem;
import com.circle.dope.order.po.RefundApply;
import com.circle.dope.order.service.OrderItemService;
import com.circle.dope.order.service.RefundApplyService;
import com.circle.dope.order.util.InventoryUtil;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.FAILD;
import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 退款申请表 - Controller
 *
 *  Created by dicksoy on '2019-01-17 13:01:57'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/refundApply")
public class RefundApplyController {

    @Resource
    private RefundApplyService refundApplyService;

    @Resource
    private OrderItemService orderItemService;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Resource
    private RedisManager redisManager;

    @Resource
    private InventoryUtil inventoryUtil;

    @Resource
    private GoodsServiceClient goodsServiceClient;

    /**
     * 1、买家申请退款
     * 3、同意 - > 退货信息填写
     * 4、拒绝 - > 申请官方介入
     * @return
     */
    @RequestMapping(value = "/buyerOperation", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult buyerOperation(@RequestBody Map<String, Object> params,
                                     @RequestParam(UserType.BUYER_TOKEN) String buyerToken) {
        AssertUtil.notNull(params);
        List<RefundApplyDto> refundApplyDtos = JSONObject.parseArray(JSONObject.toJSONString(params), RefundApplyDto.class);
        List<GoodsDetail> goodsDetails = new ArrayList<>();
        for (RefundApplyDto refundApplyDto: refundApplyDtos) {
            RefundApply refundApply = refundApplyDto.getRefundApply();
            refundApply.setBuyerUserCode(buyerToken);
            OrderItem orderItem = orderItemService.findById(refundApply.getOrderItemId());
            Assert.notNull(orderItem, "当前订单商品不存在");
            // TODO 创建的订单可直接取消
            if (ORDER_ITEM_STATUS.CREATED.name().equals(orderItem.getStatus())) {
                refundApply.setSellerUserId("SYSTEM");
                refundApply.setStatus(REFUND_STATUS.SYSTEM_AGREE.name());
                BaseResult result = goodsServiceClient.findGoodsDetailById(orderItem.getGoodsDetailId());
                AssertUtil.notNull(result);
                GoodsDetail goodsDetail = JSONObject.parseObject(JSONObject.toJSONString(result.getResult()), GoodsDetail.class);
                goodsDetails.add(inventoryUtil.updateInventory(goodsDetail, -orderItem.getGoodsCount()));
            }
            // TODO 已发货 || 已收货的订单
            if (ORDER_ITEM_STATUS.SHIPPED.name().equals(orderItem.getStatus())
                    || ORDER_ITEM_STATUS.RECEIPTED.name().equals(orderItem.getStatus())) {
                refundApply.setStatus(REFUND_STATUS.BUYER_APPLY.name());
            }
        }
        if (goodsDetails.size() > 0) {
            kafkaTemplate.send(DopeTopics.UPDATE_INVENTORY, JSONObject.toJSONString(goodsDetails));
        }
        return refundApplyService.insertRefundByList(refundApplyDtos) ? SUCCESS : FAILD;
    }

    /**
     * 2、卖家同意/拒绝
     * 5、收到货 - > 同意退款
     * 6、未收到货/虚假 - > 官方介入 - > 拒绝退款
     * 7、订单状态完成 - > 指定商品退款（价格待定）
     * @param params
     * @return
     */
    @RequestMapping(value = "/sellerOperation", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult sellerOperation(@RequestBody Map<String, Object> params,
                                      @RequestParam(UserType.SELLER_TOKEN) String sellerToken) {
        List<RefundApplyDto> refundApplyDtos = JSONObject.parseArray(JSONObject.toJSONString(params), RefundApplyDto.class);
        for (RefundApplyDto refundApplyDto: refundApplyDtos) {
            RefundApply refundApply = refundApplyDto.getRefundApply();
            refundApply.setSellerUserId(sellerToken);
        }
        return refundApplyService.insertRefundByList(refundApplyDtos) ? SUCCESS : FAILD;
    }
}
