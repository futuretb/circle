package com.circle.dope.order.impl;

import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.dao.ConsigneeSnapDao;
import com.circle.dope.order.dao.OrderInfoDao;
import com.circle.dope.order.dao.OrderItemDao;
import com.circle.dope.order.dto.CreateOrderDto;
import com.circle.dope.order.po.ConsigneeSnap;
import com.circle.dope.order.po.OrderInfo;
import com.circle.dope.order.po.OrderItem;
import com.circle.dope.order.service.OrderInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 订单主表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-10 16:46:59'.
 *  Email dicksoy@163.com
 */
@Service
public class OrderInfoServiceImpl extends BaseServiceImpl<OrderInfoDao, OrderInfo>
	implements OrderInfoService {

	@Resource
	private OrderItemDao orderItemDao;

	@Resource
	private ConsigneeSnapDao consigneeSnapDao;

	@Override
	public boolean createOrder(List<CreateOrderDto> createOrderDtos) {
		for (CreateOrderDto createOrderDto: createOrderDtos) {
			ConsigneeSnap consigneeSnap = createOrderDto.getConsigneeSnap();
			consigneeSnapDao.insert(consigneeSnap);
			OrderInfo orderInfo = createOrderDto.getOrderInfo();
			orderInfo.setConsigneeSnapId(consigneeSnap.getId());
			List<OrderItem> orderItems = createOrderDto.getOrderItems();
			mapper.insertByCondition(orderInfo);
			Long orderId = orderInfo.getId();
			for (OrderItem item: orderItems) {
				item.setOrderId(orderId);
				item.setOrderNum(orderInfo.getOrderNum());
				orderItemDao.insert(item);
			}
		}
		return true;
	}
}
