package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.po.ConsigneeSnap;

/**
 * 收货人快照 - Service
 *
 *  Created by dicksoy on '2019-01-22 11:26:48'.
 *  Email dicksoy@163.com
 */
public interface ConsigneeSnapService extends BaseService<ConsigneeSnap> {

}
