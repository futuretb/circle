package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.dto.RefundApplyDto;
import com.circle.dope.order.po.OrderImage;
import com.circle.dope.order.po.RefundApply;

import java.util.List;

/**
 * 退款申请表 - Service
 *
 *  Created by dicksoy on '2019-01-17 13:01:57'.
 *  Email dicksoy@163.com
 */
public interface RefundApplyService extends BaseService<RefundApply> {

    boolean insertRefundByList(List<RefundApplyDto> refundApplyDtos);
}
