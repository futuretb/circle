package com.circle.dope.order.impl;

import com.circle.dope.order.dao.ConsigneeSnapDao;
import com.circle.dope.order.service.ConsigneeSnapService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.po.ConsigneeSnap;

/**
 * 收货人快照 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-22 11:26:48'.
 *  Email dicksoy@163.com
 */
@Service
public class ConsigneeSnapServiceImpl extends BaseServiceImpl<ConsigneeSnapDao, ConsigneeSnap>
	implements ConsigneeSnapService {

}
