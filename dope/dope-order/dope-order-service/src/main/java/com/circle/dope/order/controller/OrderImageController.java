package com.circle.dope.order.controller;

import com.circle.dope.order.api.OrderImageApi;
import com.circle.dope.order.po.OrderImage;
import com.circle.dope.order.service.OrderImageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 订单图片表 - Controller
 *
 *  Created by dicksoy on '2019-01-22 11:26:52'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/order/orderImage")
public class OrderImageController {

}
