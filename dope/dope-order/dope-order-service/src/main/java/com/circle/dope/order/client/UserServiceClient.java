package com.circle.dope.order.client;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.order.exception.OrderServiceHystric;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 不直接继承UserInfoApi（松耦合）
 * 只继承通用
 */
@FeignClient(name = "dope-user-service", path = "/user", fallback = OrderServiceHystric.class)
public interface UserServiceClient {

    @RequestMapping(value = "/wxUserInfo/findByWxUserCode", method = RequestMethod.POST)
    public BaseResult findByWxUserCode(@RequestBody String wxUserCode);
}
