package com.circle.dope.order.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.order.po.OrderImage;

/**
 * 订单图片表 - Service
 *
 *  Created by dicksoy on '2019-01-22 11:26:52'.
 *  Email dicksoy@163.com
 */
public interface OrderImageService extends BaseService<OrderImage> {

}
