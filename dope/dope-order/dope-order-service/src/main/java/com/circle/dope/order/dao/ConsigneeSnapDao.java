package com.circle.dope.order.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.order.po.ConsigneeSnap;

/**
 * 收货人快照 - Dao
 * 
 *  Created by dicksoy on '2019-01-22 11:26:48'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ConsigneeSnapDao extends BaseDao<ConsigneeSnap> {

}
