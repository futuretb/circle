package com.circle.dope.order.dto;

import com.circle.dope.order.po.OrderImage;
import com.circle.dope.order.po.RefundApply;

import java.io.Serializable;
import java.util.List;

public class RefundApplyDto implements Serializable {

    private RefundApply refundApply;
    private List<OrderImage> orderImage;

    public RefundApply getRefundApply() {
        return refundApply;
    }

    public void setRefundApply(RefundApply refundApply) {
        this.refundApply = refundApply;
    }

    public List<OrderImage> getOrderImage() {
        return orderImage;
    }

    public void setOrderImage(List<OrderImage> orderImage) {
        this.orderImage = orderImage;
    }
}
