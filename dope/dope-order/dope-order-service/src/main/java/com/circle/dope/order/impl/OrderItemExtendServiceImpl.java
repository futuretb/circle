package com.circle.dope.order.impl;

import com.circle.dope.order.dao.OrderItemExtendDao;
import com.circle.dope.order.service.OrderItemExtendService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.po.OrderItemExtend;

/**
 * 订单条目拓展表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-22 11:25:07'.
 *  Email dicksoy@163.com
 */
@Service
public class OrderItemExtendServiceImpl extends BaseServiceImpl<OrderItemExtendDao, OrderItemExtend>
	implements OrderItemExtendService {

}
