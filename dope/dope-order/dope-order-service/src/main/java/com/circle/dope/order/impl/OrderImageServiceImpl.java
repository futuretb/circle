package com.circle.dope.order.impl;

import com.circle.dope.order.dao.OrderImageDao;
import com.circle.dope.order.service.OrderImageService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.order.po.OrderImage;

/**
 * 订单图片表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-22 11:26:52'.
 *  Email dicksoy@163.com
 */
@Service
public class OrderImageServiceImpl extends BaseServiceImpl<OrderImageDao, OrderImage>
	implements OrderImageService {

}
