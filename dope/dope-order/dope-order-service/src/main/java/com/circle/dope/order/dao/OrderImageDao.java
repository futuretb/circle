package com.circle.dope.order.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.order.po.OrderImage;

/**
 * 订单图片表 - Dao
 * 
 *  Created by dicksoy on '2019-01-22 11:26:52'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface OrderImageDao extends BaseDao<OrderImage> {

}
