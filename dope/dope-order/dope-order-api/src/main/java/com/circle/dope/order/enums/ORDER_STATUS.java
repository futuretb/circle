package com.circle.dope.order.enums;

public enum ORDER_STATUS {

    CREATED("创建"),
    CLOSED("关闭"),
    FINISHED("完成");

    private String cnName;

    ORDER_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }

}
