package com.circle.dope.order.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
import java.math.BigDecimal;
/**
 *  退款申请表 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:25:02'.
 *  Email dicksoy@163.com
 */
public class RefundApply extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 订单条目id
     */
    private Long orderItemId;

    /**
     * 退款金额
     */
    private BigDecimal refundMoney;

    /**
     * 状态
     */
    private String status;

    /**
     * 买家userCode
     */
    private String buyerUserCode;

    /**
     * 卖家userId
     */
    private String sellerUserId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 退款类型 ONLY_REFUND 仅退款 RETURN_REFUND 退货退款 EXCHANGE 换货
     */
    private String type;

    /**
     * 物流公司
     */
    private String logisticsCompany;

    /**
     * 物流单号
     */
    private String logisticsNumber;

    /**
     * 联系电话
     */
    private String phone;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public BigDecimal getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(BigDecimal refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBuyerUserCode() {
        return buyerUserCode;
    }

    public void setBuyerUserCode(String buyerUserCode) {
        this.buyerUserCode = buyerUserCode;
    }

    public String getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(String sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsNumber() {
        return logisticsNumber;
    }

    public void setLogisticsNumber(String logisticsNumber) {
        this.logisticsNumber = logisticsNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}