package com.circle.dope.order.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  收货人快照 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:26:48'.
 *  Email dicksoy@163.com
 */
public class ConsigneeSnap extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String area;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 身份证号
     */
    private String identityCardNum;

    /**
     * 身份证正面
     */
    private String identityCardImageFront;

    /**
     * 身份证反面
     */
    private String identityCardImageBack;

    /**
     * 修改时间
     */
    private Date updatedTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdentityCardNum() {
        return identityCardNum;
    }

    public void setIdentityCardNum(String identityCardNum) {
        this.identityCardNum = identityCardNum;
    }

    public String getIdentityCardImageFront() {
        return identityCardImageFront;
    }

    public void setIdentityCardImageFront(String identityCardImageFront) {
        this.identityCardImageFront = identityCardImageFront;
    }

    public String getIdentityCardImageBack() {
        return identityCardImageBack;
    }

    public void setIdentityCardImageBack(String identityCardImageBack) {
        this.identityCardImageBack = identityCardImageBack;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

}