package com.circle.dope.order.enums;

public enum ORDER_ITEM_STATUS {

    CREATED("创建"),
    CONFIRMED("确认"),
    SHIPPED("已发货"),
    RECEIPTED("已收货"),
    REFUND("退款"),
    CLOSED("关闭"),
    FINISHED("完成");

    private String cnName;

    ORDER_ITEM_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }

}
