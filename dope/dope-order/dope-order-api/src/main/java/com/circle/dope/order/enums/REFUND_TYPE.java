package com.circle.dope.order.enums;

public enum REFUND_TYPE {

    ONLY_REFUND("仅退款"),
    RETURN_REFUND("退货退款"),
    EXCHANGE("换货");

    private String cnName;

    REFUND_TYPE(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }

}
