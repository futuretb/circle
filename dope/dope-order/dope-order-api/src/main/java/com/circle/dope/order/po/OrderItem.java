package com.circle.dope.order.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
import java.math.BigDecimal;
/**
 *  订单条目表 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:25:14'.
 *  Email dicksoy@163.com
 */
public class OrderItem extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 订单编号
     */
    private String orderNum;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 详情id
     */
    private Long goodsDetailId;

    /**
     * 买家code
     */
    private String buyerUserCode;

    /**
     * 卖家id
     */
    private String sellerUserId;

    /**
     * 交易类型
     */
    private String deliveryType;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品图片
     */
    private String goodsPicture;

    /**
     * 原价
     */
    private BigDecimal originalPrice;

    /**
     * 最终价格
     */
    private BigDecimal finalPrice;

    /**
     * 优惠类型
     */
    private String discountsType;

    /**
     * 优惠金额
     */
    private BigDecimal discountsPrice;

    /**
     * 状态
     */
    private String status;

    /**
     * 尺码
     */
    private String size;

    /**
     * 颜色
     */
    private String color;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 商品数量
     */
    private Long goodsCount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getGoodsDetailId() {
        return goodsDetailId;
    }

    public void setGoodsDetailId(Long goodsDetailId) {
        this.goodsDetailId = goodsDetailId;
    }

    public String getBuyerUserCode() {
        return buyerUserCode;
    }

    public void setBuyerUserCode(String buyerUserCode) {
        this.buyerUserCode = buyerUserCode;
    }

    public String getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(String sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsPicture() {
        return goodsPicture;
    }

    public void setGoodsPicture(String goodsPicture) {
        this.goodsPicture = goodsPicture;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getDiscountsType() {
        return discountsType;
    }

    public void setDiscountsType(String discountsType) {
        this.discountsType = discountsType;
    }

    public BigDecimal getDiscountsPrice() {
        return discountsPrice;
    }

    public void setDiscountsPrice(BigDecimal discountsPrice) {
        this.discountsPrice = discountsPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Long goodsCount) {
        this.goodsCount = goodsCount;
    }

}