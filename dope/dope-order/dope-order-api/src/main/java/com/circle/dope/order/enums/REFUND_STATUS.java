package com.circle.dope.order.enums;

public enum REFUND_STATUS {

    SYSTEM_AGREE("系统同意"),
    BUYER_APPLY("买家申请"),
    SELLER_REJECT("卖家拒绝"),
    SELLER_AGREE("卖家同意"),
    BUYER_SHIP("买家退货"),
    SELLER_RECEIPT("卖家收货"),
    SELLER_REFUND("卖家退款");

    private String cnName;

    REFUND_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }

}
