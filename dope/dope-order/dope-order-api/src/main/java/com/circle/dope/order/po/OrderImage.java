package com.circle.dope.order.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  订单图片表 - Po
 *
 *  Created by dicksoy on '2019-01-22 11:26:52'.
 *  Email dicksoy@163.com
 */
public class OrderImage extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 图片链接
     */
    private String imageUrl;

    /**
     * 类型 ORDER_ITEM 订单条目 REFUND 退款
     */
    private String type;

    /**
     * 对应类型id
     */
    private Long typeId;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 订单id
     */
    private Long orderId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

}