package com.circle.outside.service.config;

import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Configuration
public class QiNiuConfig {

    @Value("${qiniu.accesskey}")
    private String accesskey;
    @Value("${qiniu.secretkey}")
    private String secretkey;
    @Value("${qiniu.bucket}")
    private String bucket;
    @Value("${qiniu.baseurl}")
    private String baseurl;

    public String imageToken() {
        Auth auth = Auth.create(accesskey, secretkey);
        return auth.uploadToken(bucket);
    }

    public String getAccesskey() {
        return accesskey;
    }

    public void setAccesskey(String accesskey) {
        this.accesskey = accesskey;
    }

    public String getSecretkey() {
        return secretkey;
    }

    public void setSecretkey(String secretkey) {
        this.secretkey = secretkey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }
}
