package com.circle.outside.service.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.QRCodeUtil;
import com.circle.outside.service.config.QiNiuConfig;
import com.circle.outside.service.util.UploadUtil;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

@Controller
@RequestMapping(value = "/outside/image")

public class ImageController {

    @Resource
    private QiNiuConfig qiNiuConfig;

    @RequestMapping(value = "/getUpToken", method = RequestMethod.GET)
    @ResponseBody
    public BaseResult getUpToken(@RequestParam(value = UserType.BUYER_TOKEN, required = false) String buyerToken,
                                 @RequestParam(value = UserType.SELLER_TOKEN, required = false) String sellerToken){
        return SUCCESS.result(qiNiuConfig.imageToken());
    }

    @RequestMapping(value = "/generateUploadQrCode", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult generateUploadQrCode(@RequestBody Map<String, Object> params) throws IOException {
        AssertUtil.notNull(params);
        URL uri = new URL(String.valueOf(params.get("logoUrl")));
        InputStream logo = uri.openStream();
        String url = String.valueOf(params.get("qrCodeUrl"));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        QRCodeUtil.generateQRImageWithLogo(url,400, logo, byteArrayOutputStream);
        String qrCode = UploadUtil.fileUpload(byteArrayOutputStream, qiNiuConfig.imageToken());
        return SUCCESS.result(qiNiuConfig.getBaseurl() + "/" + qrCode);
    }

}
