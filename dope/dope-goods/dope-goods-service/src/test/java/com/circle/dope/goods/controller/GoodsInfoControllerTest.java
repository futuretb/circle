package com.circle.dope.goods.controller;


import com.alibaba.fastjson.JSONArray;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.po.GoodsInfo;
import com.circle.dope.goods.po.GoodsPicture;
import com.circle.dope.goods.po.MiddleGoodsCategory;
import com.circle.dope.goods.service.GoodsInfoService;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.omg.CORBA.ObjectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Wenqiang on 2019/1/9.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GoodsInfoControllerTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(GoodsInfoControllerTest.class);

    @Resource
    private GoodsInfoService goodsInfoService;

    @Test
    public void createGoods() throws Exception {
        GoodsInfo goodsInfo = new GoodsInfo();
        List<MiddleGoodsCategory> mgcList = new ArrayList<>();
        List<GoodsDetail > gdList = new ArrayList<>();
        List<GoodsPicture> picList = new ArrayList<>();
        String sizes[] = new String[]{"L", "S", "m"};
        Long categories[] = new Long[]{12L, 13L, 14L};
        String pictures[] = new String[]{"http://image.235shanghai.com/Medici26560411-0.jpg", "http://image.235shanghai.com/Medici26746079-0.jpg", "http://image.235shanghai.com/Medici18643267-0.jpg"};
        /**
         * goodsInfo
         */
        goodsInfo.setAvgCost(new BigDecimal(10.00));
        goodsInfo.setPicture("http://images.shopxyy.com/Uploads/weixin/image/20190121/20190121095749_66952.jpg");
        goodsInfo.setBrandId(14L);
        goodsInfo.setFreight("到付");
        goodsInfo.setGoodsDesc("BALMAIN裸色 刘雯同款 双排扣羊毛西装 RF17204W022 0KK");
        goodsInfo.setGoodsNumber("货号");
        goodsInfo.setName("BALMAIN");
        goodsInfo.setSex("2");
        goodsInfo.setSendDate("一周内");
        goodsInfo.setUserId("123456");
        goodsInfo.setTariff(new BigDecimal(0));
        goodsInfo.setStoreId(1L);
        /**
         * goodsDetail
         */
        GoodsDetail goodsDetail = null;
        for (String size: sizes) {
            goodsDetail = new GoodsDetail();
            goodsDetail.setAgentPrice(new BigDecimal(12.00));
            goodsDetail.setColor("黑色");
            goodsDetail.setInventory(new Long(10));
            goodsDetail.setIsPresell(0);
            goodsDetail.setSize(size);
            gdList.add(goodsDetail);
        }
        /**
         * cagegoryInfo
         */
        MiddleGoodsCategory middleGoodsCategory = null;
        for (Long id: categories) {
            middleGoodsCategory = new MiddleGoodsCategory();
            middleGoodsCategory.setCategoryId(id);
            mgcList.add(middleGoodsCategory);
        }
        /**
         * picture
         */
        GoodsPicture goodsPicture = null;
        for (String pic: pictures) {
            goodsPicture = new GoodsPicture();
            goodsPicture.setPictureUrl(pic);
            picList.add(goodsPicture);
        }
        goodsInfoService.createGoods(goodsInfo, mgcList, gdList, picList);
    }

    @Test
    public void findByActivityId() throws Exception {
        Map<String, Object> map = new HashedMap();
        map.put("activityId", 1);
        map.put("wxUserId", "WX_54dd0711-0f77-445e-ae6e-55f6c7519378");
        map.put("offset", 0);
        map.put("pagesize", 10);
        ListResult<GoodsInfoVo> list = goodsInfoService.findByActivityId(map);
        LOGGER.info(JSONArray.toJSONString(list));
    }

    @Test
    public void goodsOne() throws Exception {
        Map<String, Object> map = new HashedMap();
        map.put("id", 1);
        goodsInfoService.goodsOne(map);
    }

    @Test
    public void findCircle() throws Exception {
        Map<String, Object> map = new HashedMap();
        map.put("wxUserId", "WX_54dd0711-0f77-445e-ae6e-55f6c7519378");
        map.put("offset", 0);
        map.put("pagesize", 10);
        goodsInfoService.findCircle(map);
    }

    @Test
    public void findGoods() throws Exception {
        Map<String, Object> map = new HashedMap();
        map.put("type", 1);
        map.put("storeId", 1);
        map.put("offset", 0);
        map.put("pagesize", 10);
        goodsInfoService.findGoods(map);
    }

    @Test
    public void statistics() throws Exception {
        long storeId = new Long(1);
        goodsInfoService.statistics(storeId);
    }

}
