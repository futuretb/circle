package com.circle.dope.goods.impl;

import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.dao.GoodsDetailDao;
import com.circle.dope.goods.dao.GoodsInfoDao;
import com.circle.dope.goods.dao.GoodsUpdatedDao;
import com.circle.dope.goods.param.vo.GoodsDetailVo;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.po.GoodsInfo;
import com.circle.dope.goods.po.GoodsUpdated;
import com.circle.dope.goods.service.GoodsDetailService;
import com.circle.dope.goods.utils.BigDecimalUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 商品明细表 - ServiceImpl
 *
 *  @author by dicksoy on '2019-01-06 18:49:10'.
 *  Email dicksoy@163.com
 */
@Service
public class GoodsDetailServiceImpl extends BaseServiceImpl<GoodsDetailDao, GoodsDetail>
	implements GoodsDetailService {

    @Resource
	private GoodsUpdatedDao goodsUpdatedDao;
    @Resource
    private GoodsInfoDao goodsInfoDao;

    @Override
    public void insert(List<GoodsDetail> gdList) {
        for (GoodsDetail goodsDetail: gdList) {
            mapper.insert(goodsDetail);
        }
    }

    @Override
	public void replenishment(List<GoodsDetailVo> gdList, String userCode) {
		GoodsDetail goodsDetail = null;
		BigDecimal totalCost = new BigDecimal(0);
		long totalInventory = 0L;
        long oldInventory = 0;
        if (gdList != null && gdList.size() > 0) {
            GoodsInfo goodsInfo = goodsInfoDao.findById(gdList.get(0).getGoodsId());
            for(GoodsDetailVo entity: gdList) {
                goodsDetail = mapper.findById(entity.getId());
                totalCost = totalCost.add(goodsInfo.getAvgCost().multiply(new BigDecimal(goodsDetail.getInventory())).add(entity.getGoodsCost().multiply(new BigDecimal(entity.getInventory()))));
                oldInventory = goodsDetail.getInventory();
                goodsDetail.setInventory(goodsDetail.getInventory() + entity.getInventory());
                totalInventory = totalInventory + entity.getInventory();
                mapper.update(goodsDetail);
                goodsUpdateHis(goodsDetail.getGoodsId(), goodsDetail.getId(), 1, goodsDetail.getInventory().toString(), String.valueOf(oldInventory), userCode);
            }
            BigDecimal newAvgCost = totalCost.divide(new BigDecimal(totalInventory), 2);
            goodsUpdateHis(goodsInfo.getId(), null, 2, goodsInfo.getAvgCost().toString(), newAvgCost.toString(), userCode);
            goodsInfo.setAvgCost(newAvgCost);
            goodsInfoDao.update(goodsInfo);
        }
	}

    /**
     * 记录商品修改历史
     * @param type 类型 1成本价格 2店铺售价 3代理价格 4 库存
     */
    public void goodsUpdateHis(Long goodsId, Long goodsDetailId, Integer type, String newDate, String oldDate, String userId) {
	    GoodsUpdated goodsUpdated = new GoodsUpdated();
	    goodsUpdated.setGoodsId(goodsId.toString());
        if (goodsDetailId != null) {
            goodsUpdated.setGoodsDetailId(goodsDetailId.toString());
        }
        goodsUpdated.setType(type);
        goodsUpdated.setNewData(newDate);
        goodsUpdated.setOldData(oldDate);
        goodsUpdated.setUserId(userId);
        goodsUpdatedDao.insert(goodsUpdated);
    }
}
