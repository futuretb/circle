package com.circle.dope.goods.impl;

import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.client.MessageClient;
import com.circle.dope.goods.dao.ActivityInfoDao;
import com.circle.dope.goods.dao.GoodsInfoDao;
import com.circle.dope.goods.dao.MiddleActivityGoodsDao;
import com.circle.dope.goods.param.output.ActivityinfoOutput;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.ActivityInfo;
import com.circle.dope.goods.po.MiddleActivityGoods;
import com.circle.dope.goods.service.ActivityInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.circle.dope.framework.constant.ConfFix.QINIU_URL;

/**
 * 活动发布 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-18 15:09:34'.
 *  Email dicksoy@163.com
 */
@Service
public class ActivityInfoServiceImpl extends BaseServiceImpl<ActivityInfoDao, ActivityInfo>
	implements ActivityInfoService {

	// 活动简介-商品显示数
	private final static Integer GOODSOFFSET = 0;
	private final static Integer GOODSLIMIT = 6;
	//品牌类目 活动
	private final static Integer BRANDORCATEGORYTYPE = 3;
	private final static Integer BRANDORCATEGORYOFFSET = 0;
	private final static Integer BRANDORCATEGORYLIMIT = 8;


	@Resource
	private MiddleActivityGoodsDao activityGoodsDao;
	@Resource
	private MessageClient messageClient;
	@Resource
	private GoodsInfoDao goodsInfoDao;

	@Override
	public void createActivity(ActivityInfo activityInfo, List<MiddleActivityGoods> magList) {
		activityInfo.setPicture(QINIU_URL + activityInfo.getPicture());
		mapper.insert(activityInfo);
		for (MiddleActivityGoods activityGoods: magList) {
			activityGoods.setActivityInfoId(activityInfo.getId());
			activityGoodsDao.insert(activityGoods);
		}
	}

	@Override
	public void review(ActivityInfo activityInfo) {
		Map<String, Object> map = new HashMap<>();
		ActivityInfo entity = mapper.findById(activityInfo.getId());
		if (activityInfo.getStatus() != null) {
			entity.setStatus(activityInfo.getStatus());
		}
		if (activityInfo.getRefusal() != null) {
			entity.setRefusal(activityInfo.getRefusal());
		}
		mapper.update(entity);
		// 发送通知
		if (entity.getStatus().equals("AUDIT_PASS")) {
			// 活动审核通过
//			messageClient.sendNotification(map);
		}else {
			// 活动审核拒绝
//			messageClient.sendNotification(map);
		}
	}

	@Override
	public ListResult<ActivityinfoOutput> findByType(Map<String, Object> params) {
		Map<String, Object> htMap = new HashMap<>();
		List<ActivityInfo> acList = mapper.findList(params);
		List<GoodsInfoVo> giList = null;
		List<ActivityinfoOutput> list = new ArrayList<>();
		ActivityinfoOutput activityinfoOutPut = null;
		for (ActivityInfo activityInfo: acList) {
			htMap.put("activityId", activityInfo.getId());
			htMap.put("offset", GOODSOFFSET);
			htMap.put("limit", GOODSLIMIT);
			giList = goodsInfoDao.findGoodsByActivityId(params);
			activityinfoOutPut = new ActivityinfoOutput();
			activityinfoOutPut.setActivityInfo(activityInfo);
			activityinfoOutPut.setGiList(giList);
			list.add(activityinfoOutPut);
		}
		return new ListResult(list);
	}

	@Override
	public ListResult<ActivityInfo> findCategoryOrBrand(Map<String, Object> params) {
		params.put("type", BRANDORCATEGORYTYPE);
		params.put("offset", BRANDORCATEGORYOFFSET);
		params.put("limit", BRANDORCATEGORYLIMIT);
		return new ListResult<ActivityInfo>(mapper.findList(params));
	}
}
