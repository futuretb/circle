package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.po.StoreInfo;
import com.circle.dope.goods.service.StoreInfoService;
import com.circle.dope.goods.utils.UserInfoUtil;
import com.circle.dope.user.po.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;
import static com.circle.dope.framework.base.PageResult.PAGE_NUM;
import static com.circle.dope.framework.base.PageResult.PAGE_SIZE;

/**
 * 店铺信息表 - Controller
 *
 *  Created by dicksoy on '2019-01-06 18:52:21'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/storeInfo")
public class StoreInfoController {

    @Resource
    private StoreInfoService storeInfoService;

    @Resource
    private UserInfoUtil userInfoUtil;

    @RequestMapping(value = "/inviteStore", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult inviteStore(@RequestBody StoreInfo storeInfo,
                                  @RequestParam(value = UserType.SELLER_TOKEN) String sellerToken) {
        Map<String, Object> result = new HashMap<>();
        UserInfo userInfo = userInfoUtil.findUserInfo(sellerToken);
        AssertUtil.notNull(storeInfo);
        storeInfoService.insert(storeInfo);
        result.put("userInfo", userInfo);
        result.put("storeInfo", storeInfo);
        return SUCCESS.result(result);
    }

    /**
     * 创建店铺
     * @param storeInfo
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult create(@RequestBody StoreInfo storeInfo) {
        AssertUtil.notNull(storeInfo);
        storeInfoService.insert(storeInfo);
        return SUCCESS;
    }

    /**
     * 编辑店铺
     * @param storeInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody StoreInfo storeInfo) {
        AssertUtil.notNull(storeInfo);
        storeInfoService.updateByCondition(storeInfo);
        return SUCCESS;
    }

    /**
     * 查找单个店铺
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByCondition(@RequestBody Long id) {
        AssertUtil.notNull(id);
        return SUCCESS.result(storeInfoService.findById(id));
    }
}
