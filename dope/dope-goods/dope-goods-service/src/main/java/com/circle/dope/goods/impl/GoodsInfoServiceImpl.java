package com.circle.dope.goods.impl;

import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.dao.*;
import com.circle.dope.goods.param.output.GoodsDetailOutput;
import com.circle.dope.goods.param.output.GoodsInfoOutput;
import com.circle.dope.goods.param.vo.ActivityInfoVo;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.*;
import com.circle.dope.goods.service.GoodsInfoService;
import com.circle.dope.goods.service.RedisService;
import com.circle.dope.goods.utils.StorePriceUtil;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.circle.dope.framework.constant.ConfFix.QINIU_URL;

/**
 * 商品信息表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:49:15'.
 *  Email dicksoy@163.com
 */
@Service
public class GoodsInfoServiceImpl extends BaseServiceImpl<GoodsInfoDao, GoodsInfo>
	implements GoodsInfoService {


	@Resource
	private MiddleGoodsCategoryDao middleGoodsCategoryDao;
	@Resource
	private GoodsPictureDao goodsPictureDao;
	@Resource
	private GoodsDetailDao goodsDetailDao;
	@Resource
	private CategoryInfoDao categoryInfoDao;
	@Resource
	private RedisService redisService;

	@Override
	public void createGoods(GoodsInfo goodsInfo, List<MiddleGoodsCategory> mgcList, List<GoodsDetail> gdList, List<GoodsPicture> picList) {
		CategoryInfo categoryInfo = null;
		/**
		 * 增加商品
		 */
		goodsInfo.setPicture(QINIU_URL + goodsInfo.getPicture());
		goodsInfo.setQrCode("");
		mapper.insertByCondition(goodsInfo);
		Long goodsId = goodsInfo.getId();
		/**
		 * 增加类目信息
		 */
		if (mgcList != null && mgcList.size() > 0) {
			categoryInfo = redisService.getCategoryInfo(mgcList.get(0).getCategoryId());
			for (MiddleGoodsCategory middleGoodsCategory: mgcList) {
				middleGoodsCategory.setGoodsId(goodsId);
				middleGoodsCategoryDao.insertByCondition(middleGoodsCategory);
			}
		}
		/**
		 * 增加尺码信息
		 */
		for (GoodsDetail goodsDetail: gdList) {
			if (goodsDetail.getAgentPrice() != null) {
				goodsDetail.setStorePrice(new BigDecimal(StorePriceUtil.convertStorePrice(categoryInfo, goodsDetail.getAgentPrice().doubleValue())));
			}
			goodsDetail.setGoodsId(goodsId);
			goodsDetailDao.insertByCondition(goodsDetail);
		}
		/**
		 * 增加图片
		 */
		if (picList != null && picList.size() > 0) {
			for (GoodsPicture picture: picList) {
				picture.setPictureUrl(QINIU_URL + picture.getPictureUrl());
				picture.setGoodsId(goodsId);
				goodsPictureDao.insertByCondition(picture);
			}
		}
	}


	@Override
	public void updateGoods(GoodsInfo goodsInfo, List<MiddleGoodsCategory> mgcList, List<GoodsPicture> picList) {
		/**
		 * 编辑商品信息
		 */
		if (!goodsInfo.getPicture().isEmpty()) {
			goodsInfo.setPicture(QINIU_URL + goodsInfo.getPicture());
		}
		mapper.update(goodsInfo);
		/**
		 * 编辑类目信息
		 */
		for (MiddleGoodsCategory middleGoodsCategory: mgcList) {
			if (middleGoodsCategory.getId() != null) {
				middleGoodsCategoryDao.update(middleGoodsCategory);
			}else {
				middleGoodsCategoryDao.insert(middleGoodsCategory);
			}
		}
		for (GoodsPicture goodsPicture: picList) {
			if (goodsPicture.getId() != null) {
				goodsPictureDao.update(goodsPicture);
			}else {
				goodsPictureDao.insert(goodsPicture);
			}
		}
	}

	@Override
	public ListResult<GoodsInfoVo> findByActivityId(Map<String, Object> params) {
		params.put("level", getUserLevel(params.get("wxUserId").toString()));
		List<GoodsInfoVo> list = mapper.findGoodsByActivityId(params);
		return new ListResult<GoodsInfoVo>(list);
	}

	@Resource
	private ActivityInfoDao activityInfoDao;

	@Override
	public GoodsInfoOutput goodsOne(Map<String, Object> params) {
		GoodsInfo goodsInfo = mapper.findById(Long.valueOf(params.get("id").toString()));
		GoodsInfoVo goodsInfoVo = new GoodsInfoVo();
		BeanUtils.copyProperties(goodsInfo, goodsInfoVo);
		GoodsInfoOutput goodsInfoOutPut = new GoodsInfoOutput();
		goodsInfoOutPut.setGoodsInfo(goodsInfoVo);
		Map<String, Object> gdMap = new HashMap();
		gdMap.put("goodsId", params.get("id"));
		gdMap.put("isDel", 1);
		List<GoodsDetailOutput> gdList = convertToList(goodsDetailDao.findList(gdMap));
		goodsInfoOutPut.setGdList(gdList);
		goodsInfoOutPut.setPicList(goodsPictureDao.findList(gdMap));
		List<ActivityInfoVo> acList = activityInfoDao.getByGoodsId(goodsInfo.getId());
		ActivityInfoVo activityInfoVo = null;
		if (acList != null && !acList.isEmpty()) {
			activityInfoVo = acList.get(0);
		}
		goodsInfoOutPut.setActivityInfoVo(activityInfoVo);
		goodsInfoOutPut.setStoreInfo(redisService.getStoreInfo(goodsInfo.getStoreId()));
		goodsInfoOutPut.setBrandInfo(redisService.getBrandInfo(goodsInfo.getBrandId()));
		goodsInfoOutPut.setCiList(categoryInfoDao.findByGoodsId(goodsInfo.getId()));
		/**
		 * 设置商品goodsPrice
		 */
		convertGoodsPrice(goodsInfoOutPut);
		return goodsInfoOutPut;
	}

	/**
	 * 设置商品 goodsPrice
	 * @param goodsInfoOutput
	 * @return
	 */
	public void convertGoodsPrice(GoodsInfoOutput goodsInfoOutput){
		GoodsInfoVo goodsInfoVo = goodsInfoOutput.getGoodsInfo();
		ActivityInfoVo activityInfoVo = goodsInfoOutput.getActivityInfoVo();
		WxUserInfo wxUserInfo = new WxUserInfo(); //注意此处从redis中 获取用户
		wxUserInfo.setLevel(1);
		if (wxUserInfo.getLevel() == 1) {
			if (activityInfoVo != null) {
				if (activityInfoVo.getStatus().equals("AUDIT_PASS")) {
					// 限时折扣
					for (GoodsDetail goodsDetail: goodsInfoOutput.getGdList()) {
						GoodsDetailOutput goodsDetailOutput = new GoodsDetailOutput();
						BeanUtils.copyProperties(goodsDetail, goodsDetailOutput);
						goodsDetailOutput.setGoodsPrice(goodsInfoOutput.getActivityInfoVo().getActivityPrice());
					}
					goodsInfoVo.setGoodsPrice(activityInfoVo.getActivityPrice());
				}else {
					for (GoodsDetail goodsDetail: goodsInfoOutput.getGdList()) {
						GoodsDetailOutput goodsDetailOutput = new GoodsDetailOutput();
						BeanUtils.copyProperties(goodsDetail, goodsDetailOutput);
						goodsDetailOutput.setGoodsPrice(goodsDetail.getAgentPrice());
					}
					goodsInfoVo.setGoodsPrice(null);// 商品明细最低代理价格
				}
			}
		}else {
			for (GoodsDetail goodsDetail: goodsInfoOutput.getGdList()) {
				GoodsDetailOutput goodsDetailOutput = new GoodsDetailOutput();
				BeanUtils.copyProperties(goodsDetail, goodsDetailOutput);
				goodsDetailOutput.setGoodsPrice(goodsDetail.getStorePrice());
			}
			goodsInfoVo.setGoodsPrice(null);// 商品明细最低店铺价格
		}
	}

	@Override
	public ListResult<GoodsInfoVo> findCircle(Map<String, Object> params) {
		params.put("level", getUserLevel(params.get("wxUserId").toString()));
		List<GoodsInfoVo> giList = null;
		if (params.get("maxPrice") != null || params.get("minPrice") != null) {
			// 价格筛选
			giList = mapper.findCircleByPrice(params);
		}else {
			giList = mapper.findCircle(params);
		}
		return new ListResult<GoodsInfoVo>(giList);
	}

	public List<GoodsDetailOutput> convertToList(List<GoodsDetail> gdList) {
		List<GoodsDetailOutput> gdoList = new ArrayList<>();
		GoodsDetailOutput goodsDetailOutput = null;
		for (GoodsDetail goodsDetail: gdList) {
			goodsDetailOutput = new GoodsDetailOutput();
			BeanUtils.copyProperties(goodsDetail, goodsDetailOutput);
			gdoList.add(goodsDetailOutput);
		}
		return gdoList;
	}

	@Override
	public ListResult<GoodsInfoVo> findGoods(Map<String, Object> params) {
		return  new ListResult<GoodsInfoVo>(mapper.findGoods(params));
	}

	@Override
	public GoodsInfoVo statistics(Long storeId) {
		return  mapper.statistics(storeId);
	}

	public Integer getUserLevel(String userId){
		Integer level = 0;
		WxUserInfo wxUserInfo = redisService.getWxUserInfo(userId);
		if (wxUserInfo != null) {
			level = wxUserInfo.getLevel();
		}
		return level;
	}

}
