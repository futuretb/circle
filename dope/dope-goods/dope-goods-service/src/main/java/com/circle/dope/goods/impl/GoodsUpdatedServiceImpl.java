package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.GoodsUpdatedDao;
import com.circle.dope.goods.service.GoodsUpdatedService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.GoodsUpdated;

/**
 * 商品编辑记录 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:49:20'.
 *  Email dicksoy@163.com
 */
@Service
public class GoodsUpdatedServiceImpl extends BaseServiceImpl<GoodsUpdatedDao, GoodsUpdated>
	implements GoodsUpdatedService {

}
