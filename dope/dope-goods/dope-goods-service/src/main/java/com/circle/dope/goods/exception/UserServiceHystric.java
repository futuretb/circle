package com.circle.dope.goods.exception;

import com.circle.dope.framework.base.BaseHystric;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.goods.client.UserServiceClient;
import org.springframework.stereotype.Component;

@Component
public class UserServiceHystric implements UserServiceClient {

    private static final BaseHystric HYSTRIC = BaseHystric.newInstance(UserServiceHystric.class);

    @Override
    public BaseResult findWxUserFriendOne(String userCode, String friendUserCode) {
        return HYSTRIC.defaultMessage();
    }
}
