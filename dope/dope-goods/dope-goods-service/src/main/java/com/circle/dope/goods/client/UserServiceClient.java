package com.circle.dope.goods.client;

import com.circle.dope.framework.base.BaseHystric;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.goods.exception.UserServiceHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "dope-user-service", path = "/user", fallback = UserServiceHystric.class)
public interface UserServiceClient {

    @RequestMapping(value = "/middleUserFriend/findWxUserFriendOne", method = RequestMethod.POST)
    BaseResult findWxUserFriendOne(@RequestParam("userCode") String userCode,
                                   @RequestParam("storeUserId") String friendUserCode);
}
