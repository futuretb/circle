package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.MiddleActivityGoods;

/**
 * 活动商品关系表 - Dao
 *
 *  Created by dicksoy on '2019-01-18 15:09:46'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface MiddleActivityGoodsDao extends BaseDao<MiddleActivityGoods> {

}
