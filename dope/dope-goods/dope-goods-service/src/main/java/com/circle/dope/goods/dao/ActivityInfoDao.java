package com.circle.dope.goods.dao;

import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.param.vo.ActivityInfoVo;
import com.circle.dope.goods.po.ActivityInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 活动发布 - Dao
 *
 *  Created by dicksoy on '2019-01-18 15:09:34'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ActivityInfoDao extends BaseDao<ActivityInfo> {
    List<ActivityInfoVo> getByGoodsId(Long goodsId);
}
