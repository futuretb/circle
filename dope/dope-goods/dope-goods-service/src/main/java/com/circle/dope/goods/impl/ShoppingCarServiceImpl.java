package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.ShoppingCarDao;
import com.circle.dope.goods.service.ShoppingCarService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.ShoppingCar;

/**
 * 购物车 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:52:16'.
 *  Email dicksoy@163.com
 */
@Service
public class ShoppingCarServiceImpl extends BaseServiceImpl<ShoppingCarDao, ShoppingCar>
	implements ShoppingCarService {

}
