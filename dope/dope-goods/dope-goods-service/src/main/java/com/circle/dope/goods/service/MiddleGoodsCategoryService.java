package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.MiddleGoodsCategory;

/**
 * 商品类目关系表 - Service
 *
 *  Created by dicksoy on '2019-01-09 14:19:07'.
 *  Email dicksoy@163.com
 */
public interface MiddleGoodsCategoryService extends BaseService<MiddleGoodsCategory> {

}
