package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.BrandInfoApi;
import com.circle.dope.goods.po.BrandInfo;
import com.circle.dope.goods.service.BrandInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;
import static com.circle.dope.framework.base.PageResult.PAGE_NUM;
import static com.circle.dope.framework.base.PageResult.PAGE_SIZE;

/**
 * 品牌 - Controller
 *  Created by dicksoy on '2019-01-06 18:48:52'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/brandInfo")
public class BrandInfoController {

    @Resource
    private BrandInfoService brandInfoService;

    /**
     * 查找店铺可用品牌
     * @param params
     * @return
     */
    @RequestMapping(value = "/findAllByStoreId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findAllByStoreId(@RequestBody HashMap<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<BrandInfo> biList = brandInfoService.findAllByStoreId(params);
        return SUCCESS.result(biList);
    }

    /**
     * 条件查询
     * @param params
     * @return
     */
    @RequestMapping(value = "/findByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByCondition(@RequestBody HashMap<String, Object> params) {
        AssertUtil.notNull(params);
        if (params.containsKey(PAGE_NUM) && params.containsKey(PAGE_SIZE)) {
            return SUCCESS.result(brandInfoService.findListByPage(params));
        } else {
            return SUCCESS.result(brandInfoService.findList(params));
        }
    }

    /**
     * 创建品牌
     * @param brandInfo
     * @return
     */
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insert(@RequestBody BrandInfo brandInfo) {
        AssertUtil.notNull(brandInfo);
        return SUCCESS.result(brandInfoService.insert(brandInfo));
    }

    /**
     * 修改品牌
     * @param brandInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody BrandInfo brandInfo) {
        AssertUtil.notNull(brandInfo);
        return SUCCESS.result(brandInfoService.updateByCondition(brandInfo));
    }

    /**
     * 获取单个品牌
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findById(@RequestParam Long id) {
        return SUCCESS.result(brandInfoService.findById(id));
    }

}
