package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.CategoryInfoExtend;

/**
 * 类目关联表 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:49:04'.
 *  Email dicksoy@163.com
 */
public interface CategoryInfoExtendService extends BaseService<CategoryInfoExtend> {

}
