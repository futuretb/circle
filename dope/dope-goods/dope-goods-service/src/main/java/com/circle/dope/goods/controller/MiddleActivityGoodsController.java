package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.MiddleActivityGoodsApi;
import com.circle.dope.goods.po.MiddleActivityGoods;
import com.circle.dope.goods.po.StoreInfo;
import com.circle.dope.goods.service.MiddleActivityGoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import java.util.List;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 活动商品关系表 - Controller
 *
 *  Created by dicksoy on '2019-01-18 15:09:46'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/activityGoods")
public class MiddleActivityGoodsController {

    @Resource
    private MiddleActivityGoodsService middleActivityGoodsService;

    /**
     * 活动中增加商品
     * @param macList
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insert(@RequestBody List<MiddleActivityGoods> macList) {
        AssertUtil.notNull(macList);
        middleActivityGoodsService.update(macList);
        return SUCCESS;
    }

    /**
     * 删除活动中的商品
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult deleteById(@RequestBody Long id) {
        AssertUtil.notNull(id);
        middleActivityGoodsService.deleteById(id);
        return SUCCESS;
    }

}
