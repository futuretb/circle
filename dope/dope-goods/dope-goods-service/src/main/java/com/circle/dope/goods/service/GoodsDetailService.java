package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.param.vo.GoodsDetailVo;
import com.circle.dope.goods.po.GoodsDetail;

import java.util.List;

/**
 * 商品明细表 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:49:10'.
 *  Email dicksoy@163.com
 */
public interface GoodsDetailService extends BaseService<GoodsDetail> {

    void replenishment(List<GoodsDetailVo> gdList, String userCode);

    void insert(List<GoodsDetail> gdList);
}
