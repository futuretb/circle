package com.circle.dope.goods.param.intput;

import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.po.GoodsInfo;
import com.circle.dope.goods.po.GoodsPicture;
import com.circle.dope.goods.po.MiddleGoodsCategory;

import java.util.List;

/**
 * @Classname GoodsInfoIntput
 * @Description TODO
 * @Date 2019/1/21 15:24
 * @Created by win7
 */
public class GoodsInfoIntput {

    private GoodsInfo goodsInfo;
    private GoodsDetail goodsDetail;
    private List<GoodsDetail> gdList;
    private List<MiddleGoodsCategory> mgcList;
    private List<GoodsPicture> picList;

    public GoodsInfo getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(GoodsInfo goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public GoodsDetail getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(GoodsDetail goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public List<GoodsDetail> getGdList() {
        return gdList;
    }

    public void setGdList(List<GoodsDetail> gdList) {
        this.gdList = gdList;
    }

    public List<MiddleGoodsCategory> getMgcList() {
        return mgcList;
    }

    public void setMgcList(List<MiddleGoodsCategory> mgcList) {
        this.mgcList = mgcList;
    }

    public List<GoodsPicture> getPicList() {
        return picList;
    }

    public void setPicList(List<GoodsPicture> picList) {
        this.picList = picList;
    }
}
