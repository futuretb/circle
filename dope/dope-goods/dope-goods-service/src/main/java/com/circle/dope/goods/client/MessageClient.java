package com.circle.dope.goods.client;

import com.circle.dope.framework.base.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * 只继承通用
 */
@FeignClient(name = "dope-message", path = "/goods")
public interface MessageClient {

    @RequestMapping(value = "/jpush/sendNotification")
    BaseResult sendNotification(Map<String, Object> params);

}
