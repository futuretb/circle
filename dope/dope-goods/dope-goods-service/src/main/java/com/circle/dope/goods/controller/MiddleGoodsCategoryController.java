package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.MiddleGoodsCategoryApi;
import com.circle.dope.goods.po.MiddleGoodsCategory;
import com.circle.dope.goods.service.MiddleGoodsCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 商品类目关系表 - Controller
 *
 *  Created by dicksoy on '2019-01-09 14:19:07'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/middleGoodsCategory")
public class MiddleGoodsCategoryController {

    @Resource
    private MiddleGoodsCategoryService middleGoodsCategoryService;

    /**
     * 商品添加类目
     * @param middleGoodsCategory
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult add(@RequestBody MiddleGoodsCategory middleGoodsCategory) {
        AssertUtil.notNull(middleGoodsCategory);
        middleGoodsCategoryService.insert(middleGoodsCategory);
        return SUCCESS;
    }


    /**
     * 商品删除类目
     * @param id
     * @return
     */
    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult deleteById(@RequestBody Long id) {
        AssertUtil.notNull(id);
        middleGoodsCategoryService.deleteById(id);
        return SUCCESS;
    }
}
