package com.circle.dope.goods.dao;

import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.GoodsInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 商品信息表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:49:15'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface GoodsInfoDao extends BaseDao<GoodsInfo> {

    List<GoodsInfoVo> findGoodsByActivityId(Map<String, Object> params);
    List<GoodsInfoVo> findCircle(Map<String, Object> params);
    List<GoodsInfoVo> findCircleByPrice(Map<String, Object> params);
    List<GoodsInfoVo> findGoods(Map<String, Object> params);
    GoodsInfoVo statistics(Long storeId);
}
