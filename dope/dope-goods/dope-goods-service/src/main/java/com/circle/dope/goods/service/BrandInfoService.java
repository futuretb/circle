package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.po.BrandInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 品牌 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:48:52'.
 *  Email dicksoy@163.com
 */
public interface BrandInfoService extends BaseService<BrandInfo> {

    ListResult<BrandInfo> findAllByStoreId(Map<String, Object> params);

}