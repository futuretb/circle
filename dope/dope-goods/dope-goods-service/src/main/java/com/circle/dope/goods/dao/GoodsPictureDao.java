package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.GoodsPicture;

/**
 * 图片 - Dao
 *
 *  Created by dicksoy on '2019-01-10 18:32:47'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface GoodsPictureDao extends BaseDao<GoodsPicture> {

}
