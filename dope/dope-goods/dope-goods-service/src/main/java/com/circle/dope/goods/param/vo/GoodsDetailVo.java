package com.circle.dope.goods.param.vo;

import com.circle.dope.goods.po.GoodsDetail;

import java.math.BigDecimal;

/**
 * @Classname GoodsDetailParam
 * @Description TODO
 * @Date 2019/1/10 17:23
 * @Created by win7
 */
public class GoodsDetailVo extends GoodsDetail{

    private BigDecimal goodsCost;
    private String userId;

    public BigDecimal getGoodsCost() {
        return goodsCost;
    }

    public void setGoodsCost(BigDecimal goodsCost) {
        this.goodsCost = goodsCost;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
