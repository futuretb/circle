package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.ShoppingCar;

/**
 * 购物车 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:52:16'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface ShoppingCarDao extends BaseDao<ShoppingCar> {

}
