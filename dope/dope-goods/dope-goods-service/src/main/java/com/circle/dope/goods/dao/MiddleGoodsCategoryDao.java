package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.MiddleGoodsCategory;

/**
 * 商品类目关系表 - Dao
 *
 *  Created by dicksoy on '2019-01-09 14:19:07'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface MiddleGoodsCategoryDao extends BaseDao<MiddleGoodsCategory> {

}
