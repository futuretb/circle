package com.circle.dope.goods.param.vo;

import com.circle.dope.goods.po.GoodsInfo;

import java.math.BigDecimal;

/**
 * @Classname GoodsInfoParam
 * @Description TODO
 * @Date 2019/1/14 16:38
 * @Created by win7
 */
public class GoodsInfoVo extends GoodsInfo {

    private String color;
    private String size;
    private String brandName;
    private String categoryName;
    private BigDecimal goodsPrice;
    private Long inventory;
    private Long saleIn;
    private BigDecimal agentPrice;
    private BigDecimal storePrice;
    private BigDecimal totalCost;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Long getInventory() {
        return inventory;
    }

    public void setInventory(Long inventory) {
        this.inventory = inventory;
    }

    public Long getSaleIn() {
        return saleIn;
    }

    public void setSaleIn(Long saleIn) {
        this.saleIn = saleIn;
    }

    public BigDecimal getAgentPrice() {
        return agentPrice;
    }

    public void setAgentPrice(BigDecimal agentPrice) {
        this.agentPrice = agentPrice;
    }

    public BigDecimal getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(BigDecimal storePrice) {
        this.storePrice = storePrice;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
}
