package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.StoreInfo;

/**
 * 店铺信息表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:52:21'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface StoreInfoDao extends BaseDao<StoreInfo> {

}
