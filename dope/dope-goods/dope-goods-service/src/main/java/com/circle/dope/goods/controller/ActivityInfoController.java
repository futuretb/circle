package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.ActivityInfoApi;
import com.circle.dope.goods.param.intput.ActivityInfoIntput;
import com.circle.dope.goods.param.output.ActivityinfoOutput;
import com.circle.dope.goods.po.ActivityInfo;
import com.circle.dope.goods.service.ActivityInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 活动发布 - Controller
 *
 *  Created by dicksoy on '2019-01-18 15:09:34'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/activityInfo")
public class ActivityInfoController {

    @Resource
    private ActivityInfoService activityInfoService;

    /**
     * 创建活动
     * @param activityInfoIntput
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult create(@RequestBody ActivityInfoIntput activityInfoIntput) {
        AssertUtil.notNull(activityInfoIntput);
        activityInfoService.createActivity(activityInfoIntput.getActivityInfo(), activityInfoIntput.getMagList());
        return SUCCESS;
    }

    /**
     * 活动编辑
     * @param activityInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody ActivityInfo activityInfo) {
        AssertUtil.notNull(activityInfo);
        activityInfoService.updateByCondition(activityInfo);
        return SUCCESS;
    }

    /**
     * 审核
     * @param activityInfo
     * @return
     */
    @RequestMapping(value = "/review", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult review(@RequestBody ActivityInfo activityInfo) {
        AssertUtil.notNull(activityInfo);
        activityInfoService.review(activityInfo);
        return SUCCESS;
    }

    /**
     * 获取活动
     * @param params type 0 热门推荐 1 限时折扣
     * @return
     */
    @RequestMapping(value = "/findByType", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByType(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<ActivityinfoOutput> hsList =  activityInfoService.findByType(params);
        return SUCCESS.result(hsList);
    }

    /**
     * 获取 品牌OR类目 活动
     * @param map
     * @return
     */
    @RequestMapping(value = "/findCategoryOrBrand", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findCategoryOrBrand(@RequestBody Map<String, Object> map) {
        ListResult<ActivityInfo> hsList =  activityInfoService.findCategoryOrBrand(map);
        return SUCCESS.result(hsList);
    }

}
