package com.circle.dope.goods.param.intput;

import com.circle.dope.goods.po.ActivityInfo;
import com.circle.dope.goods.po.MiddleActivityGoods;

import java.util.List;

/**
 * @Classname ActivityInfoIntput
 * @Description TODO
 * @Date 2019/1/21 14:07
 * @Created by win7
 */
public class ActivityInfoIntput extends ActivityInfo {

    private ActivityInfo activityInfo;
    private List<MiddleActivityGoods> magList;

    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    public void setActivityInfo(ActivityInfo activityInfo) {
        this.activityInfo = activityInfo;
    }

    public List<MiddleActivityGoods> getMagList() {
        return magList;
    }

    public void setMagList(List<MiddleActivityGoods> magList) {
        this.magList = magList;
    }
}
