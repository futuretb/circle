package com.circle.dope.goods.utils;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class UserInfoUtil {

    @Resource
    private RedisManager redisManager;


    public UserInfo findUserInfo(String userId) {
        return (UserInfo) baseFind(userId);
    }

    public WxUserInfo findWxUserInfo(String wxUserCode) {
        return (WxUserInfo) baseFind(wxUserCode);
    }

    private Object baseFind(String userId) {
        if (userId.startsWith("WX_")) {
            String value = redisManager.get(PrefixEnum.USER_TYPE.WX_USER.getPrefix() + userId);
            return JSONObject.parseObject(value, WxUserInfo.class);
        } else {
            String value = redisManager.get(PrefixEnum.USER_TYPE.USER.getPrefix() + userId);
            return JSONObject.parseObject(value, UserInfo.class);
        }
    }
}
