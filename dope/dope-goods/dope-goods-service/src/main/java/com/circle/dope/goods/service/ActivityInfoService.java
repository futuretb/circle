package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.param.output.ActivityinfoOutput;
import com.circle.dope.goods.po.ActivityInfo;
import com.circle.dope.goods.po.MiddleActivityGoods;

import java.util.List;
import java.util.Map;

/**
 * 活动发布 - Service
 *
 *  Created by dicksoy on '2019-01-18 15:09:34'.
 *  Email dicksoy@163.com
 */
public interface ActivityInfoService extends BaseService<ActivityInfo> {

    /**
     * 创建活动
     * @param activityInfo
     * @param magList
     */
    void createActivity(ActivityInfo activityInfo, List<MiddleActivityGoods> magList);

    /**
     * 活动审核
     * @param activityInfo
     */
    void review(ActivityInfo activityInfo);

    /**
     * 查找活动
     * @param params
     * @return
     */
    ListResult<ActivityinfoOutput> findByType(Map<String, Object> params);

    /**
     * 查找类目品牌活动
     * @param params
     * @return
     */
    ListResult<ActivityInfo> findCategoryOrBrand(Map<String, Object> params);
}
