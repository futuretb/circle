package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.GoodsPictureDao;
import com.circle.dope.goods.service.GoodsPictureService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.GoodsPicture;

/**
 * 图片 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-10 18:32:47'.
 *  Email dicksoy@163.com
 */
@Service
public class GoodsPictureServiceImpl extends BaseServiceImpl<GoodsPictureDao, GoodsPicture>
	implements GoodsPictureService {

}
