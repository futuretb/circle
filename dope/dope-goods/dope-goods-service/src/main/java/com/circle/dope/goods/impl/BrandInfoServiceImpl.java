package com.circle.dope.goods.impl;

import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.dao.BrandInfoDao;
import com.circle.dope.goods.service.BrandInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.BrandInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * 品牌 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:48:52'.
 *  Email dicksoy@163.com
 */
@Service
public class BrandInfoServiceImpl extends BaseServiceImpl<BrandInfoDao, BrandInfo>
	implements BrandInfoService {

	@Override
	public ListResult<BrandInfo> findAllByStoreId(Map<String, Object> params) {
		return new ListResult(mapper.findAllByStoreId(params));
	}

}
