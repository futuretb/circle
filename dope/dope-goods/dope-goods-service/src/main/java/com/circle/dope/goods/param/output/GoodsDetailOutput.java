package com.circle.dope.goods.param.output;

import com.circle.dope.goods.po.GoodsDetail;

import java.math.BigDecimal;

/**
 * @Classname GoodsDetailOutput
 * @Description TODO
 * @Date 2019/1/15 13:47
 * @Created by win7
 */
public class GoodsDetailOutput extends GoodsDetail {

    private BigDecimal goodsPrice;

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
}
