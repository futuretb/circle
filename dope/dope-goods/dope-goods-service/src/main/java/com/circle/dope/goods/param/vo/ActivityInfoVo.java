package com.circle.dope.goods.param.vo;

import com.circle.dope.goods.po.ActivityInfo;

import java.math.BigDecimal;

/**
 * @Classname ActivityInfoVo
 * @Description TODO
 * @Date 2019/1/21 15:00
 * @Created by win7
 */
public class ActivityInfoVo extends ActivityInfo {

    private BigDecimal activityPrice;

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }
}
