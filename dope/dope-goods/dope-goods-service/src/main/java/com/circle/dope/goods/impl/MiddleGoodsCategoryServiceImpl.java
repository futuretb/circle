package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.MiddleGoodsCategoryDao;
import com.circle.dope.goods.service.MiddleGoodsCategoryService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.MiddleGoodsCategory;

/**
 * 商品类目关系表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-09 14:19:07'.
 *  Email dicksoy@163.com
 */
@Service
public class MiddleGoodsCategoryServiceImpl extends BaseServiceImpl<MiddleGoodsCategoryDao, MiddleGoodsCategory>
	implements MiddleGoodsCategoryService {

}
