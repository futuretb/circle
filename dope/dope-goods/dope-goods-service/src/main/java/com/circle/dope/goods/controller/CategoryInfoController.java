package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.CategoryInfoApi;
import com.circle.dope.goods.po.BrandInfo;
import com.circle.dope.goods.po.CategoryInfo;
import com.circle.dope.goods.service.CategoryInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;
import static com.circle.dope.framework.base.PageResult.PAGE_NUM;
import static com.circle.dope.framework.base.PageResult.PAGE_SIZE;

/**
 * 类目 - Controller
 *
 *  Created by dicksoy on '2019-01-06 18:48:59'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/categoryInfo")
public class CategoryInfoController {

    @Resource
    private CategoryInfoService categoryInfoService;

    /**
     * 查找店铺可用类目
     * @param params
     * @return
     */
    @RequestMapping(value = "/findAllByStoreId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findAllByStoreId(@RequestBody HashMap<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<CategoryInfo> biList = categoryInfoService.findAllByStoreId(params);
        return SUCCESS.result(biList);
    }

    /**
     * 条件查询
     * @param params
     * @return
     */
    @RequestMapping(value = "/findByCondition", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByCondition(@RequestBody HashMap<String, Object> params) {
        AssertUtil.notNull(params);
        if (params.containsKey(PAGE_NUM) && params.containsKey(PAGE_SIZE)) {
            return SUCCESS.result(categoryInfoService.findListByPage(params));
        } else {
            return SUCCESS.result(categoryInfoService.findList(params));
        }
    }

    /**
     * 创建类目
     * @param categoryInfo
     * @return
     */
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insert(@RequestBody CategoryInfo categoryInfo) {
        AssertUtil.notNull(categoryInfo);
        return SUCCESS.result(categoryInfoService.insert(categoryInfo));
    }

    /**
     * 修改品牌
     * @param categoryInfo
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody CategoryInfo categoryInfo) {
        AssertUtil.notNull(categoryInfo);
        return SUCCESS.result(categoryInfoService.updateByCondition(categoryInfo));
    }

    /**
     * 获取单个类目
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findById(@RequestParam Long id) {
        return SUCCESS.result(categoryInfoService.findById(id));
    }

}
