package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.MiddleActivityGoods;

import java.util.List;

/**
 * 活动商品关系表 - Service
 *
 *  Created by dicksoy on '2019-01-18 15:09:46'.
 *  Email dicksoy@163.com
 */
public interface MiddleActivityGoodsService extends BaseService<MiddleActivityGoods> {

    void update(List<MiddleActivityGoods> macList);
}
