package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.MiddleActivityGoodsDao;
import com.circle.dope.goods.service.MiddleActivityGoodsService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.MiddleActivityGoods;

import java.util.List;

/**
 * 活动商品关系表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-18 15:09:46'.
 *  Email dicksoy@163.com
 */
@Service
public class MiddleActivityGoodsServiceImpl extends BaseServiceImpl<MiddleActivityGoodsDao, MiddleActivityGoods>
	implements MiddleActivityGoodsService {

	@Override
	public void update(List<MiddleActivityGoods> macList) {
		for (MiddleActivityGoods activityGoods: macList) {
			if (activityGoods.getId() != null){
				mapper.insert(activityGoods);
			}else {
				mapper.updateByCondition(activityGoods);
			}
		}
	}
}
