package com.circle.dope.goods.impl;

import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.dao.CategoryInfoDao;
import com.circle.dope.goods.service.CategoryInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.CategoryInfo;

import java.util.HashMap;

/**
 * 类目 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:48:59'.
 *  Email dicksoy@163.com
 */
@Service
public class CategoryInfoServiceImpl extends BaseServiceImpl<CategoryInfoDao, CategoryInfo>
	implements CategoryInfoService {

	@Override
	public ListResult<CategoryInfo> findAllByStoreId(HashMap<String, Object> params) {
		return new ListResult<CategoryInfo>(mapper.findAllByStoreId(params));
	}
}
