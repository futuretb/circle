package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.GoodsPicture;

/**
 * 图片 - Service
 *
 *  Created by dicksoy on '2019-01-10 18:32:47'.
 *  Email dicksoy@163.com
 */
public interface GoodsPictureService extends BaseService<GoodsPicture> {

}
