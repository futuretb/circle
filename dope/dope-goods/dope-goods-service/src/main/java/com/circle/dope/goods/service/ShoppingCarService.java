package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.ShoppingCar;

/**
 * 购物车 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:52:16'.
 *  Email dicksoy@163.com
 */
public interface ShoppingCarService extends BaseService<ShoppingCar> {

}
