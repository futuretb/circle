package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.StoreInfo;

/**
 * 店铺信息表 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:52:21'.
 *  Email dicksoy@163.com
 */
public interface StoreInfoService extends BaseService<StoreInfo> {

}
