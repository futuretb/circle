package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.StoreInfoDao;
import com.circle.dope.goods.service.StoreInfoService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.StoreInfo;

/**
 * 店铺信息表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:52:21'.
 *  Email dicksoy@163.com
 */
@Service
public class StoreInfoServiceImpl extends BaseServiceImpl<StoreInfoDao, StoreInfo>
	implements StoreInfoService {

}
