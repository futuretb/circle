package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.BrandInfo;

import java.util.List;
import java.util.Map;

/**
 * 品牌 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:48:52'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface BrandInfoDao extends BaseDao<BrandInfo> {

    List<BrandInfo> findAllByStoreId(Map<String, Object> params);
}
