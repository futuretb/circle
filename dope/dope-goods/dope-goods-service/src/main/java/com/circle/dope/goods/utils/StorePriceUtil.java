package com.circle.dope.goods.utils;

import com.circle.dope.goods.po.CategoryInfo;

/**
 * Created by Wenqiang on 2019/1/9.
 */
public class StorePriceUtil {



    public static Double convertStorePrice(CategoryInfo categoryInfo, Double agentPrice){
        Double storePrice = null;
        if (categoryInfo == null ) {
            storePrice = agentPrice * 1.1;
            return BigDecimalUtils.round(storePrice, 2);
        }else {
            String ascRates = categoryInfo.getAscRate();
            if (ascRates != null) {
                String[] ascRate = ascRates.split(",");
                for (String priceRates : ascRate){
                    Double price = null;
                    Double rate = null;
                    String[] priceRate = priceRates.split("-");
                    if (priceRate.length > 0) {
                        price = Double.parseDouble(priceRate[0]);
                    }
                    if (priceRate.length > 1) {
                        rate = Double.parseDouble(priceRate[1]);
                    }
                    if (agentPrice < price) {
                        storePrice = agentPrice * (1 + rate);
                        return storePrice;
                    }
                    if (price == 0) {
                        storePrice = agentPrice * (1 + rate);
                        return BigDecimalUtils.round(storePrice, 2);
                    }
                }
            }else {
                if (agentPrice != null) {
                    storePrice = agentPrice * 1.1;
                    return BigDecimalUtils.round(storePrice, 2);
                }
            }
        }
        return 0.01D;
    }
}
