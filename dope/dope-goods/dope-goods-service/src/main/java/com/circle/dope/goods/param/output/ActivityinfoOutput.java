package com.circle.dope.goods.param.output;

import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.ActivityInfo;
import com.circle.dope.goods.po.MiddleActivityGoods;

import java.util.List;

/**
 * @Classname ActivityinfoInfoOutPut
 * @Description TODO
 * @Date 2019/1/21 14:14
 * @Created by win7
 */
public class ActivityinfoOutput {

    private ActivityInfo activityInfo;
    private List<MiddleActivityGoods> magList;
    private List<GoodsInfoVo> giList;

    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    public void setActivityInfo(ActivityInfo activityInfo) {
        this.activityInfo = activityInfo;
    }

    public List<MiddleActivityGoods> getMagList() {
        return magList;
    }

    public void setMagList(List<MiddleActivityGoods> magList) {
        this.magList = magList;
    }

    public List<GoodsInfoVo> getGiList() {
        return giList;
    }

    public void setGiList(List<GoodsInfoVo> giList) {
        this.giList = giList;
    }
}
