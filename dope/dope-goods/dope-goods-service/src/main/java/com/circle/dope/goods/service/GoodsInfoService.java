package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.param.output.GoodsInfoOutput;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.po.GoodsInfo;
import com.circle.dope.goods.po.GoodsPicture;
import com.circle.dope.goods.po.MiddleGoodsCategory;

import java.util.List;
import java.util.Map;

/**
 * 商品信息表 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:49:15'.
 *  Email dicksoy@163.com
 */
public interface GoodsInfoService extends BaseService<GoodsInfo> {

    void createGoods(GoodsInfo goodsInfo, List<MiddleGoodsCategory> mgcList, List<GoodsDetail> gdList, List<GoodsPicture> picList);

    ListResult<GoodsInfoVo> findByActivityId(Map<String, Object> params);

    GoodsInfoOutput goodsOne(Map<String, Object> params);

    ListResult<GoodsInfoVo> findCircle(Map<String, Object> params);

    ListResult<GoodsInfoVo> findGoods(Map<String, Object> params);

    GoodsInfoVo statistics(Long storeId);

    void updateGoods(GoodsInfo goodsInfo, List<MiddleGoodsCategory> mgcList, List<GoodsPicture> picList);
}
