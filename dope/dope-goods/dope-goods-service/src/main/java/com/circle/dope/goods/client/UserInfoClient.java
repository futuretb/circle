package com.circle.dope.goods.client;

import com.circle.dope.framework.base.BaseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * 只继承通用
 */
@FeignClient(name = "dope-user-service", path = "/user")
public interface UserInfoClient {

    @RequestMapping(value = "/wxUserInfo/findById")
    BaseResult findByCondition(Map<String, Object> map);

}
