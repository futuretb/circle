package com.circle.dope.goods.param.output;

import com.circle.dope.goods.param.vo.ActivityInfoVo;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.*;

import java.util.List;

/**
 * @Classname GoodsInfoOutPut
 * @Description TODO
 * @Date 2019/1/21 14:50
 * @Created by win7
 */
public class GoodsInfoOutput {

    private GoodsInfoVo goodsInfo;
    private StoreInfo storeInfo;
    private BrandInfo brandInfo;
    private ActivityInfoVo activityInfoVo;
    private List<GoodsDetailOutput> gdList;
    private List<GoodsPicture> picList;
    private List<MiddleGoodsCategory> mgcList;
    private List<CategoryInfo> ciList;

    public GoodsInfoVo getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(GoodsInfoVo goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public StoreInfo getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfo storeInfo) {
        this.storeInfo = storeInfo;
    }

    public BrandInfo getBrandInfo() {
        return brandInfo;
    }

    public void setBrandInfo(BrandInfo brandInfo) {
        this.brandInfo = brandInfo;
    }

    public ActivityInfoVo getActivityInfoVo() {
        return activityInfoVo;
    }

    public void setActivityInfoVo(ActivityInfoVo activityInfoVo) {
        this.activityInfoVo = activityInfoVo;
    }

    public List<GoodsDetailOutput> getGdList() {
        return gdList;
    }

    public void setGdList(List<GoodsDetailOutput> gdList) {
        this.gdList = gdList;
    }

    public List<GoodsPicture> getPicList() {
        return picList;
    }

    public void setPicList(List<GoodsPicture> picList) {
        this.picList = picList;
    }

    public List<MiddleGoodsCategory> getMgcList() {
        return mgcList;
    }

    public void setMgcList(List<MiddleGoodsCategory> mgcList) {
        this.mgcList = mgcList;
    }

    public List<CategoryInfo> getCiList() {
        return ciList;
    }

    public void setCiList(List<CategoryInfo> ciList) {
        this.ciList = ciList;
    }
}
