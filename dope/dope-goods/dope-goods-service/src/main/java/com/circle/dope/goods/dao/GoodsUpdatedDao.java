package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.GoodsUpdated;

/**
 * 商品编辑记录 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:49:20'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface GoodsUpdatedDao extends BaseDao<GoodsUpdated> {

}
