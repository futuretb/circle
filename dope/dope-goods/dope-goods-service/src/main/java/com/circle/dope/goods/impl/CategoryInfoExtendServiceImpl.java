package com.circle.dope.goods.impl;

import com.circle.dope.goods.dao.CategoryInfoExtendDao;
import com.circle.dope.goods.service.CategoryInfoExtendService;
import org.springframework.stereotype.Service;
import com.circle.dope.framework.base.BaseServiceImpl;
import com.circle.dope.goods.po.CategoryInfoExtend;

/**
 * 类目关联表 - ServiceImpl
 *
 *  Created by dicksoy on '2019-01-06 18:49:04'.
 *  Email dicksoy@163.com
 */
@Service
public class CategoryInfoExtendServiceImpl extends BaseServiceImpl<CategoryInfoExtendDao, CategoryInfoExtend>
	implements CategoryInfoExtendService {

}
