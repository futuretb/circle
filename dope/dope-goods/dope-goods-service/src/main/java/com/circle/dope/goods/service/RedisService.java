package com.circle.dope.goods.service;

import com.circle.dope.goods.po.BrandInfo;
import com.circle.dope.goods.po.CategoryInfo;
import com.circle.dope.goods.po.StoreInfo;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;

/**
 * @Classname RedisService
 * @Description TODO
 * @Date 2019/1/22 15:30
 * @Created by win7
 */
public interface RedisService {

    UserInfo getUserInfo(String userId);

    WxUserInfo getWxUserInfo(String wxUserId);

    CategoryInfo getCategoryInfo(Long categoryId);

    BrandInfo getBrandInfo(Long brandId);

    StoreInfo getStoreInfo(Long storeId);

}
