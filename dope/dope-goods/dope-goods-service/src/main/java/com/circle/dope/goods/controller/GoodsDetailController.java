package com.circle.dope.goods.controller;

import com.alibaba.fastjson.JSON;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.DopeTopics;
import com.circle.dope.framework.constant.UserType;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.GoodsDetailApi;
import com.circle.dope.goods.param.vo.GoodsDetailVo;
import com.circle.dope.goods.po.GoodsDetail;
import com.circle.dope.goods.service.GoodsDetailService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 商品明细表 - Controller
 *
 *  Created by dicksoy on '2019-01-06 18:49:10'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/goodsDetail")
public class GoodsDetailController {

    @Resource
    private GoodsDetailService goodsDetailService;

    @RequestMapping(value = "/findById", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findById(@RequestParam("id") Long id) {
        return SUCCESS.result(goodsDetailService.findById(id));
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult insert(@RequestBody List<GoodsDetail> gdList) {
        AssertUtil.notNull(gdList);
        goodsDetailService.insert(gdList);
        return SUCCESS;
    }

    /**
     * 补货
     * @param gdList
     * @return
     */
    @RequestMapping(value = "/replenishment", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult replenishment(@RequestParam String userCode,
                                    @RequestBody List<GoodsDetailVo> gdList) {
        AssertUtil.notNull(userCode);
        AssertUtil.notNull(gdList);
        goodsDetailService.replenishment(gdList, userCode);
        return SUCCESS;
    }

    /**
     * 创建订单
     * @param content
     */
    @KafkaListener(topics = DopeTopics.UPDATE_INVENTORY)
    public void updateInventory(String content) {
        List<GoodsDetail> list = JSON.parseArray(content, GoodsDetail.class);
        for (GoodsDetail goodsDetail : list) {
            goodsDetailService.updateByCondition(goodsDetail);
        }
    }

}
