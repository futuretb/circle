package com.circle.dope.goods.utils;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * double int 加减乘除帮助类
 * 所有传入传出的值都为String 需求决定.
 * @ClassName: BigDecimalUtils 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author 郭文强  
 * @date 2016年7月7日 下午6:44:25 
 *
 */
public class BigDecimalUtils {
	/**
	 * 格式化金额格式
	 */
	private static final DecimalFormat  fmt   = new DecimalFormat("##,###,###,###,##0.00");
	/**
	 * 加法运算
	 * @param v1 
     * @param v2 
	 * @return 两个参数的和
	 */
	public static String add(String v1,String v2){
		if(StringUtils.isEmpty(v1)){
			v1 = "0";
		}
		if(StringUtils.isEmpty(v2)){
			v2 = "0";
		}
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return b1.add(b2).toString();
	}
	/**
	 * 浮点数 加 小数位四舍五入精确计算 a+b
	 * 
	 * @param a
	 * @param b
	 * @param scale
	 *            精确的小数位
	 * @return
	 */
	public static double preciseAdd(Double a, Double b, int scale) {
		if (null == b ) {
			b= 0D;
		}
		if (null == a ) {
			a= 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.add(new BigDecimal(Double.toString(b)));
		return round(r.doubleValue(), scale);
	}
	 /** 
     * 减法运算  
     * @param v1 减数 
     * @param v2 被减数 
     * @return 两个参数的差 
     */  
    public static String substract(String v1, String v2) {  
    	if(StringUtils.isEmpty(v1)){
			v1 = "0";
		}
		if(StringUtils.isEmpty(v2)){
			v2 = "0";
		}
    	BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
        return b1.subtract(b2).toString();  
    }
    
    /**
	 * 浮点数 减 小数位四舍五入精确计算 a-b
	 * 
	 * @param a
	 * @param b
	 * @param scale
	 *            精确的小数位
	 * @return
	 */
	public static double preciseSub(Double a, Double b, int scale) {
		if (null == b ) {
			b= 0D;
		}
		if (null == a ) {
			a= 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.subtract(new BigDecimal(Double.toString(b)));
		return round(r.doubleValue(), scale);
	}

	/**
	 * 浮点数 乘 精确计算 a*b
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static double preciseMul(Double a, Double b) {
		if (null == b || null == a) {
			return 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.multiply(new BigDecimal(Double.toString(b)));
		return r.doubleValue();
	}
	/**
	 * 浮点数 乘 精确计算 a*b
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static String preciseMul(String a, String b) {
		if(StringUtils.isEmpty(a)){
			a="0";
		}
		if(StringUtils.isEmpty(b)){
			b="0";
		}
		BigDecimal r = new BigDecimal(a);
		r = r.multiply(new BigDecimal(b));
		return r.toString();
	}

	/**
	 * 浮点数 乘 小数位四舍五入精确计算 a*b
	 * 
	 * @param a
	 * @param b
	 * @param scale
	 *            精确的小数位
	 * @return
	 */
	public static double preciseMul(Double a, Double b, int scale) {
		if (null == b || null == a) {
			return 0D;
		}
		return round(new BigDecimal(Double.toString(a)).multiply(
				new BigDecimal(Double.toString(b))).doubleValue(), scale);
	}
	
	/**
	 * 浮点数 除 精确计算 a/b
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static double preciseDev(Double a, Double b) {
		if (null == b || b == 0D || null == a) {
			return 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.divide(new BigDecimal(Double.toString(b)), 6,
				BigDecimal.ROUND_HALF_UP);
		return r.doubleValue();
	}

	/**
	 * 浮点数 除 小数位四舍五入精确计算 a/b
	 * @param a
	 * @param b
	 * @param scale
	 *            精确的小数位
	 * @return
	 */
	public static String preciseDev(Double a, Double b, int scale) {
		if (null == b || b == 0D || null == a) {
			return "0";
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.divide(new BigDecimal(Double.toString(b)), scale,
				BigDecimal.ROUND_HALF_UP).setScale(scale);
		return r.toPlainString();
	}

	public static Double preciseDev(Double a,Integer b, int scale) {
		if (null == b || b == 0 || null == a) {
			return 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.divide(new BigDecimal(Integer.toString(b)), scale,
				BigDecimal.ROUND_HALF_UP).setScale(scale);
		return r.doubleValue();
	}
	public static Double preciseDev(Double a,Long b, int scale) {
		if (null == b || b == 0 || null == a) {
			return 0D;
		}
		BigDecimal r = new BigDecimal(Double.toString(a));
		r = r.divide(new BigDecimal(Long.toString(b)), scale,
				BigDecimal.ROUND_HALF_UP).setScale(scale);
		return r.doubleValue();
	}
	
	
	/**
	 * 浮点数 除 小数位四舍五入精确计算 a/b
	 * 
	 * @param a
	 * @param b
	 * @param scale
	 *            精确的小数位
	 * @return
	 */
//	public static String preciseDevBigD(Double a, Double b, int scale) {
//		if (null == b || b == 0D || null == a) {
//			return "0.00";
//		}
//		BigDecimal r = new BigDecimal(Double.toString(a));
//		r = r.divide(new BigDecimal(Double.toString(b)), scale,
//				BigDecimal.ROUND_HALF_UP).setScale(scale);
//		return r.toPlainString();
//	}
    /**
     * 比较两个值的大小
     * @return
     * -1 小于  num1 < num2
     * 0 相等     num1 = num2
     * 1 大于     num1 > num2
     */
    public static int isCompareTo(String num1,String num2){
    	if(StringUtils.isEmpty(num1)){
    		num1 = "0";
		}
		if(StringUtils.isEmpty(num2)){
			num2 = "0";
		}
    	BigDecimal b1 = new BigDecimal(num1);
		BigDecimal b2 = new BigDecimal(num2);
    	return b1.compareTo(b2);
    }
    /**
	 * 提供精确的小数位四舍五入处理。
	 * 
	 * @param v
	 *            需要四舍五入的数字
	 * @param scale
	 *            小数点后保留几位
	 * @return 四舍五入后的结果
	 */
	public static double round(String v, int scale) {
		if (null == v) {
			v = "0";
		}
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(v);
		BigDecimal one = new BigDecimal("1");
		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/**
	 * 提供精确的小数位四舍五入处理。
	 * 
	 * @param v
	 *            需要四舍五入的数字
	 * @param scale
	 *            小数点后保留几位
	 * @return 四舍五入后的结果
	 */
	public static double round(Double v, int scale) {
		if (null == v) {
			v = 0D;
		}
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	/***
	 *将数字格式化，如：传入：1000000.11,输入效果为：1,000,000.11
	 * @param moeny
	 * @param b
	 * @return
	 */
	public static String formattingMoeny(String moeny,Double b){
		long moenyLong=Long.valueOf(moeny);
		return fmt.format(moenyLong/b);
	}

	/***
	 * 将数字格式化，如：传入：1000000.11,输入效果为：1,000,000.11
	 * @param moeny
	 * @param b
	 * @return
	 */
	public static String formattingMoeny(Double moeny,Double b){
		return fmt.format(moeny/b);
	}
	
	/***
	 * 将数字格式化，如：传入：1000000.11,输入效果为：1,000,000.11
	 * @param moeny
	 * @param b
	 * @return
	 */
	public static String formattingMoeny(Double moeny){
		return fmt.format(moeny);
	}
	
	
	/***
	 * 将数字格式化，如：传入：1000000.11,输入效果为：1,000,000.11
	 * @param moeny
	 * @param b
	 * @return
	 */
	public static String formattingMoeny(String moeny){
		long formattingMoeny=Long.valueOf(moeny);
		return fmt.format(formattingMoeny);
	}

}

