package com.circle.dope.goods.impl;

import com.alibaba.fastjson.JSON;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.constant.PrefixEnum;
import com.circle.dope.goods.client.UserInfoClient;
import com.circle.dope.goods.client.WxUserInfoClient;
import com.circle.dope.goods.dao.BrandInfoDao;
import com.circle.dope.goods.dao.CategoryInfoDao;
import com.circle.dope.goods.dao.StoreInfoDao;
import com.circle.dope.goods.po.BrandInfo;
import com.circle.dope.goods.po.CategoryInfo;
import com.circle.dope.goods.po.StoreInfo;
import com.circle.dope.goods.service.RedisService;
import com.circle.dope.user.po.UserInfo;
import com.circle.dope.user.po.WxUserInfo;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Classname RedisServiceImpl
 * @Description TODO
 * @Date 2019/1/22 15:32
 * @Created by win7
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private CategoryInfoDao categoryInfoDao;
    @Resource
    private BrandInfoDao brandInfoDao;
    @Resource
    private StoreInfoDao storeInfoDao;
    @Resource
    private UserInfoClient userInfoClient;
    @Resource
    private WxUserInfoClient wxUserInfoClient;


    @Override
    public UserInfo getUserInfo(String userId) {
        String redisUserInfofoKey = PrefixEnum.GOODS_TYPE.USER_ID.getPrefix() + userId;
        UserInfo userInfo = null;
        userInfo = JSON.parseObject(stringRedisTemplate.opsForValue().get(redisUserInfofoKey), UserInfo.class);
        if (null == userInfo) {
            // DB
            Map<String, Object> map = new HashMap();
            map.put("userId", userId);
            BaseResult baseResult = userInfoClient.findByCondition(map);
            if (baseResult != null && baseResult.getCode() == 200) {
                userInfo = JSON.parseObject(baseResult.getResult().toString(), UserInfo.class);
            }
            stringRedisTemplate.opsForValue().set(redisUserInfofoKey, JSON.toJSONString(userInfo), 5L, TimeUnit.MINUTES);
        }
        return userInfo;
    }

    @Override
    public WxUserInfo getWxUserInfo(String wxUserId) {
        String redisWxUserInfofoKey = PrefixEnum.GOODS_TYPE.WX_USER_ID.getPrefix() + wxUserId;
        WxUserInfo wxUserInfo = null;
        wxUserInfo = JSON.parseObject(stringRedisTemplate.opsForValue().get(redisWxUserInfofoKey), WxUserInfo.class);
        if (null == wxUserInfo) {
            // DB
            Map<String, Object> map = new HashMap();
            map.put("wxUserId", wxUserId);
            BaseResult baseResult = wxUserInfoClient.findByCondition(map);
            if (baseResult != null && baseResult.getCode() == 200) {
//                wxUserInfo = JSON.parseObject(baseResult.getResult().toString(), WxUserInfo.class);
//                stringRedisTemplate.opsForValue().set(redisWxUserInfofoKey, JSON.toJSONString(wxUserInfo), 5L, TimeUnit.MINUTES);
            }
        }
        return wxUserInfo;
    }

    @Override
    public CategoryInfo getCategoryInfo(Long categoryId) {
        String redisCategoryKey = PrefixEnum.GOODS_TYPE.CATEGORY_ID.getPrefix() + categoryId;
        CategoryInfo categoryInfo = null;
        categoryInfo = JSON.parseObject(stringRedisTemplate.opsForValue().get(redisCategoryKey), CategoryInfo.class);
        if (null == categoryInfo) {
            // DB
            categoryInfo = categoryInfoDao.findById(categoryId);
            stringRedisTemplate.opsForValue().set(redisCategoryKey, JSON.toJSONString(categoryInfo), 5L, TimeUnit.MINUTES);
        }
        return categoryInfo;
    }

    @Override
    public BrandInfo getBrandInfo(Long brandId) {
        String redisBrandKey = PrefixEnum.GOODS_TYPE.BRAND_ID.getPrefix() + brandId;
        BrandInfo brandInfo = null;
        brandInfo = JSON.parseObject(stringRedisTemplate.opsForValue().get(redisBrandKey), BrandInfo.class);
        if (null == brandInfo) {
            // DB
            brandInfo = brandInfoDao.findById(brandId);
            stringRedisTemplate.opsForValue().set(redisBrandKey, JSON.toJSONString(brandInfo), 5L, TimeUnit.MINUTES);
        }
        return brandInfo;
    }

    @Override
    public StoreInfo getStoreInfo(Long storeId) {
        String redisStoreInfoKey = PrefixEnum.GOODS_TYPE.STORE_INFO_ID.getPrefix() + storeId;
        StoreInfo storeInfo = null;
        storeInfo = JSON.parseObject(stringRedisTemplate.opsForValue().get(redisStoreInfoKey), StoreInfo.class);
        if (null == storeInfo) {
            // DB
            storeInfo = storeInfoDao.findById(storeId);
            stringRedisTemplate.opsForValue().set(redisStoreInfoKey, JSON.toJSONString(storeInfo), 5L, TimeUnit.MINUTES);
        }
        return storeInfo;
    }
}
