package com.circle.dope.goods.controller;

import com.alibaba.fastjson.JSONObject;
import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.PageResult;
import com.circle.dope.framework.util.RedisManager;
import com.circle.dope.goods.api.ShoppingCarApi;
import com.circle.dope.goods.client.UserServiceClient;
import com.circle.dope.goods.po.*;
import com.circle.dope.goods.service.*;
import com.circle.dope.user.po.MiddleUserFriend;
import com.circle.dope.user.po.WxUserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 购物车 - Controller
 *
 *  Created by dicksoy on '2019-01-06 18:52:16'.
 *  Email dicksoy@163.com
 */

@Controller
@RequestMapping(value = "/goods/shoppingCar")
public class ShoppingCarController {

    @Resource
    private ShoppingCarService shoppingCarService;

    @Resource
    private RedisManager redisManager;

    @Resource
    private MiddleActivityGoodsService middleActivityGoodsService;

    @Resource
    private ActivityInfoService activityInfoService;

    @Resource
    private GoodsDetailService goodsDetailService;

    @Resource
    private StoreInfoService storeInfoService;

    @Resource
    private UserServiceClient userServiceClient;

    @RequestMapping(value = "/findListByPage", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findListByPage(@RequestBody Map<String, Object> params,
                                     @RequestParam("token") String token) {
        String wxUserStr = redisManager.get(token);
        WxUserInfo wxUserInfo = JSONObject.parseObject(wxUserStr, WxUserInfo.class);
        PageResult<ShoppingCar> shoppingCars = shoppingCarService.findListByPage(params);
        for (ShoppingCar shoppingCar: shoppingCars.getList()) {
            if (null != shoppingCar) {
                StoreInfo storeInfo = storeInfoService.findById(shoppingCar.getStoreId());
                BigDecimal price = priceCalculation(wxUserInfo, shoppingCar.getGoodsDetailId(), shoppingCar.getStoreId());
                Map<String, Object> data = new HashMap<>();
                data.put("price", price);
                data.put("storeInfo", storeInfo);
                shoppingCar.setOtherData(data);
            }
        }
        return SUCCESS.result(shoppingCars);
    }

    /**
     * 商品价格计算
     * @param params
     * @return
     */
    @RequestMapping(value = "/priceCalculation", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult priceCalculation(Map<String, Object> params) {
        WxUserInfo wxUserInfo = JSONObject.parseObject(JSONObject.toJSONString(params.get("wxUserInfo")), WxUserInfo.class);
        Long goodsDetailId = Long.valueOf(String.valueOf(params.get("goodsDetailId")));
        Long storeInfoInfo = Long.valueOf(String.valueOf(params.get("storeInfoId")));
        return SUCCESS.result(priceCalculation(wxUserInfo, goodsDetailId, storeInfoInfo));
    }

    /**
     * 当前商品价格计算
     * @return
     */
    private BigDecimal priceCalculation(WxUserInfo wxUserInfo, Long goodsDetailId, Long storeInfoInfo) {
        GoodsDetail goodsDetail = goodsDetailService.findById(goodsDetailId);
        boolean vip = (1 == wxUserInfo.getLevel()) ? true : false;
        Map<String, Object> hotSaleMap = new HashMap<>();
        hotSaleMap.put("goodsDetailId", goodsDetail.getId());
        hotSaleMap.put("goodsId", goodsDetail.getGoodsId());
        hotSaleMap.put("status", "0");
        MiddleActivityGoods middleActivityGoods = middleActivityGoodsService.findOne(hotSaleMap);
        if (null != middleActivityGoods) {
            // 1 限时折扣
            if (null != middleActivityGoods.getActivityType() && 1 == middleActivityGoods.getActivityType()) {
                return middleActivityGoods.getActivityPrice();
            } else {
                // 0 热门推荐 2 类目品牌
                return vip ? middleActivityGoods.getActivityPrice() : goodsDetail.getStorePrice();
            }
        } else {
            ActivityInfo activityInfo = activityInfoService.findById(middleActivityGoods.getActivityInfoId());
            StoreInfo storeInfo = storeInfoService.findById(storeInfoInfo);
            BaseResult baseResult = userServiceClient.findWxUserFriendOne(wxUserInfo.getWxUserCode(), storeInfo.getUserId());
            MiddleUserFriend middleUserFriend = JSONObject.parseObject(JSONObject.toJSONString(baseResult.getResult()), MiddleUserFriend.class);
            // 个人活动
            if (0 == activityInfo.getIsCircle()) {
                // 代理
                return 1 == middleUserFriend.getIsAgent() ? goodsDetail.getAgentPrice() : goodsDetail.getStorePrice();
            } else {
                return vip ? goodsDetail.getAgentPrice() : goodsDetail.getStorePrice();
            }
        }
    }

}
