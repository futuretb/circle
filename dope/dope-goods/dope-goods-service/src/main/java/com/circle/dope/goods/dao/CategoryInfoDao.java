package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.CategoryInfo;

import java.util.HashMap;
import java.util.List;

/**
 * 类目 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:48:59'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface CategoryInfoDao extends BaseDao<CategoryInfo> {
    List<CategoryInfo> findByGoodsId(Long goodsId);

    List<CategoryInfo> findAllByStoreId(HashMap<String, Object> params);
}
