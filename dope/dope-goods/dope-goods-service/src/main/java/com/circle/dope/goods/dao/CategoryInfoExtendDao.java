package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.CategoryInfoExtend;

/**
 * 类目关联表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:49:04'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface CategoryInfoExtendDao extends BaseDao<CategoryInfoExtend> {

}
