package com.circle.dope.goods.controller;

import com.circle.dope.framework.base.BaseResult;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.framework.util.AssertUtil;
import com.circle.dope.goods.api.GoodsInfoApi;
import com.circle.dope.goods.param.intput.GoodsInfoIntput;
import com.circle.dope.goods.param.output.GoodsInfoOutput;
import com.circle.dope.goods.param.vo.GoodsInfoVo;
import com.circle.dope.goods.po.GoodsInfo;
import com.circle.dope.goods.service.GoodsInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

import static com.circle.dope.framework.base.BaseResult.SUCCESS;

/**
 * 商品信息表 - Controller
 *
 *  Created by dicksoy on '2019-01-06 18:49:15'.
 *  Email dicksoy@163.com
 */
@Controller
@RequestMapping(value = "/goods/goodsInfo")
public class GoodsInfoController {

    @Resource
    private GoodsInfoService goodsInfoService;

    /**
     * 创建商品
     * @param goodsInfoIntput
     * @return
     */
    @RequestMapping(value = "/createGoods", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult createGoods(@RequestBody GoodsInfoIntput goodsInfoIntput) {
        AssertUtil.notNull(goodsInfoIntput);
        goodsInfoService.createGoods(goodsInfoIntput.getGoodsInfo(), goodsInfoIntput.getMgcList(), goodsInfoIntput.getGdList(), goodsInfoIntput.getPicList());
        return SUCCESS;
    }

    /**
     * 编辑商品
     * @param goodsInfoIntput
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult update(@RequestBody GoodsInfoIntput goodsInfoIntput) {
        AssertUtil.notNull(goodsInfoIntput);
        goodsInfoService.updateGoods(goodsInfoIntput.getGoodsInfo(), goodsInfoIntput.getMgcList(), goodsInfoIntput.getPicList());
        return SUCCESS;
    }

    /**
     * 获取活动相关商品
     * @param params
     * @return
     */
    @RequestMapping(value = "/findByActivityId", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findByActivityId(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<GoodsInfoVo> giList = goodsInfoService.findByActivityId(params);
        return SUCCESS.result(giList);
    }

    /**
     * 买家版 商品详情
     * goodsOne
     * @param params
     * @return
     */
    @RequestMapping(value = "/goodsOne", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult goodsOne(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        GoodsInfoOutput goodsInfoOutput = goodsInfoService.goodsOne(params);
        return SUCCESS.result(goodsInfoOutput);
    }

    /**
     * 买家版 商城主页
     * @param params
     * @return
     */
    @RequestMapping(value = "/findCircle", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findCircle(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<GoodsInfoVo> giList = goodsInfoService.findCircle(params);
        return SUCCESS.result(giList);
    }


    /**
     * 商家版 商品管理
     * @param params type 1出售中 2未出售 3历史库存
     * offset pageSize 分页查询
     * category 类目筛选
     * brandId 品牌筛选
     * searchParam 模糊搜索
     * @return
     */
    @RequestMapping(value = "/findGoods", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult findGoods(@RequestBody Map<String, Object> params) {
        AssertUtil.notNull(params);
        ListResult<GoodsInfoVo> giList = goodsInfoService.findGoods(params);
        return SUCCESS.result(giList);
    }

    /**
     * 商家版 商品管理统计
     * @param storeId
     * @return
     */
    @RequestMapping(value = "/statistics", method = RequestMethod.POST)
    @ResponseBody
    public BaseResult statistics(@RequestBody Long storeId) {
        GoodsInfoVo goodsInfoVo = goodsInfoService.statistics(storeId);
        return SUCCESS.result(goodsInfoVo);
    }

}
