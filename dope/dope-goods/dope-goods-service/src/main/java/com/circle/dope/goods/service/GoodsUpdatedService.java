package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.goods.po.GoodsUpdated;

/**
 * 商品编辑记录 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:49:20'.
 *  Email dicksoy@163.com
 */
public interface GoodsUpdatedService extends BaseService<GoodsUpdated> {

}
