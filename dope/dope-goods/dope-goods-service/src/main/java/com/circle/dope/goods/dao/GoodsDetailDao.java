package com.circle.dope.goods.dao;

import org.apache.ibatis.annotations.Mapper;
import com.circle.dope.framework.base.BaseDao;
import com.circle.dope.goods.po.GoodsDetail;

/**
 * 商品明细表 - Dao
 *
 *  Created by dicksoy on '2019-01-06 18:49:10'.
 *  Email dicksoy@163.com
 */
@Mapper
public interface GoodsDetailDao extends BaseDao<GoodsDetail> {

}
