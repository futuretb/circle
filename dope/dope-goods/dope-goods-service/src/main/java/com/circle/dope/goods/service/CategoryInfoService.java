package com.circle.dope.goods.service;

import com.circle.dope.framework.base.BaseService;
import com.circle.dope.framework.base.ListResult;
import com.circle.dope.goods.po.BrandInfo;
import com.circle.dope.goods.po.CategoryInfo;

import java.util.HashMap;

/**
 * 类目 - Service
 *
 *  Created by dicksoy on '2019-01-06 18:48:59'.
 *  Email dicksoy@163.com
 */
public interface CategoryInfoService extends BaseService<CategoryInfo> {

    ListResult<CategoryInfo> findAllByStoreId(HashMap<String, Object> params);
}
