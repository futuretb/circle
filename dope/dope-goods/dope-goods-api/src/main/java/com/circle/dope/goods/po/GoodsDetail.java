package com.circle.dope.goods.po;


import com.circle.dope.framework.base.BasePo;

import java.math.BigDecimal;
import java.util.Date;
/**
 *  商品明细表 - Po
 *
 *  Created by dicksoy on '2019-01-10 18:07:19'.
 *  Email dicksoy@163.com
 */
public class GoodsDetail extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 代理价格
     */
    private BigDecimal agentPrice;

    /**
     * 颜色
     */
    private String color;

    /**
     * 商品ID
     */
    private Long goodsId;

    /**
     * 总库存
     */
    private Long inventory;

    /**
     * 是否为真实库存 0 不预售 1 预售
     */
    private Integer isPresell;

    /**
     * 在售数
     */
    private Long saleIn;

    /**
     * 尺码
     */
    private String size;

    /**
     * 直客价格（使用代理价计算）
     */
    private BigDecimal storePrice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public BigDecimal getAgentPrice() {
        return agentPrice;
    }

    public void setAgentPrice(BigDecimal agentPrice) {
        this.agentPrice = agentPrice;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getInventory() {
        return inventory;
    }

    public void setInventory(Long inventory) {
        this.inventory = inventory;
    }

    public Integer getIsPresell() {
        return isPresell;
    }

    public void setIsPresell(Integer isPresell) {
        this.isPresell = isPresell;
    }

    public Long getSaleIn() {
        return saleIn;
    }

    public void setSaleIn(Long saleIn) {
        this.saleIn = saleIn;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public BigDecimal getStorePrice() {
        return storePrice;
    }

    public void setStorePrice(BigDecimal storePrice) {
        this.storePrice = storePrice;
    }

}