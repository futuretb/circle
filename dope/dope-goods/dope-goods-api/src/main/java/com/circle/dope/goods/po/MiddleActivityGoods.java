package com.circle.dope.goods.po;


import com.circle.dope.framework.base.BasePo;

import java.math.BigDecimal;
import java.util.Date;
/**
 *  活动商品关系表 - Po
 *
 *  Created by dicksoy on '2019-01-18 15:24:23'.
 *  Email dicksoy@163.com
 */
public class MiddleActivityGoods extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 商品明细ID
     */
    private Long goodsDetailId;

    /**
     * 商品id
     */
    private Long goodsId;

    /**
     * 活动id
     */
    private Long activityInfoId;

    /**
     * 活动价格
     */
    private BigDecimal activityPrice;

    /**
     * 0 热门推荐 1 限时折扣 2 类目品牌
     */
    private Integer activityType;

    /**
     * 0 上架 1 下架
     */
    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getGoodsDetailId() {
        return goodsDetailId;
    }

    public void setGoodsDetailId(Long goodsDetailId) {
        this.goodsDetailId = goodsDetailId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getActivityInfoId() {
        return activityInfoId;
    }

    public void setActivityInfoId(Long activityInfoId) {
        this.activityInfoId = activityInfoId;
    }

    public BigDecimal getActivityPrice() {
        return activityPrice;
    }

    public void setActivityPrice(BigDecimal activityPrice) {
        this.activityPrice = activityPrice;
    }

    public Integer getActivityType() {
        return activityType;
    }

    public void setActivityType(Integer activityType) {
        this.activityType = activityType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}