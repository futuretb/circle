package com.circle.dope.goods.po;

import com.circle.dope.framework.base.BasePo;
import java.util.Date;


/**
 *  类目关联表 - Po
 *
 *  Created by dicksoy on '2019-01-06 18:49:04'.
 *  Email dicksoy@163.com
 */
public class CategoryInfoExtend extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 官方认证类目ID
     */
    private Long categoryInfoId;

    /**
     * 自定义类目ID
     */
    private Long yourSelfCategoryId;

    /**
     * 自定义类目名
     */
    private String yourSelfCategoryName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getCategoryInfoId() {
        return categoryInfoId;
    }

    public void setCategoryInfoId(Long categoryInfoId) {
        this.categoryInfoId = categoryInfoId;
    }

    public Long getYourSelfCategoryId() {
        return yourSelfCategoryId;
    }

    public void setYourSelfCategoryId(Long yourSelfCategoryId) {
        this.yourSelfCategoryId = yourSelfCategoryId;
    }

    public String getYourSelfCategoryName() {
        return yourSelfCategoryName;
    }

    public void setYourSelfCategoryName(String yourSelfCategoryName) {
        this.yourSelfCategoryName = yourSelfCategoryName;
    }

}