package com.circle.dope.goods.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  店铺信息表 - Po
 *
 *  Created by dicksoy on '2019-01-10 18:06:21'.
 *  Email dicksoy@163.com
 */
public class StoreInfo extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 支付宝真是姓名
     */
    private String alipayName;

    /**
     * 支付宝账号
     */
    private String alipayUser;

    /**
     * 品牌认证 0 未认证 1 审核中 2 审核通过 3 审核失败
     */
    private Integer brandType;

    /**
     * 店铺背景图
     */
    private String coverImageUrl;

    /**
     * 店长userid
     */
    private String userId;

    /**
     * 身份账号
     */
    private String identityCardNum;

    /**
     * 身份证反面
     */
    private String identityCardImageBack;

    /**
     * 身份证正面
     */
    private String identityCardImageFront;

    /**
     * 身份证真实名称
     */
    private String identityCardName;

    /**
     * 是否关闭代理显示库存
     */
    private Integer isCloseShowInventory;

    /**
     * 店铺jpush标识符
     */
    private String jpushCode;

    /**
     * 正品认证 0 未认证 1 审核中 2 审核通过 3:审核失败
     */
    private Integer qualityType;

    /**
     * 店铺头像
     */
    private String storeAvatar;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 0 未认证 1 审核中 2 审核通过 3 审核失败
     */
    private Integer type;

    /**
     * 微信名称
     */
    private String wxName;

    /**
     * 微信openId
     */
    private String wxOpenId;

    /**
     * 微信账号
     */
    private String wxAccount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getAlipayName() {
        return alipayName;
    }

    public void setAlipayName(String alipayName) {
        this.alipayName = alipayName;
    }

    public String getAlipayUser() {
        return alipayUser;
    }

    public void setAlipayUser(String alipayUser) {
        this.alipayUser = alipayUser;
    }

    public Integer getBrandType() {
        return brandType;
    }

    public void setBrandType(Integer brandType) {
        this.brandType = brandType;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIdentityCardNum() {
        return identityCardNum;
    }

    public void setIdentityCardNum(String identityCardNum) {
        this.identityCardNum = identityCardNum;
    }

    public String getIdentityCardImageBack() {
        return identityCardImageBack;
    }

    public void setIdentityCardImageBack(String identityCardImageBack) {
        this.identityCardImageBack = identityCardImageBack;
    }

    public String getIdentityCardImageFront() {
        return identityCardImageFront;
    }

    public void setIdentityCardImageFront(String identityCardImageFront) {
        this.identityCardImageFront = identityCardImageFront;
    }

    public String getIdentityCardName() {
        return identityCardName;
    }

    public void setIdentityCardName(String identityCardName) {
        this.identityCardName = identityCardName;
    }

    public Integer getIsCloseShowInventory() {
        return isCloseShowInventory;
    }

    public void setIsCloseShowInventory(Integer isCloseShowInventory) {
        this.isCloseShowInventory = isCloseShowInventory;
    }

    public String getJpushCode() {
        return jpushCode;
    }

    public void setJpushCode(String jpushCode) {
        this.jpushCode = jpushCode;
    }

    public Integer getQualityType() {
        return qualityType;
    }

    public void setQualityType(Integer qualityType) {
        this.qualityType = qualityType;
    }

    public String getStoreAvatar() {
        return storeAvatar;
    }

    public void setStoreAvatar(String storeAvatar) {
        this.storeAvatar = storeAvatar;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getWxName() {
        return wxName;
    }

    public void setWxName(String wxName) {
        this.wxName = wxName;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public String getWxAccount() {
        return wxAccount;
    }

    public void setWxAccount(String wxAccount) {
        this.wxAccount = wxAccount;
    }

}