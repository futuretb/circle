package com.circle.dope.goods.po;

import com.circle.dope.framework.base.BasePo;
import java.util.Date;


/**
 *  商品编辑记录 - Po
 *
 *  Created by dicksoy on '2019-01-06 18:49:20'.
 *  Email dicksoy@163.com
 */
public class GoodsUpdated extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态：0.未删除；1.删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 商品明细id
     */
    private String goodsDetailId;

    /**
     * 商品ID
     */
    private String goodsId;

    /**
     * 修改后数据
     */
    private String newData;

    /**
     * 修改前数据
     */
    private String oldData;

    /**
     * 类型 1成本价格 2店铺售价 3代理价格 4 库存
     */
    private Integer type;

    /**
     * 用户id
     */
    private String userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getGoodsDetailId() {
        return goodsDetailId;
    }

    public void setGoodsDetailId(String goodsDetailId) {
        this.goodsDetailId = goodsDetailId;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getNewData() {
        return newData;
    }

    public void setNewData(String newData) {
        this.newData = newData;
    }

    public String getOldData() {
        return oldData;
    }

    public void setOldData(String oldData) {
        this.oldData = oldData;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}