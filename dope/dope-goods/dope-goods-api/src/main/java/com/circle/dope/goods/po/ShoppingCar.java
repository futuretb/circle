package com.circle.dope.goods.po;

import com.circle.dope.framework.base.BasePo;
import java.util.Date;


/**
 *  购物车 - Po
 *
 *  Created by dicksoy on '2019-01-06 18:52:16'.
 *  Email dicksoy@163.com
 */
public class ShoppingCar extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 商品数量
     */
    private Integer count;

    /**
     * 购物车创建者
     */
    private String buyerUserCode;

    /**
     * 商品明细id
     */
    private Long goodsDetailId;

    /**
     * 0 非官方 1官方
     */
    private String isCircle;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 员工code
     */
    private String employeeUserId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getBuyerUserCode() {
        return buyerUserCode;
    }

    public void setBuyerUserCode(String buyerUserCode) {
        this.buyerUserCode = buyerUserCode;
    }

    public Long getGoodsDetailId() {
        return goodsDetailId;
    }

    public void setGoodsDetailId(Long goodsDetailId) {
        this.goodsDetailId = goodsDetailId;
    }

    public String getIsCircle() {
        return isCircle;
    }

    public void setIsCircle(String isCircle) {
        this.isCircle = isCircle;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public String getEmployeeUserId() {
        return employeeUserId;
    }

    public void setEmployeeUserId(String employeeUserId) {
        this.employeeUserId = employeeUserId;
    }

}