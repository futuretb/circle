package com.circle.dope.goods.po;


import com.circle.dope.framework.base.BasePo;

import java.math.BigDecimal;
import java.util.Date;
/**
 *  商品信息表 - Po
 *
 *  Created by dicksoy on '2019-01-10 18:07:15'.
 *  Email dicksoy@163.com
 */
public class GoodsInfo extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态：0.未删除；1.删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 品牌id
     */
    private Long brandId;

    /**
     * 公价(柜台价)
     */
    private BigDecimal counterPrice;

    /**
     * 平均成本
     */
    private BigDecimal avgCost;

    /**
     * 创建者usercode
     */
    private String userId;

    /**
     * 运费：统一到付
     */
    private String freight;

    /**
     * 商品描述
     */
    private String goodsDesc;

    /**
     * 商店货号
     */
    private String goodsNumber;

    /**
     * 商品名
     */
    private String name;

    /**
     * 商品封面图
     */
    private String picture;

    /**
     * 二维码
     */
    private String qrCode;

    /**
     * 预计发货时间
     */
    private String sendDate;

    /**
     * 1男装 2女装 3男女同 4童装
     */
    private String sex;

    /**
     * 交易备注
     */
    private String remark;

    /**
     * 店铺ID
     */
    private Long storeId;

    /**
     * 关税
     */
    private BigDecimal tariff;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFreight() {
        return freight;
    }

    public void setFreight(String freight) {
        this.freight = freight;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getremark() {
        return remark;
    }

    public void setremark(String remark) {
        this.remark = remark;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public BigDecimal getCounterPrice() {
        return counterPrice;
    }

    public void setCounterPrice(BigDecimal counterPrice) {
        this.counterPrice = counterPrice;
    }

    public BigDecimal getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(BigDecimal avgCost) {
        this.avgCost = avgCost;
    }

    public BigDecimal getTariff() {
        return tariff;
    }

    public void setTariff(BigDecimal tariff) {
        this.tariff = tariff;
    }
}