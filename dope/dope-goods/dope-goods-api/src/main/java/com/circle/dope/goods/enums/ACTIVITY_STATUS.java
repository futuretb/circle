package com.circle.dope.goods.enums;

public enum ACTIVITY_STATUS {

    WAIT_AUDIT("待审核"),
    AUDIT_PASS("审核通过"),
    AUDIT_REJECT("审核拒绝"),
    START("开始"),
    RUNNING("进行中"),
    END("结束");

    private String cnName;

    ACTIVITY_STATUS(String cnName) {
        this.cnName = cnName;
    }

    public String getCnName() {
        return cnName;
    }
}
