package com.circle.dope.goods.po;

import com.circle.dope.framework.base.BasePo;
import java.util.Date;


/**
 *  商品类目关系表 - Po
 *
 *  Created by dicksoy on '2019-01-09 14:19:07'.
 *  Email dicksoy@163.com
 */
public class MiddleGoodsCategory extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 类目id
     */
    private Long categoryId;

    /**
     * 商品uid
     */
    private Long goodsId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

}