package com.circle.dope.goods.po;


import com.circle.dope.framework.base.BasePo;
import java.util.Date;
/**
 *  活动发布 - Po
 *
 *  Created by dicksoy on '2019-01-18 15:24:15'.
 *  Email dicksoy@163.com
 */
public class ActivityInfo extends BasePo {

    public static final long serialVersionUID = 13L;

    /**
     * id
     */
    private Long id;

    /**
     * 数据状态 0 未删除 1 删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改时间
     */
    private Date updatedTime;

    /**
     * 活动中品牌id
     */
    private Long brandId;

    /**
     * 活动中类目id
     */
    private Long categoryId;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建者userid
     */
    private String userId;

    /**
     * 开始时间
     */
    private Date startDate;

    /**
     * 结束时间
     */
    private Date endDate;

    /**
     * WAIT_AUDIT 待审核 AUDIT_PASS 审核通过 AUDIT_REJECT 审核拒绝 START 开始 RUNNING 进行中 END 结束
     */
    private String status;

    /**
     * 0 个人活动 1官方活动
     */
    private Integer isCircle;

    /**
     * 活动小程序二维码
     */
    private String miniProgramQrCode;

    /**
     * 活动封面图
     */
    private String picture;

    /**
     * 拒绝原因
     */
    private String refusal;

    /**
     * 标题
     */
    private String title;

    /**
     * 0 热门推荐 1 限时折扣 2 类目品牌
     */
    private Integer type;

    /**
     * 用户版code
     */
    private String wxUserCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIsCircle() {
        return isCircle;
    }

    public void setIsCircle(Integer isCircle) {
        this.isCircle = isCircle;
    }

    public String getMiniProgramQrCode() {
        return miniProgramQrCode;
    }

    public void setMiniProgramQrCode(String miniProgramQrCode) {
        this.miniProgramQrCode = miniProgramQrCode;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getRefusal() {
        return refusal;
    }

    public void setRefusal(String refusal) {
        this.refusal = refusal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getWxUserCode() {
        return wxUserCode;
    }

    public void setWxUserCode(String wxUserCode) {
        this.wxUserCode = wxUserCode;
    }

}